<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\User;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;
use common\models\PermisosHelpers;

/**
 * This is the model class for table "textos".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $categoria
 * @property string $clasificacion
 * @property string $descripcion
 * @property string $autor
 * @property string $texto
 * @property integer $stopwords
 * @property string $stopwords_lista
 * @property integer $stemmer
 * @property string $stemmer_lista
 * @property string $observaciones
 * @property string $lugares
 * @property integer $publico
 * @property integer $cantidad_topten
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Stemmers $stemmer0
 * @property Stopwords $stopwords0
 */
class Textos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'textos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['descripcion', 'texto', 'stopwords_lista', 'stemmer_lista', 'observaciones', 'lugares'], 'string'],
            [['stopwords', 'stemmer', 'publico', 'cantidad_topten', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nombre', 'categoria', 'clasificacion', 'autor'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'categoria' => Yii::t('app', 'Categoria'),
            'clasificacion' => Yii::t('app', 'Clasificacion'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'autor' => Yii::t('app', 'Autor'),
            'texto' => Yii::t('app', 'Texto'),
            'stopwords' => Yii::t('app', 'Stopwords'),
            'stopwords_lista' => Yii::t('app', 'Stopwords Lista'),
            'stemmer' => Yii::t('app', 'Stemmer'),
            'stemmer_lista' => Yii::t('app', 'Stemmer Lista'),
            'observaciones' => Yii::t('app', 'Observaciones'),
            'lugares' => Yii::t('app', 'Lugares'),
            'publico' => Yii::t('app', 'Publico'),
            'cantidad_topten' => Yii::t('app', 'Cantidad Topten'),
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'created_by' => 'Creado Por',
            'updated_by' => 'Actualizado Por',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStemmer0()
    {
        return $this->hasOne(Stemmers::className(), ['id' => 'stemmer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStopwords0()
    {
        return $this->hasOne(Stopwords::className(), ['id' => 'stopwords']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }        
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    
    public static function listaCategorias(){
        return ArrayHelper::getColumn(Textos::find()->select(['categoria'])->distinct()->all(),'categoria');
    }    

    public static function listaClasificaciones(){
        return ArrayHelper::getColumn(Textos::find()->select(['clasificacion'])->distinct()->all(),'clasificacion');
    }    
    
    public static function listaAutores(){
        return ArrayHelper::getColumn(Textos::find()->select(['autor'])->distinct()->all(),'autor');
    }    
   
}
