<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\User;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;
use common\models\PermisosHelpers;

use yii\web\UploadedFile;

use yii\helpers\Url;
use yii\imagine\Image;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use frontend\models\EstadosArchivos;

/**
 * This is the model class for table "proyectos_archivos".
 *
 * @property integer $id
 * @property integer $proyecto
 * @property string $nombre
 * @property string $contenido
 * @property integer $estado_archivo
 * @property string $adjunto
 * @property string $archivoadjunto
 * @property string $tipo_mime
 * @property integer $bytes
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property EstadosArchivos $estadoArchivo
 * @property Proyectos $proyecto0
 */
class ProyectosArchivos extends \yii\db\ActiveRecord
{
    public $archivoadjunto;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proyectos_archivos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proyecto', 'nombre', 'estado_archivo'], 'required'],
            [['proyecto', 'estado_archivo', 'bytes', 'created_by', 'updated_by'], 'integer'],
            [['contenido'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['nombre', 'adjunto', 'tipo_mime'], 'string', 'max' => 255],
            [['archivoadjunto'],'file','skipOnEmpty'=>true,
                'uploadRequired' => 'No has seleccionado ningún archivo', //Error
                'maxSize' => 1024*1024*2, //2 MB
                'tooBig' => 'El tamaño máximo permitido es 2MB', //Error
                'minSize' => 10, //10 Bytes
                'tooSmall' => 'El tamaño mínimo permitido son 10 BYTES','extensions'=>'txt,zip'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'proyecto' => Yii::t('app', 'Proyecto'),
            'nombre' => Yii::t('app', 'Nombre'),
            'contenido' => Yii::t('app', 'Contenido'),
            'estado_archivo' => Yii::t('app', 'Estado Archivo'),
            'adjunto' => Yii::t('app', 'Adjunto'),
            'tipo_mime' => Yii::t('app', 'Tipo Mime'),
            'bytes' => Yii::t('app', 'Bytes'),
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'created_by' => 'Creado Por',
            'updated_by' => 'Actualizado Por',
            'archivoadjunto'=>'Adjunto',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoArchivo()
    {
        return $this->hasOne(EstadosArchivos::className(), ['id' => 'estado_archivo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyecto0()
    {
        return $this->hasOne(Proyectos::className(), ['id' => 'proyecto']);
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }    

    
    public function upload(){

        if($this->archivoadjunto){
            $path=Url::to('@webroot'.Yii::$app->params['archivos']);
            $fileName = strToLower('adj_'.$this->proyecto.'_'.$this->archivoadjunto->baseName).'.'.$this->archivoadjunto->extension;
            $this->adjunto=$fileName;
            $this->archivoadjunto->saveAs($path.$fileName);
            
            //Image::frame($this->imageFile->tempName,20,'00FF00',100)->save($path.$fileName,['quality'=>90]);
            
            //Image::thumbnail($this->imageFile->tempName,180,180)->save($path.$fileName,['quality'=>90]);
        }else{
            //Yii::error(VarDumper::dumpAsString("no hay archivo adjunto?"));
        }
        return true;
    }
    
    public function beforeSave($insert){
        if (parent::beforeSave($insert)){
            $this->upload();
            return true;
        }else{
            return false;
        }
        
    }

    public function beforeUpdate($insert){
        if (parent::beforeUpdate($insert)){
            $this->upload();
            return true;
        }else{
            return false;
        }
        
    }
    
    public function beforeDelete(){
        if(parent::beforeDelete()){
            $path=Url::to('@webroot'.Yii::$app->params['archivos']);
            $fileName=$this->adjunto;
            if(is_file($path.$fileName)){
                unlink($path.$fileName);
            }
            return true;
        }else{
            return false;
        }
    }    
    
    public function getFileInfo(){
        
        $path=Url::to('@webroot'.Yii::$app->params['archivos']);
        $url=Url::to('@web'.Yii::$app->params['archivos']);
        $fileName=$this->adjunto;
        $alt = "Adjunto de id:".$this->id;
        $imageInfo=['alt'=>$alt];

        if(file_exists($path.$fileName)){
            $imageInfo['url']=$url.$fileName;
        }else{
            $imageInfo['url']=$url.'default.jpg';
        }    
        return $imageInfo;
        
    }
    
    public function getUrlAdjunto(){
        $url=Url::to('@web'.Yii::$app->params['archivos']);
        $fileName=$this->adjunto;
        return Html::a($this->adjunto,$url.$fileName,['download'=>$this->adjunto,$url.$fileName]);
    }

    public function getContenidoAdjunto(){
        $path=Url::to('@webroot'.Yii::$app->params['archivos']);
        $fileName=$this->adjunto;
        $contenido=null;
        if(file_exists($path.$fileName)){
            $contenido = file_get_contents($path.$fileName);
        }   
        return $contenido;
    }
    
    
    public function estadosPosibles(){
        
        $rows=[];
        $dropciones=[];
        if($this->isNewRecord){
            $dropciones=[EstadosArchivos::ESTADO_ACTIVO=>'Activo'];
        }else{
            $rows = EstadosArchivos::find()->select(['id','estado_archivo'])
                    ->orderBy('estado_valor')->all();
            $dropciones = ArrayHelper::map($rows, 'id', 'estado_archivo');
        }
        
        return $dropciones;
    }    
    
}
