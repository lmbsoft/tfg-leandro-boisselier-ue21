<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class AnonimizadorForm extends Model
{
    public $idArchivo;
    public $personas;
    public $palabras;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idArchivo'], 'required'],
            [['personas', 'palabras'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idArchivo' => 'Archivo',
            'personas' => 'Personas',
            'palabras' => 'Palabras',
        ];
    }
}
