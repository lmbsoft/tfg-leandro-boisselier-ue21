<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "stopwords".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $stopwords
 *
 * @property Textos[] $textos
 */
class Stopwords extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stopwords';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['stopwords'], 'string'],
            [['nombre'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'stopwords' => Yii::t('app', 'Stopwords'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTextos()
    {
        return $this->hasMany(Textos::className(), ['stopwords' => 'id']);
    }
}
