<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\User;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;
use common\models\PermisosHelpers;
use frontend\models\ProyectosPersonas;
/**
 * This is the model class for table "proyectos_mensajes".
 *
 * @property integer $id
 * @property integer $proyecto
 * @property string $fecha
 * @property integer $persona
 * @property string $texto_original
 * @property string $texto
 * @property string $emojis
 * @property string $adjuntos
 * @property integer $clasificacion
 * @property integer $es_punto_inflexion
 * @property double $lat
 * @property double $lng
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property ProyectosPersonas $persona0
 * @property Proyectos $proyecto0
 */
class ProyectosMensajes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proyectos_mensajes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proyecto', 'persona'], 'required'], //,'fecha'
            [['proyecto', 'persona', 'clasificacion', 'es_punto_inflexion', 'created_by', 'updated_by'], 'integer'],
            [[ 'created_at', 'updated_at'], 'safe'], //'fecha',
            [['xfecha'],'safe'],
            //['fecha', 'datetime', 'format' => 'php:Y-m-d H:i:s'],            
            [['texto_original','texto', 'emojis', 'adjuntos'], 'string'],
            [['lat', 'lng'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'proyecto' => Yii::t('app', 'Proyecto'),
            'fecha' => Yii::t('app', 'Fecha'),
            'xfecha' => Yii::t('app', 'Fecha'),
            'persona' => Yii::t('app', 'Persona'),
            'texto_original' => Yii::t('app', 'Texto Original'),
            'texto' => Yii::t('app', 'Texto'),
            'emojis' => Yii::t('app', 'Emojis'),
            'adjuntos' => Yii::t('app', 'Adjuntos'),
            'clasificacion' => Yii::t('app', 'Clasificacion'),
            'es_punto_inflexion' => Yii::t('app', 'Es Punto Inflexion'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'created_by' => 'Creado Por',
            'updated_by' => 'Actualizado Por',
        ];
    }

    public function getXfecha() {
        if (!empty($this->fecha) && $valor = \DateTime::createFromFormat("Y-m-d H:i:s", $this->fecha)) {
            return $valor->format("d-m-Y H:i:s");
        } else {
            return $this->fecha;
        }
    }

    public function setXfecha($value) {
        if (!empty($value) && $valor = \DateTime::createFromFormat("d-m-Y H:i:s", $value)) {
            $this->fecha = $valor->format("Y-m-d H:i:s");
        } else {
            $this->fecha = $value;
        }
    }
       
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona0()
    {
        return $this->hasOne(ProyectosPersonas::className(), ['id' => 'persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyecto0()
    {
        return $this->hasOne(Proyectos::className(), ['id' => 'proyecto']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }    


    public function personasPosibles(){
        
        $rows=[];
        $dropciones=[];
        $rows = ProyectosPersonas::find()->select(['id','persona'])
                ->where(['proyecto'=>$this->proyecto])
                ->orderBy('persona')->all();
        $dropciones = ArrayHelper::map($rows, 'id', 'persona');
        
        return $dropciones;
    }    
    
    
    public function valorEmocional(){
        
        $resultado = \Yii::$app->nlp->valorarSentimientos($this->texto);
        $sentimientoFloat = floatval($resultado->sentiment);
        $sentimientoEntero = round($sentimientoFloat);
        return $sentimientoEntero;

    }
}
