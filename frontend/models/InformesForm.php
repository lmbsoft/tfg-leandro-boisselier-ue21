<?php
namespace frontend\models;

use yii\base\Model;
use Yii;

class InformesForm extends Model
{
    public $fecha;
    public $fecha_desde;
    public $fecha_hasta;
    public $responsable;
    public $tipo_tarea;
    public $estado;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha','fecha_desde', 'fecha_hasta', 'responsable', 'tipo_tarea', 'estado'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fecha' => 'Fecha',
            'fecha_desde' => 'Fecha Desde',
            'fecha_hasta' => 'Fecha Hasta',
            'responsable' => 'Responsable',
            'tipo_tarea' => 'Tipo Tarea',
            'estado' => 'Estado',
          ];
      }

}
