<?php

namespace frontend\models;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\User;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;
use common\models\PermisosHelpers;
use frontend\models\EstadosProyectos;

/**
 * This is the model class for table "proyectos".
 *
 * @property integer $id
 * @property string $numero_proyecto
 * @property string $nombre
 * @property string $fechahora_inicio
 * @property integer $estado_proyecto
 * @property string $fechahora
 * @property string $observaciones
 * @property integer $stopwords
 * @property string $stopwords_lista
 * @property integer $stemmer
 * @property string $stemmer_lista
 * @property double $lat
 * @property double $lng
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property EstadosProyectos $estadoProyecto
 * @property Stemmers $stemmer0
 * @property Stopwords $stopwords0
 * @property ProyectosArchivos[] $proyectosArchivos
 * @property ProyectosPersonas[] $proyectosPersonas
 * @property ProyectosMensajes[] $proyectosMensajes
 */
class Proyectos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proyectos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['numero_proyecto', 'nombre', 'estado_proyecto'], 'required'],
            [['fechahora_inicio', 'fechahora', 'created_at', 'updated_at'], 'safe'],
            [['estado_proyecto', 'stopwords', 'stemmer', 'created_by', 'updated_by'], 'integer'],
            [['observaciones', 'stopwords_lista', 'stemmer_lista'], 'string'],
            [['lat', 'lng'], 'number'],
            [['xfecha_inicio','xfecha'],'safe'],
            [['numero_proyecto'], 'string', 'max' => 50],
            [['nombre'], 'string', 'max' => 255],
            [['numero_proyecto'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'numero_proyecto' => Yii::t('app', 'Numero Proyecto'),
            'nombre' => Yii::t('app', 'Nombre'),
            'fechahora_inicio' => Yii::t('app', 'Fecha hora Inicio'),
            'xfecha_inicio' => Yii::t('app', 'Fecha hora Inicio'),
            'estado_proyecto' => Yii::t('app', 'Estado Proyecto'),
            'fechahora' => Yii::t('app', 'Fecha hora'),
            'xfecha' => Yii::t('app', 'Fecha hora'),
            'observaciones' => Yii::t('app', 'Observaciones'),
            'stopwords' => Yii::t('app', 'Stopwords'),
            'stopwords_lista' => Yii::t('app', 'Stopwords Lista'),
            'stemmer' => Yii::t('app', 'Stemmer'),
            'stemmer_lista' => Yii::t('app', 'Stemmer Lista'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'created_by' => 'Creado Por',
            'updated_by' => 'Actualizado Por',
        ];
    }

    
   
    public function getXfecha_inicio() {
        if (!empty($this->fechahora_inicio) && $valor = \DateTime::createFromFormat("Y-m-d H:i:s", $this->fechahora_inicio)) {
            return $valor->format("d-m-Y H:i");
        } else {
            return $this->fechahora_inicio;
        }
    }

    public function setXfecha_inicio($value) {
        if (!empty($value) && $valor = \DateTime::createFromFormat("d-m-Y H:i", $value)) {
            $this->fechahora_inicio = $valor->format("Y-m-d H:i:s");
        } else {
            $this->fechahora_inicio = $value;
        }
    }
    
    
    public function getXfecha() {
        if (!empty($this->fechahora) && $valor = \DateTime::createFromFormat("Y-m-d H:i:s", $this->fechahora)) {
            return $valor->format("d-m-Y H:i");
        } else {
            return $this->fechahora;
        }
    }

    public function setXfecha($value) {
        if (!empty($value) && $valor = \DateTime::createFromFormat("d-m-Y H:i", $value)) {
            $this->fechahora = $valor->format("Y-m-d H:i:s");
        } else {
            $this->fechahora = $value;
        }
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoProyecto()
    {
        return $this->hasOne(EstadosProyectos::className(), ['id' => 'estado_proyecto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStemmer0()
    {
        return $this->hasOne(Stemmers::className(), ['id' => 'stemmer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStopwords0()
    {
        return $this->hasOne(Stopwords::className(), ['id' => 'stopwords']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyectosArchivos()
    {
        return $this->hasMany(ProyectosArchivos::className(), ['proyecto' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyectosPersonas()
    {
        return $this->hasMany(ProyectosPersonas::className(), ['proyecto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyectosMensajes()
    {
        return $this->hasMany(ProyectosMensajes::className(), ['proyecto' => 'id']);
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCantProyectosArchivos()
    {
        return $this->getProyectosArchivos()->count();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCantProyectosPersonas()
    {
        return $this->getProyectosPersonas()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCantProyectosMensajes()
    {
        return $this->getProyectosMensajes()->count();
    }
    
    public function estadosPosibles(){
        
        $rows=[];
        $dropciones=[];
        if($this->isNewRecord){
            $dropciones=[EstadosProyectos::ESTADO_INICIADO=>'Iniciado'];
        }else{
            $rows = EstadosProyectos::find()->select(['id','estado_proyecto'])
                    ->orderBy('estado_valor')->all();
            $dropciones = ArrayHelper::map($rows, 'id', 'estado_proyecto');
        }
        
        return $dropciones;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }         
}
