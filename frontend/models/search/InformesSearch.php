<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PermisosHelpers;

class InformesSearch extends Model{
    //public $q;
    public $p_id;
    public $p_numero_proyecto;
    public $p_fechahora_inicio;
    public $pm_fecha;
    public $latitud;
    public $longitud;
    public $fechaDesde;
    public $fechaHasta;
    public $dia_semana;
    public $nombre_dia;
    public $pm_hora;
    public $horario;
    public $pm_id_persona;
    public $r_persona;
    public $r_edad;
    public $r_genero;
    public $pm_texto_original;
    public $pm_clasificacion;
    public $pm_es_punto_inflexion;
    
    public $intervalo_minutos;
    
    public function rules()
    {
        return [
            //["q", "match", "pattern" => "/^[0-9a-záéíóúñ\s]+$/i", "message" => "Sólo se aceptan letras y números"],
            ['p_id','required'],
            [[ 
                //'p_id',
                'p_numero_proyecto',
                'p_fechahora_inicio',
                'pm_fecha',
                'latitud',
                'longitud',
                'fechaDesde',
                'fechaHasta',
                'dia_semana',
                'nombre_dia',
                'pm_hora',
                'horario',
                'pm_id_persona',
                'r_persona',
                'r_edad',
                'r_genero',
                'pm_texto_original',
                'pm_clasificacion',
                'pm_es_punto_inflexion',
            ], 'safe'],
            [['intervalo_minutos'],'integer'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            //'q' => "Buscar:",
                'p_id'=>'id Proyecto',
                'p_numero_proyecto'=>'p_numero_proyecto',
                'p_fechahora_inicio'=>'p_fechahora_inicio',
                'pm_fecha'=>'pm_fecha',
                'latitud'=>'latitud',
                'longitud'=>'longitud',
                'fechaDesde'=>'fechaDesde',
                'fechaHasta'=>'fechaHasta',
                'dia_semana'=>'dia_semana',
                'nombre_dia'=>'nombre_dia',
                'pm_hora'=>'pm_hora',
                'horario'=>'horario',
                'pm_id_persona'=>'pm_id_persona',
                'r_persona'=>'r_persona',
                'r_edad'=>'r_edad',
                'r_genero'=>'r_genero',
                'pm_texto_original'=>'pm_texto_original',
                'pm_clasificacion'=>'pm_clasificacion',
                'pm_es_punto_inflexion'=>'pm_es_punto_inflexion',
                'intervalo_minutos'=>'intervalo_minutos',
        ];
    }
}
