<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\ProyectosMensajes;
use common\models\PermisosHelpers;

/**
 * ProyectosMensajesSearch represents the model behind the search form about `frontend\models\ProyectosMensajes`.
 */
class ProyectosMensajesSearch extends ProyectosMensajes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'proyecto', 'persona', 'clasificacion', 'es_punto_inflexion', 'created_by', 'updated_by'], 'integer'],
            [['fecha', 'texto_original','texto', 'emojis','adjuntos', 'created_at', 'updated_at'], 'safe'],
            [['lat', 'lng'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;        
        $idProyecto = null;
        if(isset($_GET['proyecto'])){
            $this->proyecto=$_GET['proyecto'];
        }        
        
        $query = ProyectosMensajes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'proyecto' => $this->proyecto,
            'fecha' => $this->fecha,
            'persona' => $this->persona,
            'clasificacion' => $this->clasificacion,
            'es_punto_inflexion' => $this->es_punto_inflexion,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
        
        if(!$esAdmin){ //si no es admin refuerza el user id con el usuario logueado
            $query->andFilterWhere([
                'proyectos_mensajes.created_by' => $userId,
            ]);
        }  
        $query->andFilterWhere(['like', 'texto', $this->texto])
            ->andFilterWhere(['like', 'emojis', $this->emojis])
            ->andFilterWhere(['like', 'adjuntos', $this->emojis])
            ->andFilterWhere(['like', 'texto_original', $this->texto_original])
                ;

        return $dataProvider;
    }
}
