<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\ProyectosArchivos;
use common\models\PermisosHelpers;

/**
 * ProyectosArchivosSearch represents the model behind the search form about `frontend\models\ProyectosArchivos`.
 */
class ProyectosArchivosSearch extends ProyectosArchivos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'proyecto', 'estado_archivo', 'bytes', 'created_by', 'updated_by'], 'integer'],
            [['nombre', 'contenido', 'adjunto', 'tipo_mime', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;           
        $query = ProyectosArchivos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'proyecto' => $this->proyecto,
            'estado_archivo' => $this->estado_archivo,
            'bytes' => $this->bytes,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
        if(!$esAdmin){ //si no es admin refuerza el user id con el usuario logueado
            $query->andFilterWhere([
                'proyectos_archivos.created_by' => $userId,
            ]);
        }   
        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'contenido', $this->contenido])
            ->andFilterWhere(['like', 'adjunto', $this->adjunto])
            ->andFilterWhere(['like', 'tipo_mime', $this->tipo_mime]);

        return $dataProvider;
    }
}
