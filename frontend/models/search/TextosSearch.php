<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Textos;
use common\models\PermisosHelpers;

/**
 * TextosSearch represents the model behind the search form about `frontend\models\Textos`.
 */
class TextosSearch extends Textos
{
    /**
     * @inheritdoc
     */
    
    public function attributes(){
        return array_merge(parent::attributes(),['user.username']);
    }
    
    public function attributeLabels(){
        return array_merge(parent::attributeLabels(),['user.username'=>'usuario']);
    }
    
    public function rules()
    {
        return [
            [['id', 'stopwords', 'stemmer', 'publico', 'cantidad_topten', 'created_by', 'updated_by'], 'integer'],
            [['user','user.username'],'safe'],
            [['nombre', 'categoria', 'clasificacion', 'descripcion', 'autor', 'texto', 'stopwords_lista', 'stemmer_lista', 'observaciones', 'lugares', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;
        
        $query = Textos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('user');
        $query->andFilterWhere(
                ['LIKE','user.username',$this->getAttribute('user.username')]
        );        
        
        $query->andFilterWhere([
            'id' => $this->id,
            'stopwords' => $this->stopwords,
            'stemmer' => $this->stemmer,
            'publico' => $this->publico,
            'cantidad_topten' => $this->cantidad_topten,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        if(!$esAdmin){ //si no es admin refuerza el user id con el usuario logueado
            $query->andFilterWhere([
                'created_by' => $userId,
            ]);
        }
        
        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'categoria', $this->categoria])
            ->andFilterWhere(['like', 'clasificacion', $this->clasificacion])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'autor', $this->autor])
            ->andFilterWhere(['like', 'texto', $this->texto])
            ->andFilterWhere(['like', 'stopwords_lista', $this->stopwords_lista])
            ->andFilterWhere(['like', 'stemmer_lista', $this->stemmer_lista])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'lugares', $this->lugares]);

        return $dataProvider;
    }
}
