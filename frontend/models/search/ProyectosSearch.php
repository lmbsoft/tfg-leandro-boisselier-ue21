<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Proyectos;
use common\models\PermisosHelpers;
use frontend\models\EstadosProyectos;
use frontend\models\EstadosArchivos;

/**
 * ProyectosSearch represents the model behind the search form about `frontend\models\Proyectos`.
 */
class ProyectosSearch extends Proyectos
{

    /**
     * @inheritdoc
     */
    
    public function attributes(){
        return array_merge(parent::attributes(),['user.username','estadoProyecto.estado_proyecto']);
        
    }

    public function attributeLabels(){
        return array_merge(parent::attributeLabels(),['user.username','estadoProyecto.estado_proyecto'=>'Estado Proyecto'
            ]);
    }
    
    public function rules()
    {
        return [
            [['id', 'estado_proyecto', 'stopwords', 'stemmer', 'created_by', 'updated_by'], 'integer'],
            [['estadoProyecto.estado_proyecto','user.username'], 'safe'],            
            [['numero_proyecto', 'nombre', 'fechahora_inicio', 'fechahora', 'observaciones', 'stopwords_lista', 'stemmer_lista', 'created_at', 'updated_at'], 'safe'],
            [['lat', 'lng'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;        
        
        $query = Proyectos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('user');
        $query->andFilterWhere(
                ['LIKE','user.username',$this->getAttribute('user.username')]
        );        

        $query->andFilterWhere([
            'id' => $this->id,
            'fechahora_inicio' => $this->fechahora_inicio,
            'estado_proyecto' => $this->estado_proyecto,
            'fechahora' => $this->fechahora,
            'stopwords' => $this->stopwords,
            'stemmer' => $this->stemmer,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        if(!$esAdmin){ //si no es admin refuerza el user id con el usuario logueado
            $query->andFilterWhere([
                'proyectos.created_by' => $userId,
            ]);
        }        
        
        $query->andFilterWhere(['like', 'numero_proyecto', $this->numero_proyecto])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'stopwords_lista', $this->stopwords_lista])
            ->andFilterWhere(['like', 'stemmer_lista', $this->stemmer_lista]);

        return $dataProvider;
    }
}
