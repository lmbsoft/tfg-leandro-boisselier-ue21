<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\ProyectosPersonas;
use common\models\PermisosHelpers;

/**
 * ProyectosPersonasSearch represents the model behind the search form about `frontend\models\ProyectosPersonas`.
 */
class ProyectosPersonasSearch extends ProyectosPersonas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'proyecto', 'edad', 'created_by', 'updated_by'], 'integer'],
            [['persona', 'genero', 'variedad_espanol', 'nivel_educativo', 'observaciones', 'created_at', 'updated_at'], 'safe'],
            [['lat', 'lng'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;
        
        $query = ProyectosPersonas::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'proyecto' => $this->proyecto,
            'edad' => $this->edad,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        if(!$esAdmin){ //si no es admin refuerza el user id con el usuario logueado
            $query->andFilterWhere([
                'proyectos_personas.created_by' => $userId,
            ]);
        }
        
        $query->andFilterWhere(['like', 'persona', $this->persona])
            ->andFilterWhere(['like', 'genero', $this->genero])
            ->andFilterWhere(['like', 'variedad_espanol', $this->variedad_espanol])
            ->andFilterWhere(['like', 'nivel_educativo', $this->nivel_educativo])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones]);

        return $dataProvider;
    }
}
