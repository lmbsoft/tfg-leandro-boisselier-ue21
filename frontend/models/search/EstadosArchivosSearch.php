<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\EstadosArchivos;

/**
 * EstadosArchivosSearch represents the model behind the search form about `frontend\models\EstadosArchivos`.
 */
class EstadosArchivosSearch extends EstadosArchivos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'estado_valor'], 'integer'],
            [['estado_archivo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EstadosArchivos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'estado_valor' => $this->estado_valor,
        ]);

        $query->andFilterWhere(['like', 'estado_archivo', $this->estado_archivo]);

        return $dataProvider;
    }
}
