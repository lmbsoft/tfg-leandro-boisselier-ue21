<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "estados_proyectos".
 *
 * @property integer $id
 * @property string $estado_proyecto
 * @property integer $estado_valor
 *
 * @property Proyectos[] $proyectos
 */
class EstadosProyectos extends \yii\db\ActiveRecord
{
    const ESTADO_ANULADO = 1;
    const ESTADO_INICIADO = 2;
    const ESTADO_FINALIZADO = 3;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estados_proyectos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado_proyecto', 'estado_valor'], 'required'],
            [['estado_valor'], 'integer'],
            [['estado_proyecto'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'estado_proyecto' => Yii::t('app', 'Estado Proyecto'),
            'estado_valor' => Yii::t('app', 'Estado Valor'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyectos()
    {
        return $this->hasMany(Proyectos::className(), ['estado_proyecto' => 'id']);
    }
}
