<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class DiffForm extends Model
{
    public $texto1;
    public $texto2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['texto1', 'texto2'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'texto1' => 'Texto 1',
            'texto2' => 'Texto 2',
        ];
    }
}
