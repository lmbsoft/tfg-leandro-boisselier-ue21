<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "stemmers".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $palabras
 *
 * @property Textos[] $textos
 */
class Stemmers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stemmers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['palabras'], 'string'],
            [['nombre'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'palabras' => Yii::t('app', 'Palabras'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTextos()
    {
        return $this->hasMany(Textos::className(), ['stemmer' => 'id']);
    }
}
