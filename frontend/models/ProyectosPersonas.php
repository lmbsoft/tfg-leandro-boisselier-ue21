<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\User;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;
use common\models\PermisosHelpers;
/**
 * This is the model class for table "proyectos_personas".
 *
 * @property integer $id
 * @property integer $proyecto
 * @property string $persona
 * @property integer $edad
 * @property string $genero
 * @property string $variedad_espanol
 * @property string $nivel_educativo
 * @property string $observaciones
 * @property double $lat
 * @property double $lng
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property ProyectosMensajes[] $proyectosMensajes
 * @property Proyectos $proyecto0
*/
class ProyectosPersonas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proyectos_personas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proyecto', 'persona'], 'required'],
            [['proyecto', 'edad', 'created_by', 'updated_by'], 'integer'],
            [['observaciones'], 'string'],
            [['lat', 'lng'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['persona', 'genero', 'variedad_espanol', 'nivel_educativo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'proyecto' => Yii::t('app', 'Proyecto'),
            'persona' => Yii::t('app', 'Persona'),
            'edad' => Yii::t('app', 'Edad'),
            'genero' => Yii::t('app', 'Genero'),
            'variedad_espanol' => Yii::t('app', 'Variedad Espanol'),
            'nivel_educativo' => Yii::t('app', 'Nivel Educativo'),
            'observaciones' => Yii::t('app', 'Observaciones'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'created_by' => 'Creado Por',
            'updated_by' => 'Actualizado Por',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyecto0()
    {
        return $this->hasOne(Proyectos::className(), ['id' => 'proyecto']);
    }    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyectosMensajes()
    {
        return $this->hasMany(ProyectosMensajes::className(), ['persona' => 'id'])->orderBy('fecha');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCantProyectosMensajes()
    {
        return $this->getProyectosMensajes()->count();
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }    
   
    public static function listaGenero(){
        return ArrayHelper::getColumn(ProyectosPersonas::find()->select(['genero'])->distinct()->all(),'genero');
    }    
    
    public static function listaVariedadEspanol(){
        return ArrayHelper::getColumn(ProyectosPersonas::find()->select(['variedad_espanol'])->distinct()->all(),'variedad_espanol');
    }    
    
    public static function listaNivelEducativo(){
        return ArrayHelper::getColumn(ProyectosPersonas::find()->select(['nivel_educativo'])->distinct()->all(),'nivel_educativo');
    }    
    
}
