<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "estados_archivos".
 *
 * @property integer $id
 * @property string $estado_archivo
 * @property integer $estado_valor
 *
 * @property ProyectosArchivos[] $proyectosArchivos
 */
class EstadosArchivos extends \yii\db\ActiveRecord
{
    const ESTADO_ACTIVO = 1;
    const ESTADO_INACTIVO = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estados_archivos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado_archivo', 'estado_valor'], 'required'],
            [['estado_valor'], 'integer'],
            [['estado_archivo'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'estado_archivo' => Yii::t('app', 'Estado Archivo'),
            'estado_valor' => Yii::t('app', 'Estado Valor'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyectosArchivos()
    {
        return $this->hasMany(ProyectosArchivos::className(), ['estado_archivo' => 'id']);
    }
}
