<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

use common\models\User;

class PasswordForm extends Model{
    public $oldpass;
    public $newpass;
    public $repeatnewpass;
    
    public function rules(){
        return [
            [['oldpass', 'newpass','repeatnewpass'],'required'],
            ['oldpass','validatePassword'],//'findPassword'],
            ['repeatnewpass','compare','compareAttribute'=>'newpass','message'=>'Las nuevas contraseñas no son iguales'],
        ];
    }
    
    
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user =User::find()->where(['username'=>Yii::$app->user->identity->username])->one(); //$this->getUser();
            if (!$user || !$user->validatePassword($this->oldpass)) {
                $this->addError($attribute, 'La contraseña anterior es incorrecta');
            }
        }
    }
    
    public function findPassword($attribute, $params){
        $user = User::find()->where(['username'=>Yii::$app->user->identity->username])->one();
        $pw = $user->password;
        if($pw!=$this->oldpass){
            $this->addError($attribute,'La contraseña anterior es incorrecta');
        }
    }
    
    public function attributeLabels(){
        return [
            'oldpass'=>'Contraseña Anterior',
            'newpass'=>'Nueva Contraseña',
            'repeatnewpass'=>'Repetir nueva contraseña',
        ];
    }
    
}
