<?php
namespace frontend\assets;
 
use yii\web\AssetBundle;
 
/**
 * @author Float.la
 * @since 2.0
 */
class DateTimePickerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [ 
        'datetimepicker-master/build/css/bootstrap-datetimepicker.css',
    ];
    public $js = [
        'datetimepicker-master/moment-with-locales.min.js',
        'datetimepicker-master/build/js/bootstrap-datetimepicker.min.js',
        
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
    ];
}