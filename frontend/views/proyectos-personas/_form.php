<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\ArrayHelper;
use frontend\models\ProyectosPersonas;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProyectosPersonas */
/* @var $form yii\widgets\ActiveForm */

$generosData = ProyectosPersonas::listaGenero();
$niveleseducativosData = ProyectosPersonas::listaNivelEducativo();
$variedadespanolData = ProyectosPersonas::listaVariedadEspanol();
?>

<div class="proyectos-personas-form">
    <h3>Número de Proyecto: <?= $model->proyecto0->numero_proyecto ?></h3>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'proyecto')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'persona')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'edad')->textInput() ?>

    
    <?php if(count($generosData)>0): ?>
    <?= $form->field($model, 'genero')->textInput(['maxlength' => 255])
            ->widget(TypeaheadBasic::classname(), ['data'=>$generosData])
    ?>
    <?php else: ?>
        <?= $form->field($model, 'genero')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>
    
    <?php if(count($variedadespanolData)>0): ?>
    <?= $form->field($model, 'variedad_espanol')->textInput(['maxlength' => 255])
            ->widget(TypeaheadBasic::classname(), ['data'=>$variedadespanolData])
    ?>
    <?php else: ?>
        <?= $form->field($model, 'variedad_espanol')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>

    <?php if(count($niveleseducativosData)>0): ?>
    <?= $form->field($model, 'nivel_educativo')->textInput(['maxlength' => 255])
            ->widget(TypeaheadBasic::classname(), ['data'=>$niveleseducativosData])
    ?>
    <?php else: ?>
        <?= $form->field($model, 'nivel_educativo')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lat')->textInput() ?>

    <?= $form->field($model, 'lng')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
