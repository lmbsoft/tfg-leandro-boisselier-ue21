<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ProyectosPersonas */

$this->title = Yii::t('app', 'Agregar Persona a Proyecto');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proyectos Personas'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs']=[];

?>
<div class="proyectos-personas-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::a(Yii::t('app', 'Volver'), ['proyectos/view', 'id' => $model->proyecto], ['class' => 'btn btn-success']) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
