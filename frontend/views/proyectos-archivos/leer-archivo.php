<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProyectosArchivos */

$fileInfo = $model->fileInfo;
//$archivoadjunto = Html::a($model->adjunto,$fileInfo['url']);
$archivoadjunto = $model->urlAdjunto;

$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proyectos Archivos'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'] = [$this->title];

?>
<div class="proyectos-archivos-view">

    <h1> Lectura de archivo finalizada </h1>

    <p>
        <?= Html::a(Yii::t('app', 'Volver'), ['proyectos/view', 'id' => $model->proyecto], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'proyecto',
            'proyecto0.numero_proyecto',
            'nombre',
            'contenido:ntext',
            //'estado_archivo',
            'estadoArchivo.estado_archivo',
            'adjunto',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
    
    <?php if (!$model->isNewRecord) { 
          echo 'Descargar Archivo: '.$archivoadjunto; 
      }
    ?>   
</div>
