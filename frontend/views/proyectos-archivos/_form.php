<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProyectosArchivos */
/* @var $form yii\widgets\ActiveForm */
$fileInfo = $model->fileInfo;
//$archivoadjunto = Html::a($model->adjunto,$fileInfo['url']);
$archivoadjunto = $model->urlAdjunto;
?>

<div class="proyectos-archivos-form">

    <h2>Seleccione un archivo para incorporar al proyecto</h2>
    <h3>Número de Proyecto: <?= $model->proyecto0->numero_proyecto ?></h3>
    <?php //$form = ActiveForm::begin(); ?>
    <?php $form = ActiveForm::begin([
     "method" => "post",
     "enableClientValidation" => true,
     "options" => ["enctype" => "multipart/form-data"],
     ]);
    ?>    

    <?= $form->field($model, 'proyecto')->hiddenInput()->label(false) ?>
    
    <?php if (!$model->isNewRecord) { 
          echo 'Descargar archivo anterior: '.$archivoadjunto; 
      }
    ?>    
 
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contenido')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'estado_archivo')->dropDownList(
            //ArrayHelper::map( EstadosOrdenes::find()->orderBy('estado_valor')->All(), 'id','estado_orden'), [ 'prompt' => 'Seleccione Estado de la Orden' ]
            $model->estadosPosibles()
            , [ 'prompt' => 'Seleccione Estado del archivo' ]
    ) ?>

    <?= $form->field($model, 'archivoadjunto')->fileInput()->hint('Máximo 2Mb') ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
