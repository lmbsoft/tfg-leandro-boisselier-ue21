<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProyectosArchivos */

$fileInfo = $model->fileInfo;
//$archivoadjunto = Html::a($model->adjunto,$fileInfo['url']);
$archivoadjunto = $model->urlAdjunto;

$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proyectos Archivos'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'] = [$this->title];

?>
<div class="proyectos-archivos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Volver'), ['proyectos/view', 'id' => $model->proyecto], ['class' => 'btn btn-success']) ?>
        
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Leer archivo', ['leer-archivo', 'id' => $model->id], [
            'class' => 'btn btn-primary',
            'data' => [
                'confirm' => 'Reemplazar contenido con el archivo?',
                'method' => 'post',
            ],
        ]) ?> 
        
        <?= Html::a('Anonimizar', ['anonimizar', 'idArchivo' => $model->id], [
            'class' => 'btn btn-primary',
        ]) ?> 
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'proyecto',
            'proyecto0.numero_proyecto',
            'nombre',
            'contenido:ntext',
            //'estado_archivo',
            'estadoArchivo.estado_archivo',
            'adjunto',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'label' => 'Creado Por',
                'value' => RegistrosHelpers::getUserName($model->created_by)
            ],
            [
                'label' => 'Actualizado Por',
                'value' => RegistrosHelpers::getUserName($model->updated_by)
            ],            
        ],
    ]) ?>
    
    <?php if (!$model->isNewRecord) { 
          echo 'Descargar Archivo: '.$archivoadjunto; 
      }
    ?>   
</div>
