<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\ProyectosArchivosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proyectos-archivos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'proyecto') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'contenido') ?>

    <?= $form->field($model, 'estado_archivo') ?>

    <?php // echo $form->field($model, 'adjunto') ?>

    <?php // echo $form->field($model, 'tipo_mime') ?>

    <?php // echo $form->field($model, 'bytes') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
