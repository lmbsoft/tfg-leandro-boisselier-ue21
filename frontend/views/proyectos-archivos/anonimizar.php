<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\web\View;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model frontend\models\ProyectosArchivos */

$this->title = $anonimizar->idArchivo;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proyectos Archivos'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'] = [$this->title];

?>
<?= Html::a(Yii::t('app', 'Volver'), ['proyectos-archivos/view', 'id' => $anonimizar->idArchivo], ['class' => 'btn btn-success']) ?>

<div class="proyectos-archivos-view">
    <h2>Herramienta de reemplazo basada en sustituciones sobre el texto</h2>
    <div class="diferencias-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($anonimizar, 'idArchivo')->hiddenInput()->label(false) ?>
        <p class="alert alert-info">recuerde mantener el formato (cantidad) palabra <-> reemplazo<br/>
            No deje líneas vacías y si desea quitar personas o palabras puede hacerlo eliminando la línea completa
        </p>
        
        <?php echo $form->field($anonimizar, 'personas')->textArea(['rows'=>10, 'cols'=>50]) ?>
        <?= $form->field($anonimizar, 'palabras')->textArea(['rows'=>20, 'cols'=>50]) ?>

        <div class="form-group">
            <?= Html::submitButton('Anonimizar', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <p>Texto:</p>
    <?= nl2br($texto) ?>
    <!--
    <table class ="table table-bordered">
        <tr>
            <th>Palabra</th>
            <td>Cantidad</th>
        </tr>
        <?php foreach ($ocurrencias as $palabra=>$cantidad):?>
            <tr>
                <td><?=$palabra?></td>
                <td><?=$cantidad?></td>
            </tr>
        <?php endforeach;?>
    </table>
    -->
    
 
</div>
