<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

use frontend\assets\FontAwesomeAsset;
use common\models\PermisosHelpers;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
FontAwesomeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'Analizador de Comunicaciones Digitales <i class="fa fa-clone"></i>',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                ['label' => 'Inicio', 'url' => ['/site/index']],
                //['label' => 'About', 'url' => ['/site/about']],
                //['label' => 'Contact', 'url' => ['/site/contact']],
            ];
            if (Yii::$app->user->isGuest) {
                //$menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                if(PermisosHelpers::requerirMinimoRol('Usuario')){
//                    $menuItems[] = ['label' => 'Proyectos', 'items' => [
//                         ['label' => 'Proyectos', 'url' => ['/proyectos/index']],
//                         ['label' => 'Proyectos Archivos', 'url' => ['/proyectos-archivos/index']],
//                         ['label' => 'Proyectos Personas', 'url' => ['/proyectos-personas/index']],
//                         ['label' => 'Proyectos Mensajes', 'url' => ['/proyectos-mensajes/index']],
//                        ]];
                    
                    $menuItems[] = ['label' => 'Proyectos', 'url' => ['/proyectos/index']];
                    $menuItems[] = ['label' => 'Textos', 'url' => ['/textos/index']];
                    
                    $menuItems[] = ['label' => 'Recursos', 'items' => [
                         ['label' => 'Diferencias', 'url' => ['/textos/diferencias']],
                         ['label' => 'Stopwords', 'url' => ['/stopwords/index']],
                         ['label' => 'Stemmers', 'url' => ['/stemmers/index']],
                        ]];

                }
                /*
                if(PermisosHelpers::requerirMinimoRol('Usuario')){
                    $menuItems[] = ['label' => 'Informes', 'items' => [
                         ['label' => 'Stemmers', 'url' => ['/informes/stemmers']],
                         ['label' => 'Stopwords', 'url' => ['/informes/stopwords']],
                         ['label' => 'Textos', 'url' => ['/informes/textos']],
                        ]];
                }
                */
                if(PermisosHelpers::requerirMinimoRol('Admin')){
                    $menuItems[] = ['label' => 'Entidades', 'items' => [
                    ['label' => 'Estados Proyectos', 'url' => ['/estados-proyectos/index']],
                    ['label' => 'Estados Archivos', 'url' => ['/estados-archivos/index']],
                   ]];
                }

                $menuItems[] = ['label' => Yii::$app->user->identity->username, 'items' =>[
                    ['label' => 'Perfil', 'url' => ['/perfil/view']],
                    [
                       'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                       'url' => ['/site/logout'],
                       'linkOptions' => ['data-method' => 'post']
                    ],
                    ]];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; LMBSoft <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
