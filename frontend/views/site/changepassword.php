<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Cambiar Contraseña';
$this->params['breadcrumbs'][]=$this->title;

?>

<div class="site-changepassword">
    <h1><?= Html::encode($this->title);?></h1>
    <p>Comlete los siguientes campos para cambiar su contraseña</p>
    
    <?php $form=ActiveForm::begin([
        'id'=>'changepassword-form',
        'options'=>['class'=>'form-horizontal'],
        'fieldConfig'=>[
            'template'=>"{label}\n<div class=\"col-lg-3\">
                {input}</div>\n<div class=\"col-lg-5\">
                {error}</div>",
            'labelOptions'=>['class'=>'col-lg-2 control-label'],
        ],
    ]); ?>
    
    <?= $form->field($model,'oldpass',['inputOptions'=>['placeHolder'=>'Contraseña Anterior']])->passwordInput() ?>
    <?= $form->field($model,'newpass',['inputOptions'=>['placeHolder'=>'Nueva Contraseña']])->passwordInput() ?>
    <?= $form->field($model,'repeatnewpass',['inputOptions'=>['placeHolder'=>'Repetir Nueva Contraseña']])->passwordInput() ?>
    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-11">
            <?= Html::submitButton('Cambiar Contraseña',['class'=>'btn btn-primary']);?>
        </div>
    </div>
    <?php ActiveForm::end()?>
</div>