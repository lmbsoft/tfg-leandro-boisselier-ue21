<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Acerca de';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Esta es la página Acerca de.:</p>

    <code><?= __FILE__ ?></code>
</div>
