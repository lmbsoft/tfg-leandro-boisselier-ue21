<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Stopwords */

$this->title = Yii::t('app', 'Agregar Stopwords');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Stopwords'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stopwords-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
