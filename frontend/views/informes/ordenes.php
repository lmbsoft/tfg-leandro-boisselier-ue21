<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use common\models\PermisosHelpers;
use yii\helpers\ArrayHelper;
use frontend\models\Inmuebles;
use backend\models\TipoUsuario;
use frontend\models\TiposTareas;
use frontend\models\EstadosOrdenes;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\EstablecimientosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Lista de Ordenes de Trabajo');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ordenes-index">
<?php

$gridColumns=[
            ['class' => 'yii\grid\SerialColumn'],
           'id',
            'numero_orden',
            'descripcion',
            //'inmueble',
            [
                'attribute'=>'inmueble',
                'value' => 'inmueble0.nombre',
                'filter'=>ArrayHelper::map(Inmuebles::find()->orderBy('zona')->asArray()->all(), 
                               'id', 'nombre'),
            ],                  
            [
                'label' => 'x nombre',
                'attribute' => 'inmueble0.nombre',
            ],
            //'tipo_usuario',
            [
                'attribute'=>'tipo_usuario',
                'value' => 'tipoUsuario.tipo_usuario_nombre',
                'filter'=>ArrayHelper::map(TipoUsuario::find()->orderBy('tipo_usuario_valor')->asArray()->all(), 
                               'id', 'tipo_usuario_nombre'),
            ],                              
            //'tipoUsuario.tipo_usuario_nombre', //busca por descripción con like
            //'tipo_tarea',
            [
                'attribute'=>'tipo_tarea',
                'value' => 'tipoTarea.tipo_tarea',
                'filter'=>ArrayHelper::map(TiposTareas::find()->orderBy('id')->asArray()->all(), 
                               'id', 'tipo_tarea'),
            ],                              
            
            //'tipoTarea.tipo_tarea', //busca tipo de tarea por like
            'fechahora_inicio:datetime',
            //'estado_orden',
            [
                'attribute'=>'estado_orden',
                'value' => 'estado.estado_orden',
                'filter'=>ArrayHelper::map(EstadosOrdenes::find()->orderBy('estado_valor')->asArray()->all(), 
                               'id', 'estado_orden'),
            ],                              
            //'estado.estado_orden', //filtrar por descripción del estado orden
            'fechahora_fin:datetime',
            [
                'label' => 'Usuarios',
                'attribute' => 'cantOrdenesAsignaciones',
            ],
            // 'observaciones:ntext',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
        ];


    echo "<h2><span class='label label-success'>Exportar</span></h2>";
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns
    ]);
?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
    ]); ?>

</div>
