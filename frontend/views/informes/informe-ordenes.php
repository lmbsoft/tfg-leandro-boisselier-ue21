<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use common\models\PermisosHelpers;

use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\EstablecimientosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Informe de Ordenes con Detalles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="informe-ordenes">
<?php

$gridColumns=[
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'jornada',
            'observaciones',
            'descripcion',
            'fecha',
            'observaciones',
            'created_by',
            'username',
            'nombre',
            'apellido',
            'localidad',
            'telefono',
            'cargo',
            'nivel',
            'nombre_establecimiento',
            'direccion',
            'zona_supervisiva',
            'Capacitadores_Presentes',
            'Capacitadores_Ausentes',
            'Presentes',
            'Ausentes',
        ];


    echo "<h2><span class='label label-success'>Descargar Detalles</span></h2>";
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns
    ]);
?>

    <?php $form = ActiveForm::begin(['id' => 'cuadro_filtrar']); ?>
        <?= $form->field($informesForm, 'jornada')->checkboxList($ids_jornadas_data) ?>
        <?= $form->field($informesForm, 'username') ?>
        <?= $form->field($informesForm, 'nombre_establecimiento') ?>
        <?= $form->field($informesForm, 'nivel')->checkboxList($niveles_data) ?>
        <?= $form->field($informesForm, 'zona_supervisiva')->checkboxList($zonas_supervisivas_data) ?>
        <div class="form-group">
            <?= Html::submitButton('Filtrar', ['class' => 'btn btn-primary', 'name' => 'Filtrar']) ?>
        </div>
    <?php ActiveForm::end(); ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table table-bordered">
    <tr>
      <th>cue</th>
      <th>email</th>
      <th>nombre</th>
      <th>apellido</th>
      <th>telefono</th>
      <th>nivel</th>
      <th>localidad</th>
      <th>zona supervisiva</th>
      <th>datos calculados</th>
      <?php foreach($cabecera_jornadas as $cabjor): ?>
          <th><?= $cabjor['jornada'] ?> </th>
      <?php endforeach; ?>
    </tr>

    <?php foreach($cabecera_cues as $cc => $cj): ?>
    <tr>
      <td><?= $cj['username'] ?></td>
      <td><?= $cj['email'] ?></td>
      <td><?= $cj['nombre'] ?></td>
      <td><?= $cj['apellido']?></td>
      <td><?= $cj['telefono']?></td>
      <td><?= $cj['nivel']?></td>
      <td><?= $cj['localidad']?></td>
      <td><?= $cj['zona_supervisiva']?></td>
      <td>Fechas:<br>C.Presentes:<br>C.Ausentes:<br>Presentes:<br>Ausentes:<br></td>
      <?php foreach($cabecera_jornadas as $cabjor): ?>
      <td>
          <?php if(isset($cuadro_jornadas[$cj['username']][$cabjor['id']]['cantidad'])): ?>
            <?php echo $cuadro_jornadas[$cj['username']][$cabjor['id']]['cantidad'] ?> <br>
            <?php echo $cuadro_jornadas[$cj['username']][$cabjor['id']]['Capacitadores_Presentes'] ?> <br>
            <?php echo $cuadro_jornadas[$cj['username']][$cabjor['id']]['Capacitadores_Ausentes'] ?> <br>
            <?php echo $cuadro_jornadas[$cj['username']][$cabjor['id']]['Presentes'] ?> <br>
            <?php echo $cuadro_jornadas[$cj['username']][$cabjor['id']]['Ausentes'] ?><br>
          <?php else: ?>
            &nbsp;
          <?php endif; ?>
      </td>
    <?php endforeach;?>

    </tr>
    <?php endforeach; ?>
    </table>
    

</div>
