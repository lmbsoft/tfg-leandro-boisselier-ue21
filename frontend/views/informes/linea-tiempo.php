<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use common\models\PermisosHelpers;
use yii\helpers\ArrayHelper;
use frontend\models\Inmuebles;
use backend\models\TipoUsuario;
use frontend\models\TiposTareas;
use frontend\models\EstadosOrdenes;
use yii\jui\DatePicker;

use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\EstablecimientosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Línea de Tiempo');
//$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'] = null //[] = $this->title;
?>
<div class="linea-tiempo">
<?php

$gridColumns=[
            ['class' => 'yii\grid\SerialColumn'],
                    'id_orden',
                    'numero_orden',
                    'descripcion',
                    'inmueble',
                    'nombre_inmueble',
                    'tipo_usuario',
                    'tipo_usuario_nombre',
                    'id_tipo_tarea',
                    'tipo_tarea',
                    'fechahora_inicio',
                    'id_estado_orden',
                    'estado_orden',
                    'fechahora_fin',
                    'observaciones_orden',
                    'id_orden_asignacion',
                    'id_responsable',
                    'username_responsable',
                    'responsable_nombre',
                    'responsable_apellido',
                    'id_estado_asignacion',
                    'estado_asignacion',
                    'fecha_ultimo_estado',
                    'observaciones_asignacion',
        ];

?>
    <h2 style="text-align:center;" class="alert alert-info">Línea de Tiempo de órdenes Tomadas por usuario</h2>

    <div class="row">
        <?php $form = ActiveForm::begin(['id' => 'cuadro_filtrar',
            'method'=>'get']);  //tiene que quedar en get para que funcione el export del widget
        ?>
        <?= $form->field($informesForm, 'responsable')->dropDownList($ids_usuarios_data, ['prompt' => 'Por favor Seleccione Un responsable' ]); ?>
        <label>Fecha Desde</label>
        <?php //echo $form->field($informesForm, 'fecha_desde') ?>
        <?php
            echo DatePicker::widget([
                  'model' => $informesForm,
                  'attribute' => 'fecha_desde',
                  'language'=>'es',
                  'dateFormat'=>'dd/MM/yyyy',
            ]);
        ?>            
        <label>Fecha Hasta</label>
        <?php // $form->field($informesForm, 'fecha_hasta') ?>
        <?php
            echo DatePicker::widget([
                  'model' => $informesForm,
                  'attribute' => 'fecha_hasta',
                  'language'=>'es',
                  'dateFormat'=>'dd/MM/yyyy',
            ]);
        ?>                  
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= Html::submitButton('Filtrar', ['class' => 'btn btn-primary', 'name' => 'Filtrar']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?php /*echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
    ]); */?>
    
    <?php 
    $resultados = $query->all();
    ?>
    <?php if(count($resultados)>0):?>
    <table class="table table-bordered">
        <tr>
            <th>Fecha asignación</th>
            <th>Estado</th>
            <th>Detalles Orden</th>

            <th>Orden</th>

        </tr>
        <?php foreach($resultados as $r):?>
        <tr>
            <td><?= $r['fecha_ultimo_estado'] ?></td>
            <td><?= $r['estado_asignacion'] ?></td>
            <td><?= $r['observaciones_asignacion'] ?></td>
            <td><?php //echo $r['id_orden'] ?>
                Orden:<?= $r['numero_orden'] ?> : <?= $r['fechahora_inicio'] ?> <br/>
                <?= $r['descripcion'] ?><br/>
                Inmueble:<?= $r['nombre_inmueble'] ?><br/>
                Tipo Tarea:<?= $r['tipo_tarea'] ?><br/>
                Estado Orden:<?= $r['estado_orden'] ?><br/>
                Observaciones<br/>
                    <?= nl2br($r['observaciones_orden'])?>
            </td>
            
        </tr>    
        <?php endforeach; ?>
    </table>
    <?php endif;?>

</div>
