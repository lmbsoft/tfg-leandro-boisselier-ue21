<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use common\models\PermisosHelpers;
use yii\helpers\ArrayHelper;
use frontend\models\Inmuebles;
use backend\models\TipoUsuario;
use frontend\models\TiposTareas;
use frontend\models\EstadosOrdenes;
use yii\jui\DatePicker;

use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\EstablecimientosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Detalles de Ordenes de Trabajo');
//$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'] = null; //[] = $this->title;
?>
<div class="detalles-index">
    <h2 style="text-align:center;" class="alert alert-info"><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
<?php

$gridColumns=[
            ['class' => 'yii\grid\SerialColumn'],
                    'id_orden',
                    'numero_orden',
                    'descripcion',
                    'inmueble',
                    'nombre_inmueble',
                    'tipo_usuario',
                    'tipo_usuario_nombre',
                    'id_tipo_tarea',
                    'tipo_tarea',
                    'fechahora_inicio',
                    'id_estado_orden',
                    'estado_orden',
                    'fechahora_fin',
                    'observaciones_orden',
                    'id_orden_asignacion',
                    'id_responsable',
                    'username_responsable',
                    'responsable_nombre',
                    'responsable_apellido',
                    'id_estado_asignacion',
                    'estado_asignacion',
                    'fecha_ultimo_estado',
                    'observaciones_asignacion',
        ];

?>
    <div class="row">
        <?php $form = ActiveForm::begin(['id' => 'cuadro_filtrar',
            'method'=>'get']);  //tiene que quedar en get para que funcione el export del widget
        ?>
        <div class="col-md-3">
            <?= $form->field($informesForm, 'responsable')->checkboxList($ids_usuarios_data) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($informesForm, 'tipo_tarea')->checkboxList($ids_tipostarea_data) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($informesForm, 'estado')->checkboxList($ids_estados_data) ?>
        </div>
        <div class="col-md-3">
            <label>Fecha Desde</label>
            <?php //echo $form->field($informesForm, 'fecha_desde') ?>
            <?php
                echo DatePicker::widget([
                      'model' => $informesForm,
                      'attribute' => 'fecha_desde',
                      'language'=>'es',
                      'dateFormat'=>'dd/MM/yyyy',
                ]);
            ?>            
            <label>Fecha Hasta</label>
            <?php // $form->field($informesForm, 'fecha_hasta') ?>
            <?php
                echo DatePicker::widget([
                      'model' => $informesForm,
                      'attribute' => 'fecha_hasta',
                      'language'=>'es',
                      'dateFormat'=>'dd/MM/yyyy',
                ]);
            ?>                  
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= Html::submitButton('Filtrar', ['class' => 'btn btn-primary', 'name' => 'Filtrar']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <?php

            echo "<span class='label label-success'>Exportar</span>";
            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns
            ]);
        ?>
    </div>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
    ]); ?>

</div>
