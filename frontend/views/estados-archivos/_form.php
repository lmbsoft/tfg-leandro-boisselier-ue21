<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\EstadosArchivos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estados-archivos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'estado_archivo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado_valor')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
