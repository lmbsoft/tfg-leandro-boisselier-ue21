<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\EstadosArchivos */

$this->title = Yii::t('app', 'Agregar Estados Archivos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estados Archivos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estados-archivos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
