<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
use yii\grid\GridView;

use yii\data\ActiveDataProvider;
use frontend\models\ProyectosArchivos;
use frontend\models\ProyectosMensajes;
use frontend\models\ProyectosPersonas;
use frontend\models\EstadosProyectos;

$show_this_nav = PermisosHelpers::requerirMinimoRol('Usuario');
/* @var $this yii\web\View */
/* @var $model frontend\models\Proyectos */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proyectos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$estadoIniciado = $model->estado_proyecto == EstadosProyectos::ESTADO_INICIADO;

?>
<div class="proyectos-view">

    <h2 style="text-align:center" class="alert alert-info">Proyecto: <?= Html::encode($model->numero_proyecto) ?></h2>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?php if($estadoIniciado): ?>
            <?= Html::a('Procesar', ['procesar', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Se sobreescribirán todos los mensajes existentes',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Valorar', ['valorar', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Se sobreescribirán todos los valores emocionales de los mensajes existentes',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Puntos Inflexión', ['determinar-puntos-inflexion', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Se sobreescribirán todos los puntos de inflexión ya detectados, confirma?',
                'method' => 'post',
            ],
        ]) ?>
       <?php endif; ?>
        
    </p>
    <p>
        <?php //echo Html::a('Data', ['data', 'id' => $model->id], ['class' => 'btn btn-primary',]) ?>        
        <?= Html::a('Heat Map', ['heatmap', 'p_id' => $model->id], ['class' => 'btn btn-primary',]) ?>
        <?= Html::a('Mensajes x Persona', ['mensajesxpersona', 'p_id' => $model->id], ['class' => 'btn btn-primary',]) ?>
        <?= Html::a('Estadísticas', ['estadisticas', 'p_id' => $model->id], ['class' => 'btn btn-primary',]) ?>
        <?= Html::a('Mensajería', ['mensajeria', 'p_id' => $model->id], ['class' => 'btn btn-primary',]) ?>
        <?= Html::a('Mensajería Totales', ['mensajeria-totales', 'p_id' => $model->id], ['class' => 'btn btn-primary',]) ?>
        <?= Html::a('Mensajería Línea de Tiempo', ['mensajeria-linea-tiempo', 'p_id' => $model->id], ['class' => 'btn btn-primary',]) ?>
    </p>
    
    
    <div class="row">
        <div class="col-md-6">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'id',
                'user.username',
                'numero_proyecto',
                'nombre',
                //'fechahora_inicio:datetime',
                'xfecha_inicio',
                //'estado_proyecto',
                'estadoProyecto.estado_proyecto',
                //'fechahora:datetime',
                'xfecha',
                'observaciones:ntext',
                'stopwords0.nombre',
                'stopwords_lista:ntext',
                'stemmer0.nombre',
                'stemmer_lista:ntext',
                'lat',
                'lng',
                'created_at:datetime',
                'updated_at:datetime',
                [
                    'label' => 'Creado Por',
                    'value' => RegistrosHelpers::getUserName($model->created_by)
                ],
                [
                    'label' => 'Actualizado Por',
                    'value' => RegistrosHelpers::getUserName($model->updated_by)
                ],
            ],
        ]) ?>
        </div>
        <div class="col-md-6">

        <?php 
        if (!Yii::$app->user->isGuest ){ //&& $show_this_nav
        ?>    
        <p>
            <?= Html::a(Yii::t('app', 'Agregar Archivo'), ['proyectos-archivos/create', 'proyecto'=>$model->id], ['class' => 'btn btn-success']) ?>
        </p>    
        <?php 
        }else{
            echo "<h2>Archivos</h2>";
        };
        ?>

        <?php
            $query = ProyectosArchivos::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination'=>false,
            ]);
            $query->andWhere(['proyecto'=>$model->id]);
        ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
    //            'id',
                'nombre',
                'adjunto',
                'urlAdjunto:raw', 
    //            'tipo_mime',
    //            'bytes',

                ['class' => 'yii\grid\ActionColumn',
                     'template' => $show_this_nav? '{view} {update} {delete}':'{view} {update} {delete}','controller' => 'proyectos-archivos'
                ],            
            ],
        ]); ?>


        <?php 
        if (!Yii::$app->user->isGuest ){ //&& $show_this_nav
        ?>    
        <p>
            <?= Html::a(Yii::t('app', 'Agregar Persona'), ['proyectos-personas/create', 'proyecto'=>$model->id], ['class' => 'btn btn-success']) ?>
        </p>    
        <?php 
        }else{
            echo "<h2>Personas Asociadas</h2>";
        };
        ?>

        <?php
            $query = ProyectosPersonas::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination'=>false,
            ]);
            $query->andWhere(['proyecto'=>$model->id]);
        ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'persona',
                [
                    'label' => 'Mensajes',
                    'attribute' => 'cantProyectosMensajes',
                ],
               ['class' => 'yii\grid\ActionColumn',
                    'template' => $show_this_nav? '{view} {update} {delete}':'{view} {update} {delete}','controller' => 'proyectos-personas'
               ],            
            ],
        ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        
        
        <?php 
        if (!Yii::$app->user->isGuest ){ //&& $show_this_nav
        ?>    
        <p>
            <?= Html::a(Yii::t('app', 'Agregar Mensaje'), ['proyectos-mensajes/create', 'proyecto'=>$model->id], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Grilla'), ['proyectos-mensajes/index', 'proyecto' => $model->id], ['class' => 'btn btn-success','target'=>'_blank']) ?>
        </p>    
        <?php 
        }else{
            echo "<h2>Mensajes Asociados</h2>";
        };
        ?>

        <?php
            $query = ProyectosMensajes::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination'=>false,
            ]);
            $query->andWhere(['proyecto'=>$model->id]);
        ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
            //'fecha',
            'xfecha',
            'persona0.persona',
            'emojis:ntext',
            'adjuntos:ntext',
            'texto:ntext',
            'clasificacion',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $show_this_nav? '{view} {update} {delete}':'{view} {update} {delete}','controller' => 'proyectos-mensajes'
            ],
                
            ],
        ]); ?>            

        </div>
    </div>
</div>