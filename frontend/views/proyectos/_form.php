<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use frontend\models\Stemmers;
use frontend\models\Stopwords;
/* @var $this yii\web\View */
/* @var $model frontend\models\Proyectos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proyectos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numero_proyecto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?php //echo  $form->field($model, 'fechahora_inicio')->textInput(); ?>
    <div class="input-group date">
        Fecha Hora de Inicio:
        <?php
        echo \kartik\datetime\DateTimePicker::widget([
            'name' => 'Proyectos[xfecha_inicio]',
            'id' => 'xfecha_inicio',
            'type' => \kartik\datetime\DateTimePicker::TYPE_INPUT,
            'convertFormat' => true,
            'readonly' => true,
            'value' => (isset($model->xfecha_inicio)) ? $model->xfecha_inicio : "",
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'php:d-m-Y H:i',
                'locale' => 'es',
                'language' => 'es'
            ]
        ]);
        ?>
    </div>
    
    

    <?= $form->field($model, 'estado_proyecto')->dropDownList(
            //ArrayHelper::map( EstadosOrdenes::find()->orderBy('estado_valor')->All(), 'id','estado_orden'), [ 'prompt' => 'Seleccione Estado de la Orden' ]
            $model->estadosPosibles()
            , [ 'prompt' => 'Seleccione Estado del Proyecto' ]
    ) ?>

    <?php //echo $form->field($model, 'fechahora')->textInput(); ?>
    <div class="input-group date">
        Fecha y Hora:
        <?php
        echo \kartik\datetime\DateTimePicker::widget([
            'name' => 'Proyectos[xfecha]',
            'id' => 'xfecha',
            'type' => \kartik\datetime\DateTimePicker::TYPE_INPUT,
            'convertFormat' => true,
            'readonly' => true,
            'value' => (isset($model->xfecha)) ? $model->xfecha : "",
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'php:d-m-Y H:i',
                'locale' => 'es',
                'language' => 'es'
            ]
        ]);
        ?>
    </div>
 
    <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'stopwords')->DropDownList(
            ArrayHelper::map(Stopwords::find()->orderBy('nombre')->All(),'id','nombre'),['prompt'=>'Seleccione una lista de Stopwords']
            ) ?>

    <?= $form->field($model, 'stopwords_lista')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'stemmer')->DropDownList(
            ArrayHelper::map(Stemmers::find()->orderBy('nombre')->All(),'id','nombre'),['prompt'=>'Seleccinoes una lista de Stemmer']
            ) ?>

    <?= $form->field($model, 'stemmer_lista')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lat')->textInput() ?>

    <?= $form->field($model, 'lng')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
