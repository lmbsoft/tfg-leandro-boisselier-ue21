<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use frontend\models\EstadosProyectos;
use common\models\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ProyectosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Proyectos');
$this->params['breadcrumbs'][] = $this->title;

$es_admin = PermisosHelpers::requerirMinimoRol('Admin');
$template = '{view}{update}{delete}';
//if (!$es_admin){
//    $template = '{view}{update}';
//}
?>

<div class="proyectos-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Agregar Proyecto'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{pager}\n{summary}\n{items}\n{pager}",        
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user.username',
            'numero_proyecto',
            'nombre',
            'fechahora_inicio:datetime',
            //'estadoProyecto.estado_proyecto',
            [
                'attribute'=>'estado_proyecto',
                'value' => 'estadoProyecto.estado_proyecto',
                'filter'=>ArrayHelper::map(EstadosProyectos::find()->orderBy('estado_valor')->asArray()->all(), 
                               'id', 'estado_proyecto'),
            ],
            [
                'label' => 'Mensajes',
                'attribute' => 'cantProyectosMensajes',
            ],            
            //            //'fechahora:datetime',
            // 'observaciones:ntext',
            // 'stopwords',
            // 'stopwords_lista:ntext',
            // 'stemmer',
            // 'stemmer_lista:ntext',
            // 'lat',
            // 'lng',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            [   
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => $template,
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
        'pager' => [
            'options'=>['class'=>'pagination'],   // set clas name used in ui list of pagination
            'prevPageLabel' => '<',   // Set the label for the "previous" page button
            'nextPageLabel' => '>',   // Set the label for the "next" page button
            'firstPageLabel'=>'<<',   // Set the label for the "first" page button
            'lastPageLabel'=>'>>',    // Set the label for the "last" page button
            'nextPageCssClass'=>'next',    // Set CSS class for the "next" page button
            'prevPageCssClass'=>'prev',    // Set CSS class for the "previous" page button
            'firstPageCssClass'=>'first',    // Set CSS class for the "first" page button
            'lastPageCssClass'=>'last',    // Set CSS class for the "last" page button
            'maxButtonCount'=>10,    // Set maximum number of page buttons that can be displayed
        ],
    ]); ?>

</div>
