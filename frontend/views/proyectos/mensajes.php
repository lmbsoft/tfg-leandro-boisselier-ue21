<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use common\models\PermisosHelpers;

use kartik\typeahead\TypeaheadBasic;

use yii\web\View;


$esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
$userId = \Yii::$app->user->identity->id;        

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\EstablecimientosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mensajes');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1>Listado de mensajes del sistema Mensajería</h1>

<?php $f = ActiveForm::begin([
    "method" => "get",
    //"action" => Url::toRoute("proyectos/mensajeria"),
    //"enableClientValidation" => true,
    ]);
?>
<h2><span class="label label-primary" id="boton-filtro">Filtrar</span>
<?= Html::a('Volver', ['view', 'id' => $form->p_id], ['class' => 'btn btn-primary']) ?>
</h2>
<div class="row">
<div class="form-group">
    <div class="col-md-6">
        <?= $f->field($form, 'p_id')->hiddenInput()->label(false) ?>
        <?= $f->field($form, 'fechaDesde')->textInput()->hint('Use el formato AAAA-MM-DD') ?>
        <?= $f->field($form, 'fechaHasta')->textInput()->hint('Use el formato AAAA-MM-DD') ?>
        <?= $f->field($form, 'r_persona')->textInput(['maxlength' => true])?>
    </div>
    <div class="col-md-6">
        <?= $f->field($form, 'pm_texto_original')->textInput(['maxlength' => true])?>
        <?= $f->field($form, 'pm_id_persona')->checkboxList($ids_personas_data) ?>
    </div>
</div>
</div>
<?= Html::submitButton("Filtrar", ["class" => "btn btn-primary"]) ?>
<?php $f->end() ?>

<div class="mensajeria-index">
<?php

$gridColumns=[
            ['class' => 'yii\grid\SerialColumn'],
                'p_id',
                'p_numero_proyecto',
                'latitud',
                'longitud',
                'p_fechahora_inicio:datetime', //:datetime
                'pm_fecha:date',
                'dia_semana',
                'nombre_dia',
                'pm_hora',
                'horario',
                'pm_id_persona',
                'r_persona',
                'r_edad',
                'r_genero',
                'pm_texto_original:raw', //:ntext
                'pm_texto:raw', //:ntext
                'pm_clasificacion',
                'pm_es_punto_inflexion:boolean',
        ];


    echo "<h2><span class='label label-success'>Exportar</span></h2>";
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns
    ]);    
?>    
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
    ]); ?>

</div>

<?php
$this->registerJs(<<< EOF_JS
   var formw0 = $('#w0');
   formw0.on('submit', function(e) {
      return formw0.yiiActiveForm('submitForm');
   }); 
   $('#boton-filtro').on('click', function() { 
      formw0.submit();
   });
EOF_JS
,View::POS_READY);
?>
