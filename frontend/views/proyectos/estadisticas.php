<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
use yii\grid\GridView;

use yii\data\ActiveDataProvider;
use frontend\models\ProyectosArchivos;
use frontend\models\ProyectosMensajes;
use frontend\models\ProyectosPersonas;
use frontend\models\EstadosProyectos;
use yii\helpers\VarDumper;

use yii\web\View;


$esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
$userId = \Yii::$app->user->identity->id;  

$show_this_nav = PermisosHelpers::requerirMinimoRol('Usuario');
/* @var $this yii\web\View */
/* @var $model frontend\models\Proyectos */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proyectos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$estadoIniciado = $model->estado_proyecto == EstadosProyectos::ESTADO_INICIADO;

?>
<div class="proyectos-view">
    
<h1>Estadísticas</h1>

    <?php $f = ActiveForm::begin([
        "method" => "get",
        //"action" => Url::toRoute("proyectos/mensajeria"),
        //"enableClientValidation" => true,
        ]);
    ?>
    <?= Html::submitButton("Filtrar", ["id"=>"boton-filtro","class" => "btn btn-primary"]) ?>
    <?= Html::a('Volver', ['view', 'id' => $form->p_id], ['class' => 'btn btn-primary']) ?>
    </h2>
    <div class="row">
    <div class="form-group">
        <div class="col-md-6">
            <?= $f->field($form, 'p_id')->hiddenInput()->label(false) ?>
            <?= $f->field($form, 'fechaDesde')->textInput()->hint('Use el formato AAAA-MM-DD') ?>
            <?= $f->field($form, 'fechaHasta')->textInput()->hint('Use el formato AAAA-MM-DD') ?>
            <?= $f->field($form, 'r_persona')->textInput(['maxlength' => true])?>
        </div>
        <div class="col-md-6">
            <?= $f->field($form, 'pm_texto_original')->textInput(['maxlength' => true])?>
            <?= $f->field($form, 'pm_id_persona')->checkboxList($ids_personas_data) ?>
        </div>
    </div>
    </div>
    <?= Html::submitButton("Filtrar", ["class" => "btn btn-primary"]) ?>
    <?php $f->end() ?>


    <h1>Respuesta</h1>
    <pre>
    <?php
        //echo VarDumper::dumpAsString($resultado); //para gráficos
        
        //print_r($resultado); //para Data

        //print_r( json_decode($resultado));
        #print_r($palabras_mas_usadas);
        #print_r($palabras_por_persona);
        #print_r($emojis_mas_usados);
    ?>
    </pre>
    <h2>Palabras más usadas</h2>
    <table class="table table-bordered">
        <tr><th>Palabra</th><th>Cantidad</th></tr>
        <?php foreach($palabras_mas_usadas as $fila => $palabra_cantidad): ?>
            <tr>
                <td><?=$palabra_cantidad[0]?></td><td><?=$palabra_cantidad[1]?></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <h2>Palabras por persona</h2>
    <table class="table table-bordered">
        <tr><th>Persona</th><th>Gráfico</th></tr>
        <?php foreach($palabras_por_persona as $persona => $grafico): ?>
            <tr>
                <td><?=$persona?></td><td><?=$grafico?></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <h2>Ranking de Emojis por mensajes</h2>
    <!--<pre>
        <?php
           // print_r($emojis_mas_usados); //[('👍', ':pulgar_hacia_arriba:')] => 84
        ?>
    </pre>-->
    <table class="table table-bordered">
        <tr><th>Emoji</th><th>Nombre</th><th>Cantidad de mensajes</th></tr>

        
    <?php foreach($emojis_mas_usados as $emoji => $cantidad ): ?>
        <tr>
            <?php
                $emojistr =  preg_replace("/['():]/", "", $emoji); 
                $emoji_nombre = mb_split(",",$emojistr);
            ?>
            <td style="font-size:2em;"><?php //echo $emoji['emoji']?><?php echo $emoji_nombre[0]?></td>
            <td><?php echo $emoji_nombre[1];//echo $emoji['emojiname']?></td>
            <td><?php echo $cantidad; ?></td>
            <!-- <td style="font-size:2em;"><?php //echo $emoji?></td><td>&nbsp;</td><td><?php //echo $cantidad?></td> -->
        </tr>
    <?php endforeach; ?>
    </table>
        

</div>

