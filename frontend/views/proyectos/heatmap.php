<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
use yii\grid\GridView;

use yii\data\ActiveDataProvider;
use frontend\models\ProyectosArchivos;
use frontend\models\ProyectosMensajes;
use frontend\models\ProyectosPersonas;
use frontend\models\EstadosProyectos;
use yii\helpers\VarDumper;

use yii\web\View;


$esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
$userId = \Yii::$app->user->identity->id;  

$show_this_nav = PermisosHelpers::requerirMinimoRol('Usuario');
/* @var $this yii\web\View */
/* @var $model frontend\models\Proyectos */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proyectos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$estadoIniciado = $model->estado_proyecto == EstadosProyectos::ESTADO_INICIADO;

?>
<div class="proyectos-view">
    
<h1>Listado de mensajes del sistema Mensajería</h1>

    <?php $f = ActiveForm::begin([
        "method" => "get",
        //"action" => Url::toRoute("proyectos/mensajeria"),
        //"enableClientValidation" => true,
        ]);
    ?>
    <?= Html::submitButton("Filtrar", ["id"=>"boton-filtro","class" => "btn btn-primary"]) ?>
    <?= Html::a('Volver', ['view', 'id' => $form->p_id], ['class' => 'btn btn-primary']) ?>
    </h2>
    <div class="row">
    <div class="form-group">
        <div class="col-md-6">
            <?= $f->field($form, 'p_id')->hiddenInput()->label(false) ?>
            <?= $f->field($form, 'fechaDesde')->textInput()->hint('Use el formato AAAA-MM-DD') ?>
            <?= $f->field($form, 'fechaHasta')->textInput()->hint('Use el formato AAAA-MM-DD') ?>
            <?= $f->field($form, 'r_persona')->textInput(['maxlength' => true])?>
        </div>
        <div class="col-md-6">
            <?= $f->field($form, 'pm_texto_original')->textInput(['maxlength' => true])?>
            <?= $f->field($form, 'pm_id_persona')->checkboxList($ids_personas_data) ?>
        </div>
    </div>
    </div>
    <?= Html::submitButton("Filtrar", ["class" => "btn btn-primary"]) ?>
    <?php $f->end() ?>


    <h2>Respuesta</h2>
    
    <?php
    //print_r($response->getBody()->getContents());
    //print_r($response);
    
    //print_r($resultado);
    echo VarDumper::dumpAsString($resultado);
    
    //print_r($pedido);
    //print_r(json_decode($resultado));

    ?>
        

</div>

