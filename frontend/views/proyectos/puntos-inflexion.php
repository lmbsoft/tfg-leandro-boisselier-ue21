<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
use yii\grid\GridView;

use yii\data\ActiveDataProvider;
use frontend\models\ProyectosArchivos;
use frontend\models\ProyectosMensajes;
use frontend\models\ProyectosPersonas;
use frontend\models\EstadosProyectos;
use yii\helpers\VarDumper;

$show_this_nav = PermisosHelpers::requerirMinimoRol('Usuario');
/* @var $this yii\web\View */
/* @var $model frontend\models\Proyectos */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proyectos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$estadoIniciado = $model->estado_proyecto == EstadosProyectos::ESTADO_INICIADO;

?>
<div class="proyectos-view">
    <?= Html::a('Volver', ['view', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    <h2 style="text-align:center" class="alert alert-info">Proyecto: <?= Html::encode($model->numero_proyecto) ?></h2>
    
    <h2>Resultado: <?= $puntos_encontrados?> puntos encontrados</h2>
    <?php foreach ($model->proyectosMensajes as $m):?>
        <?php if($m->es_punto_inflexion): ?>
            <p> 
                <?php 
                    echo $m->clasificacion. "|";
                    echo $m->es_punto_inflexion. "|";
                    echo $m->persona0->persona. "|";
                    echo $m->texto_original. "|";
                ?> 
            
            </p>
        <?php endif;?>  
    <?php endforeach;?>

</div>

