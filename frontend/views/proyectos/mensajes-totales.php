<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use common\models\PermisosHelpers;

use kartik\typeahead\TypeaheadBasic;

use yii\web\View;


$esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
$userId = \Yii::$app->user->identity->id;        

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\EstablecimientosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mensajes');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1>Totales por Clave - Sistema Mensajería</h1>

<?php $f = ActiveForm::begin([
    "method" => "get",
    //"action" => Url::toRoute("informes/mensajeria-totales"),
    //"enableClientValidation" => true,
    ]);
?>
<h2><span class="label label-primary" id="boton-filtro">Filtrar</span>
<?= Html::a('Volver', ['view', 'id' => $form->p_id], ['class' => 'btn btn-primary']) ?>
</h2>
<div class="row">
<div class="form-group">
    <div class="col-md-6">
        <?= $f->field($form, 'p_id')->hiddenInput()->label(false) ?>
        <?= $f->field($form, 'fechaDesde')->textInput()->hint('Use el formato AAAA-MM-DD') ?>
        <?= $f->field($form, 'fechaHasta')->textInput()->hint('Use el formato AAAA-MM-DD') ?>
        <?= $f->field($form, 'r_persona')->textInput(['maxlength' => true])
        ?>
    </div>
    <div class="col-md-6">
        <?= $f->field($form, 'pm_texto_original')->textInput(['maxlength' => true])?>
        <?= $f->field($form, 'pm_id_persona')->checkboxList($ids_personas_data) ?>
    </div>
</div>
</div>
<?= Html::submitButton("Filtrar", ["class" => "btn btn-primary"]) ?>
<?php $f->end() ?>

<h2>Totales por Clave</h2>
<?php 
$gridColumns = [
        'clave',
        'etiqueta',
        'cantidad',
];
// Renders a export dropdown menu
echo "<h2><span class='label label-success'>Exportar Totales por Clave</span></h2>";
echo ExportMenu::widget([
    'dataProvider' => $provider,
    'columns' => $gridColumns
]);

?>
<h3>Totales por clave</h3>
<table class="table table-bordered">
    <tr>
        <th>Clave</th>
        <th>Etiqueta</th>
        <th>Cantidad</th>
    </tr>
    
    <?php foreach($resultado as $r): ?>
    <tr>
        <td><?= $r['clave'] ?></td>
        <td><?= $r['etiqueta'] ?></td>
        <td><?= $r['cantidad'] ?></td>
    </tr>
    <?php endforeach ?>
</table>

<?php
$this->registerJs(<<< EOF_JS
   var formw0 = $('#w0');
   formw0.on('submit', function(e) {
      return formw0.yiiActiveForm('submitForm');
   }); 
   $('#boton-filtro').on('click', function() { 
      formw0.submit();
   });
EOF_JS
,View::POS_READY);
?>
