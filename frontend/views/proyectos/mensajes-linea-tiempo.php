<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use common\models\PermisosHelpers;
use common\models\MensajesHelpers;

use kartik\typeahead\TypeaheadBasic;

use yii\web\View;

$esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
$userId = \Yii::$app->user->identity->id;        

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\EstablecimientosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Línea de tiempo');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1>Línea de tiempo de Mensajería (Cada <?= $form->intervalo_minutos ?> minutos)</h1>

<?php $f = ActiveForm::begin([
    "method" => "get",
    //"action" => Url::toRoute("informes/mensajeria-linea-tiempo"),
    //"enableClientValidation" => true,
    ]);
?>
<h2><span class="label label-primary" id="boton-filtro">Filtrar</span>
<?= Html::a('Volver', ['view', 'id' => $form->p_id], ['class' => 'btn btn-primary']) ?>
</h2>
<div class="row">
<div class="form-group">
    <div class="col-md-6">
        <?= $f->field($form, 'p_id')->hiddenInput()->label(false) ?>
        <?= $f->field($form, 'fechaDesde')->textInput()->hint('Use el formato AAAA-MM-DD') ?>
        <?= $f->field($form, 'fechaHasta')->textInput()->hint('Use el formato AAAA-MM-DD') ?>
        <?= $f->field($form, 'r_persona')->textInput(['maxlength' => true])
        ?>
    </div>
    <div class="col-md-6">
        <?= $f->field($form, 'intervalo_minutos')->textInput(['maxlength' => true])?>
        <?= $f->field($form, 'pm_texto_original')->textInput(['maxlength' => true])?>
        <?= $f->field($form, 'pm_id_persona')->checkboxList($ids_personas_data) ?>
    </div>
</div>
</div>
<?= Html::submitButton("Filtrar", ["id"=>"boton-filtro","class" => "btn btn-primary"]) ?>
<?php $f->end() ?>

<h3>Línea de Tiempo</h3>
<table class="table table-bordered" id="dataTable">
    <tr>
        <th>Fecha y Hora</th>
        <?php
        foreach($remitentes as $r=>$ind){
            echo "<th>".$r."</th>";
        }
        ?>
    </tr>
    <?php
    $cantRemitentes = count($remitentes);
    $hora_vacia = 0;

    $fila_tiene_datos = false; //ver si estoy en una fila vacia

    for($i=0;$i<count($lineaTiempo);$i++){

        for ($contVacios=1; $contVacios<=$cantRemitentes; $contVacios++){
            if($lineaTiempo[$i][$contVacios]!="&nbsp;"){
                $fila_tiene_datos=true;
                break;
            }else{
                if ($hora_vacia == 0){ //
                    $hora_vacia = $lineaTiempo[$i][0];
                }
            }
        }
        
        if($fila_tiene_datos){
            if($hora_vacia !=0){//si he pasado por una hora vacía antes imprimo una fila con la diferencia
                $diferencia = $lineaTiempo[$i][0] - $hora_vacia; //- $hora_vacia
                if($diferencia>0){
                    $diferenciaStr = MensajesHelpers::dateToMinutes($diferencia);
                    $padding = intval(5 + ($diferencia/17280)); 
                    $color = $diferencia>86400? "yellow":"lightyellow";
                    echo "<tr><td style='background-color:$color;padding:".$padding."px;font-size:1.5em;text-align:center;' colspan=".($cantRemitentes+1).">". ($diferenciaStr)."</td></tr>";
                }
            }
            
            $hora_vacia = 0; //restablezco el contador de horas vacias
            $fila_tiene_datos=false;
            
            //luego de resolver la fila vacía imprimo la fila con datos
            echo "<tr>";
            //echo "<td>".date('Y/m/d H:i:s D',$lineaTiempo[$i][0])."</td>";
            echo "<td>".date('Y/m/d H:i D',$lineaTiempo[$i][0])."</td>";
            foreach($remitentes as $r=>$ind){
                echo "<td>".$lineaTiempo[$i][$ind+1]."</td>";
            }
            echo "</tr>";
        }else{
            
        }
    }
    ?>
</table>

<?php
$this->registerJs(<<< EOF_JS
   var formw0 = $('#w0');
   formw0.on('submit', function(e) {
      return formw0.yiiActiveForm('submitForm');
   }); 
   $('#boton-filtro').on('click', function() { 
      formw0.submit();
   });
EOF_JS
,View::POS_READY);


$this->registerCss(<<< EOF_CSS
table#dataTable {
        width: 100%;
        table-layout: fixed;
}
        
table#dataTable td {
        background-color: rgb(250,250,250);
        padding: 1px;
        border: 1px solid gray;
        font-size: .9em;
        text-align: left;
        height: 25px;
}
      
  
EOF_CSS
);

?>
