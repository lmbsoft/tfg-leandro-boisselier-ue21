<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
use yii\grid\GridView;

use yii\data\ActiveDataProvider;
use frontend\models\ProyectosArchivos;
use frontend\models\ProyectosMensajes;
use frontend\models\ProyectosPersonas;
use frontend\models\EstadosProyectos;

$show_this_nav = PermisosHelpers::requerirMinimoRol('Usuario');
/* @var $this yii\web\View */
/* @var $model frontend\models\Proyectos */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proyectos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$estadoIniciado = $model->estado_proyecto == EstadosProyectos::ESTADO_INICIADO;

?>
<div class="proyectos-view">
    <?= Html::a('Volver', ['view', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    <h2 style="text-align:center" class="alert alert-info">Proyecto: <?= Html::encode($model->numero_proyecto) ?></h2>
    
        <?php /*echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'id',
                'user.username',
                'numero_proyecto',
                'nombre',
                //'fechahora_inicio:datetime',
                'xfecha_inicio',
                //'estado_proyecto',
                'estadoProyecto.estado_proyecto',
                //'fechahora:datetime',
                'xfecha',
                'observaciones:ntext',
                'stopwords0.nombre',
                'stopwords_lista:ntext',
                'stemmer0.nombre',
                'stemmer_lista:ntext',
                'lat',
                'lng',

            ],
        ])
         
         */ 
         ?>
      
    
    <h2 class="alert alert-success">mensajes procesados: <?= count($listafrm)?></h2>
    <h2 class="alert alert-success">personas procesadas: <?= count($listapersonas)?></h2>
    
    <h2 class="alert alert-info">Texto</h2>
        <?= nl2br($texto) ?>
    <pre>
    <?php
//       print_r(DateTime::createFromFormat("!d/m/y H:i", "10/05/18 10:15"));
//       print_r(DateTime::createFromFormat("!Y-m-d H:i:s", "2018-05-10 10:15:10"));
//       
//       $today = new DateTime();
//       $today= $today->createFromFormat('!Y-m-d H:i', '2018-05-10 10:05');
//       print_r($today);
    ?>
    </pre>
    <?php foreach($mensajes as $m): ?>
    <p>
    <?php
//        $fecha_mensaje = mb_split("-",$m,2);
//        
//        echo var_dump($fecha_mensaje)."<br/>";
//        $remitente_mensaje = mb_split(":",$fecha_mensaje[1],2);
//        echo var_dump($remitente_mensaje)."<br/>";
//
//        $fechastr = trim($fecha_mensaje[0]);
//        $fecha=DateTime::createFromFormat("!j/n/y H:i", $fechastr);
//        
//        echo var_dump($fecha)."<br/>";
//        
//        $remitente = trim($remitente_mensaje[0]);
//        $mensaje = trim($remitente_mensaje[1]);
        
           // echo var_dump($m)."<br/>";
        
                
    ?>
    </p>
    <?php endforeach; ?>
    <pre>
        <?php //print_r($listapersonas);?>
        <?php //print_r($listafrm);?>
    </pre>
    

</div>

