<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;

use kartik\export\ExportMenu;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model frontend\models\Textos */

$this->title = "Contador:".$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Textos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="textos-view">

    <h1><?= Html::encode($this->title) ?></h1>

<?php
$gridColumns = [
    'palabra',
    'cantidad',
];
echo "<h2><span class='label label-success'>Exportar Palabras</span></h2>";
echo Exportmenu::widget([
    'dataProvider'=>$provider,
    'columns'=>$gridColumns,
]);
?>
    
    <h3>Texto Original</h3>
    <?=$cadena ?>
    <h3>Texto Procesado</h3>
    <?= $cadenaReemplazada?>
    <h3>Resultados encontrados</h3>
    
    <?php //echo "<pre>"; print_r($ocurrencias); echo "</pre>"?>
    <table class ="table table-bordered">
        <tr>
            <th>Palabra</th>
            <td>Cantidad</th>
        </tr>
        <?php foreach ($ocurrencias as $palabra=>$cantidad):?>
            <tr>
                <td><?=$palabra?></td>
                <td><?=$cantidad?></td>
            </tr>
        <?php endforeach;?>
    </table>
    
    

</div>
