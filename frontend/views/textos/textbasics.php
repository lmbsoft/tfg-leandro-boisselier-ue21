<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;
use yii\web\View;
use yii\helpers\VarDumper;

use okeanos\chartist\Chartist;
use yii\helpers\Json;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model frontend\models\Textos */

$this->title = "Estadísticas:".$model->id . " - ".$model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Textos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="textos-view">

    <h1 class="alert alert-success"><?= Html::encode($this->title) ?> - idioma: <?= $lang ?></h1>

    <h1 class="alert alert-success">Estadísticas básicas</h1>
    <?php if(count($textbasics_textos)>0): ?>
        <?php 
            $parte = $textbasics_textos[0];
            if (count($parte)==4):?>
            <table class="table table-bordered">
                <tr>
                    <td class="alert alert-success">Text <?= $model->nombre ?></td>
                    <td class="alert alert-success">Polarity</td>
                    <td class="alert alert-success">Subjective</td>
                </tr>
                <?php 
                    $filas = count($parte[0]['textos']);
                    for($cont = 0 ; $cont<$filas ; $cont++){
                ?>
                    <tr>
                        <td><?= $parte[0]['textos'][$cont]?></td>
                        <td><?= $parte[0]['polarity'][$cont]?></td>
                        <td><?= $parte[0]['subjective'][$cont]?></td>
                    </tr>
                <?php }?>
            </table>
            <?php //print_r($parte[0]); ?>

            <table class="table table-bordered">
                <tr>
                    <td class="alert alert-success">#</td>
                    <td class="alert alert-success">Frecuency</td>
                    <td class="alert alert-success">Bigram/Trigram <?= $model->nombre ?></td>
                    <td class="alert alert-success">Polarity</td>
                    <td class="alert alert-success">Subjective</td>
                </tr>
                <?php 
                    $filas = count($parte[1]['frequency']);
                    $filas = $filas>$topten?$topten:$filas; //limito al topten
                    for($cont = 0 ; $cont<$filas ; $cont++){
                ?>
                    <tr>
                        <td><?= $cont+1?></td>
                        <td><?= $parte[1]['frequency'][$cont]?></td>
                        <td><?= $parte[1]['bigram/trigram'][$cont]?></td>
                        <td><?= $parte[1]['polarity'][$cont]?></td>
                        <td><?= $parte[1]['subjective'][$cont]?></td>
                    </tr>
                <?php }?>
            </table>
            <?php //print_r($parte[1]); ?>

            <table class="table table-bordered">
                <tr>
                    <td class="alert alert-success">Tópicos NFM <?= $model->nombre ?></td>
                </tr>
                <?php 
                    $filas = count($parte[2]);
                    for($cont = 0 ; $cont<$filas ; $cont++){
                ?>
                    <tr>
                        <td><?= $parte[2][$cont] ?></td>
                    </tr>
                <?php }?>
            </table>
            <?php //print_r($parte[2]); ?>

            <table class="table table-bordered">
                <tr>
                    <td class="alert alert-success">Tópicos LDA <?= $model->nombre ?></td>
                </tr>
                <?php 
                    $filas = count($parte[3]);
                    for($cont = 0 ; $cont<$filas ; $cont++){
                ?>
                    <tr>
                        <td><?= $parte[3][$cont] ?></td>
                    </tr>
                <?php }?>
            </table>
            <?php //print_r($parte[3]); ?>

        <?php endif; ?>

    <?php endif; ?>

    
    <h1 class="alert alert-success">Texto Procesado</h1>
    <?php foreach($parrafos_textos as $parrdoc): ?>
        <p>
            <?php echo nl2br($parrdoc);?>
        </p>
    <?php endforeach; ?>
                
</div>
