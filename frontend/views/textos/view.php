<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
/* @var $this yii\web\View */
/* @var $model frontend\models\Textos */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Textos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="textos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user.username',
            'nombre',
            'categoria',
            'clasificacion',
            'descripcion:ntext',
            'autor',
            'texto:ntext',
            'stopwords',
            'stopwords_lista:ntext',
            'stemmer',
            'stemmer_lista:ntext',
            'observaciones:ntext',
            'lugares:ntext',
            'publico',
            'cantidad_topten',
            'created_at:datetime',
            [
                'label'=>'Creado Por',
                'value'=>RegistrosHelpers::getUserName($model->created_by)
            ],
            'updated_at:datetime',
            //'created_by',
            //'updated_by',
            [
                'label'=>'Actualizado Por',
                'value'=>RegistrosHelpers::getUserName($model->updated_by)
            ],
        ],
    ]) ?>

</div>
