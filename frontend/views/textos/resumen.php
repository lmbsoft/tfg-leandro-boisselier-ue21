<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;
use yii\web\View;
use yii\helpers\VarDumper;
/* @var $this yii\web\View */
/* @var $model frontend\models\Textos */

$this->title = "Sumarios para:".$model->id . " - ".$model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Textos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="textos-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <h2>Resultados obtenidos procesando con idioma: <?= $lang ?></h2>
<?php
$gridColumns = [
    'doc',
    'parte',
    'parrafo',
    'sumario:raw',
    //'original:raw',
];
echo "<h2><span class='label label-success'>Exportar Palabras</span></h2>";

echo Exportmenu::widget([
    'dataProvider'=>$provider,
    'columns'=>$gridColumns,
]);
?>
    
    <h3>Resultados</h3>
    <!--
    <pre>
    <?php
        //print_r($arrayLineas); //$cabeceras = ['lemma','text','upos'];
        //echo VarDumper::dumpAsString("Error:".$error); //$cabeceras = ['lemma','text','upos'];
    ?>
    </pre>
    -->
    <table class ="table table-bordered">
        <tr>
                <td>doc</td>
                <td>parte</td>
                <td>párrafo</td>
                <td>sumario</td>
                <!--<td>original</td>-->
        </tr>
        <?php foreach( $tableArray as $r): ?>
            <tr>
                <td><?= $r['doc'] ?></td>
                <td><?= $r['parte'] ?></td>
                <td><?= $r['parrafo'] ?></td>
                <td><?= $r['sumario'] ?></td>
                <!--<td><?php //echo $r['original'] ?></td>-->
            </tr>            
        <?php endforeach;?>
    </table>
    
</div>
