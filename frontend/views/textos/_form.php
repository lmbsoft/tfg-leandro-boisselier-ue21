<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\typeahead\TypeaheadBasic;
use yii\web\View;
use yii\helpers\Url;
use frontend\models\Textos;

/* @var $this yii\web\View */
/* @var $model frontend\models\Textos */
/* @var $form yii\widgets\ActiveForm */
$categoriasData = Textos::listaCategorias();
$clasificacionesData = Textos::listaClasificaciones();
$autoresData = Textos::listaAutores();

?>

<div class="textos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?php if(count($categoriasData)>0): ?>
    <?= $form->field($model, 'categoria')->textInput(['maxlength' => 255])
            ->widget(TypeaheadBasic::classname(), ['data'=>$categoriasData])
    ?>
    <?php else: ?>
        <?= $form->field($model, 'categoria')->textInput(['maxlength' => 255])?>
    <?php endif; ?>
 
    <?php if(count($clasificacionesData)>0): ?>
    <?= $form->field($model, 'clasificacion')->textInput(['maxlength' => 255])
            ->widget(TypeaheadBasic::classname(), ['data'=>$clasificacionesData])
    ?>
    <?php else: ?>
        <?= $form->field($model, 'clasificacion')->textInput(['maxlength' => 255])?>
    <?php endif; ?>
 

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>
    
    <?php if(count($autoresData)>0): ?>
    <?= $form->field($model, 'autor')->textInput(['maxlength' => 255])
            ->widget(TypeaheadBasic::classname(), ['data'=>$autoresData])
    ?>
    <?php else: ?>
        <?= $form->field($model, 'autor')->textInput(['maxlength' => 255])?>
    <?php endif; ?>

    <?= $form->field($model, 'texto')->textarea(['rows' => 50]) ?>

    <?php //echo $form->field($model, 'stopwords')->textInput() ?>
    <?= $form->field($model, 'stopwords')->dropDownList(
            ArrayHelper::map( \frontend\models\Stopwords::find()->All(), 'id','nombre'), [ 'prompt' => 'Seleccione Stopword' ]
    ) ?>    

    <?= $form->field($model, 'stopwords_lista')->textarea(['rows' => 6])->hint("Una palabra por línea") ?>

    <?php // echo $form->field($model, 'stemmer')->textInput() ?>
    <?= $form->field($model, 'stemmer')->dropDownList(
            ArrayHelper::map( \frontend\models\Stemmers::find()->All(), 'id','nombre'), [ 'prompt' => 'Seleccione Stemmer' ]
    ) ?>    

    <?= $form->field($model, 'stemmer_lista')->textarea(['rows' => 6])->hint("Una por línea, formato: valor=reemplazo") ?>

    <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lugares')->textarea(['rows' => 6])->hint("Un lugar por línea, formato: nombrelugarlugar=lat=lng=alt") ?>

    <?= $form->field($model, 'publico')->checkbox() ?>

    <?= $form->field($model, 'cantidad_topten')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
