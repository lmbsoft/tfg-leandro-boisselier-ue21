<?php
use yii\helpers\Html;

use yii\widgets\ActiveForm;

//$diff = Diff::compare("line1\nline2", "lineA\nlineB");

// compare two strings character by character
//$diff = Diff::compare('abcmnz', 'amnxyz', true);

?>
<?= Html::style("
    
table{
  margin          : 0 auto 1.5em;
  border-collapse : collapse;
  border-spacing  : 0;
}

td,
th{
  font-weight    : normal;
  vertical-align : top;
  padding-left   : 0.375em;
  padding-right  : 0.375em;
}

.diff td{
        padding:0 0.667em;
        vertical-align:top;
        white-space:pre;
        white-space:pre-wrap;
        font-family:Consolas,'Courier New',Courier,monospace;
        font-size:0.75em;
        line-height:1.333;
      }

      .diff span{
        display:block;
        min-height:1.333em;
        margin-top:-1px;
        padding:0 3px;
      }

      * html .diff span{
        height:1.333em;
      }

      .diff span:first-child{
        margin-top:0;
      }

      .diffDeleted span{
        border:1px solid rgb(255,192,192);
        background:rgb(255,224,224);
      }

      .diffInserted span{
        border:1px solid rgb(192,255,192);
        background:rgb(224,255,224);
      }

      #toStringOutput{
        margin:0 2em 2em;
      }
") ?>
<h1>Comparador de textos</h1>
<p>Inserte textos en los cuadros y presione el botón Comparar para obtener resultados</p>
<p>Las diferencias se calculan línea a línea</p>
<div class="diferencias-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'texto1')->textArea(['rows'=>10, 'cols'=>50]) ?>
    <?= $form->field($model, 'texto2')->textArea(['rows'=>10, 'cols'=>50]) ?>

    <div class="form-group">
        <?= Html::submitButton('Comparar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<h2>Resultados</h2>
<!--
<p>Texto1:</p>
<?php //echo nl2br($texto1) ?>
<p></p>
<p>Texto2:</p>
<?php //echo nl2br($texto2) ?>
-->

<h2>Diferencias</h2>

<h3>Tabla de diferencias</h3>
<?php echo $textoDiffTable ?>
<!--
<h3>En formato de Texto</h3>
<?php //echo nl2br($textoDiffTxt) ?>
<h3>En formato HTML</h3>
<?php //echo $textoDiffHtml ?>
-->

<!--<h2>Diferencias Caracter a caracter</h2>

<h3>Tabla de diferencias</h3>-->
<?php //echo  $textoDiffTable2 ?>

<!--<h3>En formato de Texto</h3>-->
<?php //echo nl2br($textoDiffTxt2) ?>
<!--<h3>En formato HTML</h3>-->
<?php //echo $textoDiffHtml2 ?>

