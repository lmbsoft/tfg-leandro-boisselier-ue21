<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\TextosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="textos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'categoria') ?>

    <?= $form->field($model, 'clasificacion') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'autor') ?>

    <?php // echo $form->field($model, 'texto') ?>

    <?php // echo $form->field($model, 'stopwords') ?>

    <?php // echo $form->field($model, 'stopwords_lista') ?>

    <?php // echo $form->field($model, 'stemmer') ?>

    <?php // echo $form->field($model, 'stemmer_lista') ?>

    <?php // echo $form->field($model, 'observaciones') ?>

    <?php // echo $form->field($model, 'lugares') ?>

    <?php // echo $form->field($model, 'publico') ?>

    <?php // echo $form->field($model, 'cantidad_topten') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
