<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\TextosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Textos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="textos-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Agregar Textos'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user.username',
            'nombre',
            'categoria',
            'clasificacion',
            'user.username',
            [
              'header' => 'Contador',
              'content' => function ($model, $key, $index, $column) {
                  return Html::a('Contador',
                    ['contador', 'id'
                      => $model->id]);
                    }
            ],             
            [
              'header' => 'Stanza',
              'content' => function ($model, $key, $index, $column) {
                  return Html::a('Stanza',
                    ['stanza', 'id'
                      => $model->id]);
                    }
            ],             
            [
              'header' => 'Text Basics',
              'content' => function ($model, $key, $index, $column) {
                  return Html::a('textbasics',
                    ['textbasics', 'id'
                      => $model->id]);
                    }
            ],
                                [
              'header' => 'Summ',
              'content' => function ($model, $key, $index, $column) {
                  return Html::a('Summary',
                    ['sumario', 'id'
                      => $model->id]);
                    }
            ], 

            //'descripcion:ntext',
            // 'autor',
            // 'texto:ntext',
            // 'stopwords',
            // 'stopwords_lista:ntext',
            // 'stemmer',
            // 'stemmer_lista:ntext',
            // 'observaciones:ntext',
            // 'lugares:ntext',
            // 'publico',
            // 'cantidad_topten',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
