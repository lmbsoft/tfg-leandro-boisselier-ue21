<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model frontend\models\Textos */

$this->title = "Stanza:".$model->id . " - ".$model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Textos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="textos-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <h2>Resultados obtenidos procesando con idioma: <?= $lang ?></h2>
<?php
$gridColumns = [
    'lemma:raw',
    'text:raw',
    'upos',
];

echo "<h2><span class='label label-success'>Exportar Palabras</span></h2>";

echo Exportmenu::widget([
    'dataProvider'=>$provider,
    'columns'=>$gridColumns,
]);

?>
    
    <h3>Resultados</h3>
    <?php //echo $cadena ?>
    <?php
    $cabeceras = [];
    foreach($resultado as $x){
        foreach ($x as $r){
            foreach ($r as $k=>$v){
                $cabeceras[$k]=$v;
            }
        }
    }
    $cabeceras=array_keys($cabeceras);
    $cabeceras = ['lemma','text','upos'];
    ?>
    <table class ="table table-bordered">
        <tr>
            <th><?php echo implode("</th><th>",$cabeceras) ?></th>
        </tr>
        <?php foreach( $resultado as $x): ?>
            <?php foreach( $x as $r): ?>
            <tr>
                <?php foreach($cabeceras as $c):?>
                    <td><?= $r->$c ?></td>
                <?php endforeach;?>
            </tr>            
            <?php endforeach;?>
        <?php endforeach;?>
    </table>
    
    

</div>
