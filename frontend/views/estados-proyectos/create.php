<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\EstadosProyectos */

$this->title = Yii::t('app', 'Agregar Estados Proyectos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estados Proyectos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estados-proyectos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
