<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\RegistrosHelpers;
use common\models\PermisosHelpers;
/* @var $this yii\web\View */
/* @var $model frontend\models\ProyectosMensajes */

$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proyectos Mensajes'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'] = [$this->title];

?>
<div class="proyectos-mensajes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Volver'), ['proyectos/view', 'id' => $model->proyecto], ['class' => 'btn btn-success']) ?>
        
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'proyecto',
            'fecha:datetime',
            //'xfecha',
            'persona0.persona',
            'texto_original:ntext',
            'texto:ntext',
            'emojis:ntext',
            'adjuntos:ntext',
            'clasificacion',
            'es_punto_inflexion',
            'lat',
            'lng',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'label' => 'Creado Por',
                'value' => RegistrosHelpers::getUserName($model->created_by)
            ],
            [
                'label' => 'Actualizado Por',
                'value' => RegistrosHelpers::getUserName($model->updated_by)
            ],
        ],
    ]) ?>

</div>
