<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProyectosMensajes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proyectos-mensajes-form">
    <h3>Número de Proyecto: <?= $model->proyecto0->numero_proyecto ?></h3>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'proyecto')->hiddenInput()->label(false) ?>

    <?php //echo  $form->field($model, 'fecha')->textInput(); ?>
    <?php echo  $form->field($model, 'xfecha')->textInput(); ?>
    
    <?= $form->field($model, 'persona')->dropDownList(
            //ArrayHelper::map( EstadosOrdenes::find()->orderBy('estado_valor')->All(), 'id','estado_orden'), [ 'prompt' => 'Seleccione Estado de la Orden' ]
            $model->personasPosibles()
            , [ 'prompt' => 'Seleccione una persona' ]
    ) ?>

    <?= $form->field($model, 'texto_original')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'texto')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'emojis')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'adjuntos')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'clasificacion')->textInput() ?>

    <?= $form->field($model, 'es_punto_inflexion')->checkbox() ?>

    <?= $form->field($model, 'lat')->textInput() ?>

    <?= $form->field($model, 'lng')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
