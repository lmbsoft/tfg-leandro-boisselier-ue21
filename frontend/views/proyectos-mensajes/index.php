<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ProyectosMensajesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Proyectos Mensajes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proyectos-mensajes-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Volver'), ['proyectos/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Agregar Mensaje'), ['create', 'proyecto'=>$idProyecto], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{pager}\n{summary}\n{items}\n{pager}",        
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'proyecto0.nombre',
            //'fecha',
            'xfecha',
            'persona0.persona',
            'texto_original:ntext',
            'texto:ntext',
            'emojis:ntext',
            'adjuntos:ntext',
            'clasificacion',
            // 'es_punto_inflexion',
            // 'lat',
            // 'lng',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'pager' => [
            'options'=>['class'=>'pagination'],   // set clas name used in ui list of pagination
            'prevPageLabel' => '<',   // Set the label for the "previous" page button
            'nextPageLabel' => '>',   // Set the label for the "next" page button
            'firstPageLabel'=>'<<',   // Set the label for the "first" page button
            'lastPageLabel'=>'>>',    // Set the label for the "last" page button
            'nextPageCssClass'=>'next',    // Set CSS class for the "next" page button
            'prevPageCssClass'=>'prev',    // Set CSS class for the "previous" page button
            'firstPageCssClass'=>'first',    // Set CSS class for the "first" page button
            'lastPageCssClass'=>'last',    // Set CSS class for the "last" page button
            'maxButtonCount'=>10,    // Set maximum number of page buttons that can be displayed
        ],
    ]); ?>

</div>
