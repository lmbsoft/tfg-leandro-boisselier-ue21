<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Stemmers */

$this->title = Yii::t('app', 'Agregar Stemmers');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Stemmers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stemmers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
