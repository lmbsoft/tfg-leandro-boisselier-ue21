<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\PermisosHelpers;

$show_this_nav = PermisosHelpers::requerirMinimoRol('Admin');

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\StemmersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Stemmers');
$this->params['breadcrumbs'][] = $this->title;
$es_admin = PermisosHelpers::requerirMinimoRol('Admin');
$template = '{view}{update}{delete}';
if (!$es_admin){
    $template = '{view}';
}

?>
<div class="stemmers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if($show_this_nav):?>
        <p>
            <?= Html::a(Yii::t('app', 'Agregar Stemmer'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif;?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout'=>"{pager}\n{summary}\n{items}\n{pager}",        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            //'palabras:ntext',

            [   
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => $template,
            ],

        ],
        'pager' => [
            'options'=>['class'=>'pagination'],   // set clas name used in ui list of pagination
            'prevPageLabel' => '<',   // Set the label for the "previous" page button
            'nextPageLabel' => '>',   // Set the label for the "next" page button
            'firstPageLabel'=>'<<',   // Set the label for the "first" page button
            'lastPageLabel'=>'>>',    // Set the label for the "last" page button
            'nextPageCssClass'=>'next',    // Set CSS class for the "next" page button
            'prevPageCssClass'=>'prev',    // Set CSS class for the "previous" page button
            'firstPageCssClass'=>'first',    // Set CSS class for the "first" page button
            'lastPageCssClass'=>'last',    // Set CSS class for the "last" page button
            'maxButtonCount'=>10,    // Set maximum number of page buttons that can be displayed
        ],
    ]); ?>

</div>
