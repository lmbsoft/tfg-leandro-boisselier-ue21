<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\PermisosHelpers;

$show_this_nav = PermisosHelpers::requerirMinimoRol('Admin');

/* @var $this yii\web\View */
/* @var $model frontend\models\Stemmers */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Stemmers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stemmers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if($show_this_nav):?>
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif;?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'palabras:ntext',
        ],
    ]) ?>

</div>
