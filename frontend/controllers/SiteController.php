<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use frontend\models\PasswordForm;
use common\models\User;

use common\models\PermisosHelpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use yii\helpers\Json;

use yii\helpers\VarDumper;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'change-password'],
                'rules' => [
                    [
                        'actions' => ['signup','change-password'],
                        'allow' => true,
                        'roles' => ['@'], //solo pueden crear los ya logueados
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRegexp()
    {
        return $this->render('regexp');
    }

    public function actionWebnlp()
    {
        $client = new \GuzzleHttp\Client();
        //$response = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');
        $pedido=[
            'text'=>'esto no me gusta',
            'lang'=>'es',
        ];
        $response=null;
        $resultado=null;
        
        try {
            $response = $client->request('POST', 'http://web64nlpserver:6400/polyglot/sentiment', [
                'allow_redirects' => true,
                'timeout'=> 10,
                'form_params'=>$pedido,
            ]);

            $resultado = $response->getBody()->getContents();

        }catch(RequestException $e){
            /**
             * Here we actually catch the instance of GuzzleHttp\Psr7\Response
             * (find it in ./vendor/guzzlehttp/psr7/src/Response.php) with all
             * its own and its 'Message' trait's methods. See more explanations below.
             *
             * So you can have: HTTP status code, message, headers and body.
             * Just check the exception object has the response before.
             */
            echo var_dump(($e->getRequest()));

            if ($e->hasResponse()) {
                $response = $e->getResponse();
                var_dump($response->getStatusCode()); // HTTP status code;
                var_dump($response->getReasonPhrase()); // Response message;
                var_dump((string) $response->getBody()); // Body, normally it is JSON;
                var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
                var_dump($response->getHeaders()); // Headers array;
                var_dump($response->hasHeader('Content-Type')); // Is the header presented?
                var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
            }
        }
        return $this->render('webnlp',[
            'resultado'=>$resultado,
            'response'=>$response,
            'pedido'=>$pedido,
        ]);        
    }
    
    public function actionWebnlpcomponent()
    {
        $mensaje = "Esto es bueno";
        $resultado = \Yii::$app->nlp->valorarSentimientos($mensaje);
        return $this->render('webnlp',[
            'resultado'=>$resultado,
        ]);        
    }
    
    public function actionFlaskcomponent()
    {
        $mensaje = "Esto es bueno";
        $resultado = \Yii::$app->flask->imagen($mensaje);
        //\Yii::error(VarDumper::dumpAsString("Resultado2!:".$resultado));
        return $this->render('flask',[
            'resultado'=>$resultado,
        ]);        
    }
    
    public function actionGuzzle()
    {
        $client = new \GuzzleHttp\Client();
        //$response = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');
        $response=null;
        $resultado=null;
        
        try {
            $response = $client->request('GET', 'https://httpbin.org/uuid', [
                'allow_redirects' => true,
                'timeout'=> 10,
            ]);

            $resultado = $response->getBody()->getContents();

        }catch(RequestException $e){
            echo var_dump(($e->getRequest()));
            if ($e->hasResponse()) {
                echo var_dump(($e->getResponse()));
            }
        }
        
        //echo $response->getStatusCode(); # 200
        //echo $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'
        //if($resultado){
        //    $resultado = $response->getBody(); # '{"id": 1420053, "name": "guzzle", ...}'
        // }
        # Send an asynchronous request.
       // $request = new \GuzzleHttp\Psr7\Request('GET', 'http://httpbin.org/uuid');
       // $resultado = null;
       // $promise = $client->sendAsync($request)->then(function ($response) {
       //     $resultado = $response->getBody();
       //     //echo 'I completed! ' . $response->getBody();
       // });
        
        //$promise->wait();        

        return $this->render('guzzle',[
            'resultado'=>$resultado,
            'response'=>$response,
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            /*
            if(PermisosHelpers::requerirMinimoRol('Admin')){
                return $this->redirect(['/ordenes/dashboard']); //despues de iniciar sesión muestra el Dashboard
            }else{ //si no es admin lo reenvío a ver las tareas asignadas
                return $this->redirect(['/ordenes-asignaciones/tareas-asignadas']);
            }
            */
            
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Gracias por contactarse con nosotros, le responderemos a la brevedad.');
            } else {
                Yii::$app->session->setFlash('error', 'Ocurrió un error enviando el email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Revise su correo para más instrucciones.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Lo lamento, no hemos podido restablecer la contraseña para el email ingresado.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'Nueva contraseña asignada.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionChangePassword(){
        $model=new PasswordForm();
        $modelUser = User::find()->where(['username'=>Yii::$app->user->identity->username])->one();
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                try{
                    $modelUser->password=$_POST['PasswordForm']['newpass'];
                    if ($modelUser->save()){
                        Yii::$app->getSession()->setFlash('success','Contraseña Cambiada');
                        return $this->redirect('index');
                    }else{
                        Yii::$app->getSession()->setFlash('error','No se pudo guardar la contraseña');
                        return $this->redirect('index');
                    }
                } catch (Exception $e){
                    Yii::$app->getSession()->setFlash('error',"{$e->getMessage()}");
                    return $this->render('changepassword',['model'=>$model]);
                }
            }else{
                return $this->render('changepassword',['model'=>$model]);
            }
        }else{
            return $this->render('changepassword',['model'=>$model]);
        }
    }

}
