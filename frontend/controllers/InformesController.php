<?php

namespace frontend\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\PermisosHelpers;

use yii\data\Pagination;
use yii\helpers\Html;

use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

use frontend\models\InformesForm;


use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

use yii\db\Query;
/**
 * EstablecimientosController implements the CRUD actions for Establecimientos model.
 */
class InformesController extends Controller
{
    public function behaviors()
    {
       return [
        'access' => [
               'class' => \yii\filters\AccessControl::className(),
               'only' => ['index', 'ordenes','detalles','informe-ordenes','linea-tiempo'],
               'rules' => [
                    [
                       'actions' => [ 'index','ordenes','detalles','informe-ordenes','linea-tiempo'],
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {
                        return PermisosHelpers::requerirMinimoRol('Usuario')
                        && PermisosHelpers::requerirEstado('Activo');
                       }
                   ],
               ],
           ],

          'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['post'],
                    ],
                ],
            ];
    }


    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionOrdenes(){
        $ordenes = Ordenes::find();
        //$ordenes->orderBy('user_id,fecha_vencimiento'); //->limit(4);

        $dataProvider= new ActiveDataProvider([
            'query'=>$ordenes,
            'pagination'=>false,
            'sort'=>false,
        ]);
        return $this->render('ordenes',['dataProvider'=>$dataProvider]);
    }
    
    
    public function actionDetalles(){
        
        $informesForm=new InformesOrdenesForm();
        
        $informesForm->load(Yii::$app->request->get()); //el get no es lo mejor pero es necesario para el widget de export
        
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        
        $userId = \Yii::$app->user->identity->id;
        
        $ids_usuarios_data= ArrayHelper::map(User::find()->orderBy('username')->All(),'id','username');
        $ids_tipostarea_data= ArrayHelper::map(TiposTareas::find()->orderBy('id')->All(),'id','tipo_tarea');
        $ids_estados_data= ArrayHelper::map(EstadosAsignaciones::find()->orderBy('estado_valor')->All(),'id','estado_asignacion');
        
        $query= (new Query())->select([
                    'o.id as id_orden',
                    'o.numero_orden',
                    'o.descripcion',
                    'o.inmueble',
                    'i.nombre as nombre_inmueble',
                    'o.tipo_usuario',
                    'tu.tipo_usuario_nombre',
                    'o.tipo_tarea as id_tipo_tarea',
                    'tt.tipo_tarea',
                    'o.fechahora_inicio',
                    'o.estado_orden as id_estado_orden',
                    'eo.estado_orden',
                    'o.fechahora_fin',
                    'o.observaciones observaciones_orden',
                    'oa.id as id_orden_asignacion',
                    'oa.user_id as id_responsable',
                    'us.username username_responsable',
                    'p.nombre responsable_nombre',
                    'p.apellido responsable_apellido',
                    'oa.estado_asignacion id_estado_asignacion',
                    'ea.estado_asignacion as estado_asignacion',
                    'oa.fecha_ultimo_estado',
                    'oa.observaciones as observaciones_asignacion',
                ])
                ->from('ordenes o')
                ->join('LEFT JOIN','inmuebles i','o.inmueble=i.id')
                ->join('LEFT JOIN','tipo_usuario tu','o.tipo_usuario=tu.id')
                ->join('LEFT JOIN','tipos_tareas tt','o.tipo_tarea=tt.id')
                ->join('LEFT JOIN','estados_ordenes eo','o.estado_orden=eo.id')
                ->join('LEFT JOIN','ordenes_asignaciones oa','o.id = oa.orden')
                ->join('LEFT JOIN','ejordenes.public.user us','oa.user_id = us.id')
                ->join('LEFT JOIN','perfil p','us.id = p.user_id')
                ->join('LEFT JOIN','estados_asignaciones ea','oa.estado_asignacion=ea.id')
                
                ->orderBy('o.id')
        ;
        
        if ($informesForm->fecha_desde){
                    $informesForm->fecha_desde= Yii::$app->formatter->asDate( date_create_from_format('d/m/Y',
                                $informesForm->fecha_desde), 'php:Y-m-d' );         
        }
        
        if ($informesForm->fecha_hasta){
                    $informesForm->fecha_hasta= Yii::$app->formatter->asDate( date_create_from_format('d/m/Y',
                                $informesForm->fecha_hasta), 'php:Y-m-d' );         
        }
        
        //if($informesForm->validate()){
            $query
            ->andFilterWhere(['in','oa.user_id',$informesForm->responsable])
            ->andFilterWhere(['in','oa.estado_asignacion',$informesForm->estado])
            ->andFilterWhere(['in','o.tipo_tarea',$informesForm->tipo_tarea])
            ->andFilterWhere(['>=','date(o.fechahora_inicio)',$informesForm->fecha_desde])
            ->andFilterWhere(['<=','date(o.fechahora_inicio)',$informesForm->fecha_hasta])
            ;
        //}
        
        $dataProvider= new ActiveDataProvider([
            'query'=>$query,
            'pagination'=>false,
            'sort'=>false,
        ]);
        
//        $dataProvider= new ArrayDataProvider([
//            'allModels'=>$query->all(),
//            'pagination'=>false,
//            'sort'=>false,
//        ]);
        
        
        
        return $this->render('detalles',['dataProvider'=>$dataProvider, 
                    "informesForm" => $informesForm,
                    "ids_usuarios_data" => $ids_usuarios_data,
                    "ids_tipostarea_data" => $ids_tipostarea_data,
                    "ids_estados_data" => $ids_estados_data,
                ]);        
    }
    
    
    public function actionLineaTiempo(){
        
        $informesForm=new InformesOrdenesForm();
        
        $informesForm->load(Yii::$app->request->get()); //el get no es lo mejor pero es necesario para el widget de export
        
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        
        $userId = \Yii::$app->user->identity->id;
        
        $ids_usuarios_data= ArrayHelper::map(User::find()->orderBy('username')->All(),'id','username');
        $ids_tipostarea_data= ArrayHelper::map(TiposTareas::find()->orderBy('id')->All(),'id','tipo_tarea');
        $ids_estados_data= ArrayHelper::map(EstadosAsignaciones::find()->orderBy('estado_valor')->All(),'id','estado_asignacion');
        
        $query= (new Query())->select([
                    'o.id as id_orden',
                    'o.numero_orden',
                    'o.descripcion',
                    'o.inmueble',
                    'i.nombre as nombre_inmueble',
                    'o.tipo_usuario',
                    'tu.tipo_usuario_nombre',
                    'o.tipo_tarea as id_tipo_tarea',
                    'tt.tipo_tarea',
                    'o.fechahora_inicio',
                    'o.estado_orden as id_estado_orden',
                    'eo.estado_orden',
                    'o.fechahora_fin',
                    'o.observaciones observaciones_orden',
                    'oa.id as id_orden_asignacion',
                    'oa.user_id as id_responsable',
                    'us.username username_responsable',
                    'p.nombre responsable_nombre',
                    'p.apellido responsable_apellido',
                    'oa.estado_asignacion id_estado_asignacion',
                    'ea.estado_asignacion as estado_asignacion',
                    'oa.fecha_ultimo_estado',
                    'oa.observaciones as observaciones_asignacion',
                ])
                ->from('ordenes o')
                ->join('LEFT JOIN','inmuebles i','o.inmueble=i.id')
                ->join('LEFT JOIN','tipo_usuario tu','o.tipo_usuario=tu.id')
                ->join('LEFT JOIN','tipos_tareas tt','o.tipo_tarea=tt.id')
                ->join('LEFT JOIN','estados_ordenes eo','o.estado_orden=eo.id')
                ->join('LEFT JOIN','ordenes_asignaciones oa','o.id = oa.orden')
                ->join('LEFT JOIN','ejordenes.public.user us','oa.user_id = us.id')
                ->join('LEFT JOIN','perfil p','us.id = p.user_id')
                ->join('LEFT JOIN','estados_asignaciones ea','oa.estado_asignacion=ea.id')
                
                ->orderBy('oa.fecha_ultimo_estado desc')
        ;
        
        if ($informesForm->fecha_desde){
                    $informesForm->fecha_desde= Yii::$app->formatter->asDate( date_create_from_format('d/m/Y',
                                $informesForm->fecha_desde), 'php:Y-m-d' );         
        }
        
        if ($informesForm->fecha_hasta){
                    $informesForm->fecha_hasta= Yii::$app->formatter->asDate( date_create_from_format('d/m/Y',
                                $informesForm->fecha_hasta), 'php:Y-m-d' );         
        }
        
        
        
        $query
            ->andWhere(['in','oa.user_id',$informesForm->responsable ? $informesForm->responsable : 0 ]) //si viene sin usuarios no trae nada
            ->andWhere(['in','oa.estado_asignacion',EstadosAsignaciones::ESTADO_TOMADA])
            ->andFilterWhere(['>=','date(o.fechahora_inicio)',$informesForm->fecha_desde])
            ->andFilterWhere(['<=','date(o.fechahora_inicio)',$informesForm->fecha_hasta])
            ;
        //}
        
        $dataProvider= new ActiveDataProvider([
            'query'=>$query,
            'pagination'=>false,
            'sort'=>false,
        ]);
        
//        $dataProvider= new ArrayDataProvider([
//            'allModels'=>$query->all(),
//            'pagination'=>false,
//            'sort'=>false,
//        ]);
        
        
        
        return $this->render('linea-tiempo',['dataProvider'=>$dataProvider, 
                    "informesForm" => $informesForm,
                    "ids_usuarios_data" => $ids_usuarios_data,
                    "ids_tipostarea_data" => $ids_tipostarea_data,
                    "ids_estados_data" => $ids_estados_data,
                    'query' => $query
                ]);        
    }
    
    
    
    public function actionInformeOrdenes(){
        $informesForm = new InformesOrdenesForm();
        
        return $this->renderContent('Nada por aquí');
        
        
    }
}
