<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Proyectos;
use frontend\models\search\ProyectosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\filters\VerbFilter;
use common\models\PermisosHelpers;
use common\models\MensajesHelpers;
use frontend\models\NumeroOrden;
use frontend\models\EstadosProyectos;
use frontend\models\EstadosArchivos;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;
use yii\db\Query;

use frontend\models\ProyectosPersonas;
use frontend\models\ProyectosMensajes;
use frontend\models\search\InformesSearch;

use yii\data\Pagination;
use yii\helpers\Html;

use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
/**
 * ProyectosController implements the CRUD actions for Proyectos model.
 */
class ProyectosController extends Controller
{
  public function behaviors()
  {
     return [
      'access' => [
             'class' => \yii\filters\AccessControl::className(),
             'only' => ['index', 'view','create', 'update', 'delete', 
                        'procesar','valorar','determinar-puntos-inflexion','data','heatmap',
                        'mensajesxpersona',
                        'mensajeria','mensajeria-linea-tiempo','mensajeria-totales'],
             'rules' => [
                 [
                     'actions' => ['index', 'view', 'procesar','valorar'
                         ,'determinar-puntos-inflexion','data','heatmap',
                        'mensajesxpersona',
                        'mensajeria','mensajeria-linea-tiempo','mensajeria-totales'],
                     'allow' => true,
                     'roles' => ['@'],
                     'matchCallback' => function ($rule, $action) {
                      return PermisosHelpers::requerirMinimoRol('Usuario')
                      && PermisosHelpers::requerirEstado('Activo');
                     }
                 ],
                  [
                     'actions' => [ 'create', 'update'],
                     'allow' => true,
                     'roles' => ['@'],
                     'matchCallback' => function ($rule, $action) {
                      return PermisosHelpers::requerirMinimoRol('Usuario')
                      && PermisosHelpers::requerirEstado('Activo');
                     }
                 ],
                  [
                     'actions' => [  'delete'],
                     'allow' => true,
                     'roles' => ['@'],
                     'matchCallback' => function ($rule, $action) {
                      return PermisosHelpers::requerirMinimoRol('Usuario')
                      && PermisosHelpers::requerirEstado('Activo');
                     }
                 ],
             ],
         ],

        'verbs' => [
                  'class' => VerbFilter::className(),
                  'actions' => [
                      'delete' => ['post'],
                      'anular' => ['post'],
                  ],
              ],
          ];
  }

    /**
     * Lists all Proyectos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProyectosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Proyectos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Proyectos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Proyectos();

        if ($model->load(Yii::$app->request->post())) { // && $model->save()
            $error = null;
            
            $transaction = \Yii::$app->db->beginTransaction(); //El crear involucra varias tablas
            try {
               if (!$model->save()){
                   throw new Exception(VarDumper::dumpAsString($model->errors));
               }
            } catch (Exception $e) {
                Yii::error(VarDumper::dumpAsString($e->getMessage()));
                $error = 'No se pudo guardar el registro';
            }            
            
            if($error == null){
                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                $transaction->rollback();
               throw new ServerErrorHttpException('Error al guardar los datos');
            }            
            
        } else {
            $model->fechahora_inicio=date("Y-m-d H:i:s"); //si estoy creando sugiero como fecha de inicio la fecha actual
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Procesar en python el texto y recuperar la tabla de datos
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionData($id)
    {
        $model = $this->findModel($id);
        $dataStr = "";
        $error = null;
        $resultado = null;
        try {
           foreach($model->proyectosArchivos as $archivo){
               $dataStr .= $archivo->contenido . "\n";
           }
           if($dataStr!=""){
               $resultado = \Yii::$app->flask->data($dataStr);
           }

        } catch (Exception $e) {
            Yii::error(VarDumper::dumpAsString($e->getMessage()));
            $error = "Error al procesar datos";
        }            

        if($error == null){
            //return $this->redirect(['view', 'id' => $model->id]);
        }else{
           throw new ServerErrorHttpException('Error al procesar');
        }            
        
        return $this->render('data', [
            'model' => $model,
            'resultado' => json_decode($resultado),
            'mensajes' => $mensajes,
        ]);
    }
    
    /**
     * Mostrar un gráfico con la cantidad de mensajes por día y hora
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionHeatmap($p_id)
    {
        $form=new InformesSearch();
        
        $form->load(Yii::$app->request->get());
        
        $model = $this->findModel($p_id);
        $form->p_id=$model->id;

        $ids_personas_data= ArrayHelper::map($model->proyectosPersonas,'id','persona');
        
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;        
        
        $query= (new Query())->select([
                'p.id as p_id',
                'p.numero_proyecto as p_numero_proyecto',
                'p.fechahora_inicio as p_fechahora_inicio',
                //'date_format(pm.fecha,"%e/%c/%y") as pm_fecha', //lo cambio para éste caso
                'date_format(pm.fecha,"%d/%m/%y") as pm_fecha', //lo cambio para éste caso
                //'pm.fecha as pm_fecha',
                'ifnull(pm.lat,ifnull(r.lat,p.lat)) as latitud',
                'ifnull(pm.lng,ifnull(r.lng,p.lng)) as longitud',
                'dayofweek(pm.fecha) as dia_semana',
                'dayname(pm.fecha) as nombre_dia',
                'date_format(pm.fecha,"%H:%i") as pm_hora',
                'if(hour(pm.fecha)>=6 and hour(pm.fecha)<12, "mañana", if(hour(pm.fecha)>=12 and hour(pm.fecha)<22,"tarde", "noche")) as horario',
                'pm.persona as pm_id_persona',
                'r.persona as r_persona',
                'r.edad as r_edad',
                'r.genero as r_genero',
                'pm.texto_original as pm_texto_original',
                'pm.clasificacion as pm_clasificacion',
                'pm.es_punto_inflexion as pm_es_punto_inflexion',
                ])
                ->from('proyectos as p')
                ->join('JOIN','proyectos_mensajes as pm','pm.proyecto=p.id')
                ->join('JOIN','proyectos_personas as r','pm.persona=r.id')
                ->orderBy('pm.fecha, pm.persona')
        ;
        
        if($form->validate()){
            $query->andWhere(['p.id'=>$form->p_id])
            ->andFilterWhere(['>=','pm.fecha',$form->fechaDesde])
            ->andFilterWhere(['<=','pm.fecha',$form->fechaHasta])
            ->andFilterWhere(['like', 'r.persona', $form->r_persona])        
            ->andFilterWhere(['like', 'pm.texto_original', $form->pm_texto_original])        
            ->andFilterWhere(['in', 'pm.persona', $form->pm_id_persona])        
            ;
        }
        
        if(!$esAdmin){//refuerzo que solo pueda operar con proyectos creados por el usuario si no es admin
            $query->andWhere(['p.created_by'=>$userId]);
        }
        
        $model = $query->all();

        $dataStr = "";
        $error = null;
        $resultado = null;
        try {
           foreach($model as $mensaje){
               $dataStr .= $mensaje['pm_fecha']." ". $mensaje['pm_hora']." - ".$mensaje['r_persona'].": ".$mensaje['pm_texto_original']."\n";
           }
           //\Yii::warning(VarDumper::dumpAsString($dataStr));
           if($dataStr!=""){
               $resultado = \Yii::$app->flask->heatmap($dataStr);
           }

        } catch (Exception $e) {
            Yii::error(VarDumper::dumpAsString($e->getMessage()));
            $error = "Error al procesar mapa de calor";
        }            

        if($error == null){
            //return $this->redirect(['view', 'id' => $model->id]);
        }else{
           throw new ServerErrorHttpException('Error al procesar');
        }            
        
        return $this->render('heatmap', [
            'model' => $model,
            'resultado' => $resultado,
            'mensajes' => $mensajes,
            'ids_personas_data' => $ids_personas_data,
            'form' => $form,
        ]);
    }
    
    /**
     * Mensajes por Persona
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionMensajesxpersona($p_id)
    {
        $form=new InformesSearch();
        
        $form->load(Yii::$app->request->get());
        
        $model = $this->findModel($p_id);
        $form->p_id=$model->id;

        $ids_personas_data= ArrayHelper::map($model->proyectosPersonas,'id','persona');
        
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;        
        
        $query= (new Query())->select([
                'p.id as p_id',
                'p.numero_proyecto as p_numero_proyecto',
                'p.fechahora_inicio as p_fechahora_inicio',
                //'date_format(pm.fecha,"%e/%c/%y") as pm_fecha', //lo cambio para éste caso
                'date_format(pm.fecha,"%d/%m/%y") as pm_fecha', //lo cambio para éste caso
                //'pm.fecha as pm_fecha',
                'ifnull(pm.lat,ifnull(r.lat,p.lat)) as latitud',
                'ifnull(pm.lng,ifnull(r.lng,p.lng)) as longitud',
                'dayofweek(pm.fecha) as dia_semana',
                'dayname(pm.fecha) as nombre_dia',
                'date_format(pm.fecha,"%H:%i") as pm_hora',
                'if(hour(pm.fecha)>=6 and hour(pm.fecha)<12, "mañana", if(hour(pm.fecha)>=12 and hour(pm.fecha)<22,"tarde", "noche")) as horario',
                'pm.persona as pm_id_persona',
                'r.persona as r_persona',
                'r.edad as r_edad',
                'r.genero as r_genero',
                'pm.texto_original as pm_texto_original',
                'pm.clasificacion as pm_clasificacion',
                'pm.es_punto_inflexion as pm_es_punto_inflexion',
                ])
                ->from('proyectos as p')
                ->join('JOIN','proyectos_mensajes as pm','pm.proyecto=p.id')
                ->join('JOIN','proyectos_personas as r','pm.persona=r.id')
                ->orderBy('pm.fecha, pm.persona')
        ;
        
        if($form->validate()){
            $query->andWhere(['p.id'=>$form->p_id])
            ->andFilterWhere(['>=','pm.fecha',$form->fechaDesde])
            ->andFilterWhere(['<=','pm.fecha',$form->fechaHasta])
            ->andFilterWhere(['like', 'r.persona', $form->r_persona])        
            ->andFilterWhere(['like', 'pm.texto_original', $form->pm_texto_original])        
            ->andFilterWhere(['in', 'pm.persona', $form->pm_id_persona])        
            ;
        }
        
        if(!$esAdmin){//refuerzo que solo pueda operar con proyectos creados por el usuario si no es admin
            $query->andWhere(['p.created_by'=>$userId]);
        }
        
        $model = $query->all();

        $dataStr = "";
        $error = null;
        $resultado = null;
        try {
//           foreach($model->proyectosArchivos as $archivo){
//               $dataStr .= $archivo->contenido . "\n";
//           }
           foreach($model as $mensaje){
               $dataStr .= $mensaje['pm_fecha']." ". $mensaje['pm_hora']." - ".$mensaje['r_persona'].": ".$mensaje['pm_texto_original']."\n";
           }
           \Yii::warning(VarDumper::dumpAsString($dataStr));
           if($dataStr!=""){
               $resultado = \Yii::$app->flask->mensajesxpersona($dataStr);
           }

        } catch (Exception $e) {
            Yii::error(VarDumper::dumpAsString($e->getMessage()));
            $error = "Error al procesar mensajes x persona";
        }            

        if($error == null){
            //return $this->redirect(['view', 'id' => $model->id]);
        }else{
           throw new ServerErrorHttpException('Error al procesar');
        }            
        
        return $this->render('mensajesxpersona', [
            'model' => $model,
            'resultado' => $resultado,
            'mensajes' => $mensajes,
            'ids_personas_data' => $ids_personas_data,
            'form' => $form,
        ]);
    }
    
    /**
     * Estadísticas generales
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionEstadisticas($p_id)
    {
        $form=new InformesSearch();
        
        $form->load(Yii::$app->request->get());
        
        $model = $this->findModel($p_id);
        $stopwords = "";
        if($model->stopwords0->stopwords){
            $stopwords = $model->stopwords0->stopwords;
        }
        if($model->stopwords_lista){
            $stopwords .= " ".$model->stopwords_lista;
        }
        
        $form->p_id=$model->id;

        $ids_personas_data= ArrayHelper::map($model->proyectosPersonas,'id','persona');
        
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;        
        
        $query= (new Query())->select([
                'p.id as p_id',
                'p.numero_proyecto as p_numero_proyecto',
                'p.fechahora_inicio as p_fechahora_inicio',
                'date_format(pm.fecha,"%e/%c/%y") as pm_fecha', //lo cambio para éste caso
                //'pm.fecha as pm_fecha',
                'ifnull(pm.lat,ifnull(r.lat,p.lat)) as latitud',
                'ifnull(pm.lng,ifnull(r.lng,p.lng)) as longitud',
                'dayofweek(pm.fecha) as dia_semana',
                'dayname(pm.fecha) as nombre_dia',
                'date_format(pm.fecha,"%H:%i") as pm_hora',
                'if(hour(pm.fecha)>=6 and hour(pm.fecha)<12, "mañana", if(hour(pm.fecha)>=12 and hour(pm.fecha)<22,"tarde", "noche")) as horario',
                'pm.persona as pm_id_persona',
                'r.persona as r_persona',
                'r.edad as r_edad',
                'r.genero as r_genero',
                'pm.texto_original as pm_texto_original',
                'pm.texto as pm_texto',
                'pm.emojis as pm_emojis',
                'pm.clasificacion as pm_clasificacion',
                'pm.es_punto_inflexion as pm_es_punto_inflexion',
                ])
                ->from('proyectos as p')
                ->join('JOIN','proyectos_mensajes as pm','pm.proyecto=p.id')
                ->join('JOIN','proyectos_personas as r','pm.persona=r.id')
                ->orderBy('pm.fecha, pm.persona')
        ;
        
        if($form->validate()){
            $query->andWhere(['p.id'=>$form->p_id])
            ->andFilterWhere(['>=','pm.fecha',$form->fechaDesde])
            ->andFilterWhere(['<=','pm.fecha',$form->fechaHasta])
            ->andFilterWhere(['like', 'r.persona', $form->r_persona])        
            ->andFilterWhere(['like', 'pm.texto_original', $form->pm_texto_original])        
            ->andFilterWhere(['in', 'pm.persona', $form->pm_id_persona])        
            ;
        }
        
        if(!$esAdmin){//refuerzo que solo pueda operar con proyectos creados por el usuario si no es admin
            $query->andWhere(['p.created_by'=>$userId]);
        }
        
        $model = $query->all();

        $dataStr = "";
        $error = null;
        $resultado = null;
        $personas=[];
        $palabras_mas_usadas = [];
        $palabras_por_persona = [];
        $emojis_mas_usados = [];
        try {
//           foreach($model->proyectosArchivos as $archivo){
//               $dataStr .= $archivo->contenido . "\n";
//           }
           foreach($model as $mensaje){
               $texto = MensajesHelpers::removerStopwords($mensaje['pm_texto'],$stopwords);
               $texto.= $mensaje['pm_emojis'];
               
               $dataStr .= $mensaje['pm_fecha']." ". $mensaje['pm_hora']." - ".$mensaje['r_persona'].": ".$texto."\n";
               $personas[$mensaje['r_persona']]=$mensaje['r_persona'];
           }
           $personasNombres = array_keys($personas);
           //\Yii::warning(VarDumper::dumpAsString($dataStr));
           if($dataStr!=""){
               $resultado = \Yii::$app->flask->estadisticas($dataStr, $personasNombres);
               $r = json_decode($resultado);
               if (count($r==3)){
                   $palabras_mas_usadas = $r[0];
                   $palabras_por_persona = $r[1];
                   $emojis_mas_usados=json_decode($r[2]);
               }
           }

        } catch (Exception $e) {
            Yii::error(VarDumper::dumpAsString($e->getMessage()));
            $error = "Error al procesar estadísticas";
        }            

        if($error == null){
            //return $this->redirect(['view', 'id' => $model->id]);
        }else{
           throw new ServerErrorHttpException('Error al procesar');
        }            
        
        return $this->render('estadisticas', [
            'model' => $model,
            'resultado' => $resultado,
            'mensajes' => $mensajes,
            'ids_personas_data' => $ids_personas_data,
            'form' => $form,
            'palabras_mas_usadas' => $palabras_mas_usadas,
            'palabras_por_persona' => $palabras_por_persona,
            'emojis_mas_usados' => $emojis_mas_usados,
            
        ]);
    }
    
    
    public function actionMensajeria($p_id){
        $form=new InformesSearch();
        
        $form->load(Yii::$app->request->get());
        
        $model = $this->findModel($p_id);
        $form->p_id=$model->id;

        $ids_personas_data= ArrayHelper::map($model->proyectosPersonas,'id','persona');
        
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;        
        
        $query= (new Query())->select([
                'p.id as p_id',
                'p.numero_proyecto as p_numero_proyecto',
                'p.fechahora_inicio as p_fechahora_inicio',
                'date_format(pm.fecha,"%Y-%m-%d") as pm_fecha',
                //'pm.fecha as pm_fecha',
                'ifnull(pm.lat,ifnull(r.lat,p.lat)) as latitud',
                'ifnull(pm.lng,ifnull(r.lng,p.lng)) as longitud',
                'dayofweek(pm.fecha) as dia_semana',
                'dayname(pm.fecha) as nombre_dia',
                'date_format(pm.fecha,"%H:%i") as pm_hora',
                'if(hour(pm.fecha)>=6 and hour(pm.fecha)<12, "mañana", if(hour(pm.fecha)>=12 and hour(pm.fecha)<22,"tarde", "noche")) as horario',
                'pm.persona as pm_id_persona',
                'r.persona as r_persona',
                'r.edad as r_edad',
                'r.genero as r_genero',
                'pm.texto_original as pm_texto_original',
                'pm.texto as pm_texto',
                'pm.clasificacion as pm_clasificacion',
                'pm.es_punto_inflexion as pm_es_punto_inflexion',
                ])
                ->from('proyectos as p')
                ->join('JOIN','proyectos_mensajes as pm','pm.proyecto=p.id')
                ->join('JOIN','proyectos_personas as r','pm.persona=r.id')
                ->orderBy('pm.fecha, pm.persona')
        ;
        
        if($form->validate()){
            $query->andWhere(['p.id'=>$form->p_id])
            ->andFilterWhere(['>=','pm.fecha',$form->fechaDesde])
            ->andFilterWhere(['<=','pm.fecha',$form->fechaHasta])
            ->andFilterWhere(['like', 'r.persona', $form->r_persona])        
            ->andFilterWhere(['like', 'pm.texto_original', $form->pm_texto_original])        
            ->andFilterWhere(['in', 'pm.persona', $form->pm_id_persona])        
            ;
        }
        
        if(!$esAdmin){//refuerzo que solo pueda operar con proyectos creados por el usuario si no es admin
            $query->andWhere(['p.created_by'=>$userId]);
        }
        
        $dataProvider= new ActiveDataProvider([
            'query'=>$query,
            'pagination'=>false,
            'sort'=>false,
        ]);
        //\yii::warning("Form:".VarDumper::dumpAsString($form));
        return $this->render('mensajes',
                [
                    'dataProvider'=>$dataProvider, 
                    'form' => $form,
                    'model'=>$model,
                    'ids_personas_data' => $ids_personas_data,
                ]
                );        
    }
    
    public function actionMensajeriaTotales($p_id){
        $form=new InformesSearch();
        $form->load(Yii::$app->request->get());
        $model = $this->findModel($p_id);
        $form->p_id=$model->id;
        $ids_personas_data= ArrayHelper::map($model->proyectosPersonas,'id','persona');
        
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;        
        $query= (new Query())->select([
                'p.id as p_id',
                'p.numero_proyecto as p_numero_proyecto',
                'p.fechahora_inicio as p_fechahora_inicio',
                //'pm.fecha as pm_fecha',
                'date_format(pm.fecha,"%Y-%m-%d") as pm_fecha',
                'ifnull(pm.lat,ifnull(r.lat,p.lat)) as latitud',
                'ifnull(pm.lng,ifnull(r.lng,p.lng)) as longitud',
                'dayofweek(pm.fecha) as dia_semana',
                'dayname(pm.fecha) as nombre_dia',
                'date_format(pm.fecha,"%H:%i") as pm_hora',
                'if(hour(pm.fecha)>=6 and hour(pm.fecha)<12, "mañana", if(hour(pm.fecha)>=12 and hour(pm.fecha)<22,"tarde", "noche")) as horario',
                'pm.persona as pm_id_persona',
                'r.persona as r_persona',
                'r.edad as r_edad',
                'r.genero as r_genero',
                'pm.texto_original as pm_texto_original',
                'pm.clasificacion as pm_clasificacion',
                'pm.es_punto_inflexion as pm_es_punto_inflexion',
                ])
                ->from('proyectos as p')
                ->join('JOIN','proyectos_mensajes as pm','pm.proyecto=p.id')
                ->join('JOIN','proyectos_personas as r','pm.persona=r.id')
                ->orderBy('pm.fecha, pm.persona')
        ;

        if($form->validate()){
            $query->andWhere(['p.id'=>$form->p_id])
            ->andFilterWhere(['>=','pm.fecha',$form->fechaDesde])
            ->andFilterWhere(['<=','pm.fecha',$form->fechaHasta])
            ->andFilterWhere(['like', 'r.persona', $form->r_persona])        
            ->andFilterWhere(['like', 'pm.texto_original', $form->pm_texto_original])        
            ->andFilterWhere(['in', 'pm.persona', $form->pm_id_persona])        
            ;
        }
        
        if(!$esAdmin){//refuerzo que solo pueda operar con proyectos creados por el usuario si no es admin
            $query->andWhere(['p.created_by'=>$userId]);
        }
        
        $model = $query->all();

        $key='r_persona';
        $resultado[$key]=[];
        $key='nombre_dia';
        $resultado[$key]=[];
        $key='horario';
        $resultado[$key]=[];
        $key='pm_fecha';
        $resultado[$key]=[];
        $key='r_edad';
        $resultado[$key]=[];
        $key='r_genero';
        $resultado[$key]=[];
        $key='pm_clasificacion';
        $resultado[$key]=[];
        $key='pm_es_punto_inflexion';
        $resultado[$key]=[];
        foreach($model as $mensaje){
            //m_fecha
            $key='pm_fecha';
            if($mensaje[$key]){
                if(array_key_exists($mensaje[$key],$resultado[$key])){
                    $resultado[$key][$mensaje[$key]]+=1;
                }else{
                    $resultado[$key][$mensaje[$key]]=1;
                }
            }
            //nombre_dia
            $key='nombre_dia';
            if($mensaje[$key]){
                if(array_key_exists($mensaje[$key],$resultado[$key])){
                    $resultado[$key][$mensaje[$key]]+=1;
                }else{
                    $resultado[$key][$mensaje[$key]]=1;
                }
            }
            //horario
            $key='horario';
            if($mensaje[$key]){
                if(array_key_exists($mensaje[$key],$resultado[$key])){
                    $resultado[$key][$mensaje[$key]]+=1;
                }else{
                    $resultado[$key][$mensaje[$key]]=1;
                }
            }
            //m_remitente
            $key='r_persona';
            if($mensaje[$key]){
                if(array_key_exists($mensaje[$key],$resultado[$key])){
                    $resultado[$key][$mensaje[$key]]+=1;
                }else{
                    $resultado[$key][$mensaje[$key]]=1;
                }
            }
            //r_edad
            $key='r_edad';
            if($mensaje[$key]){
                if(array_key_exists($mensaje[$key],$resultado[$key])){
                    $resultado[$key][$mensaje[$key]]+=1;
                }else{
                    $resultado[$key][$mensaje[$key]]=1;
                }
            }
            //r_genero
            $key='r_genero';
            if($mensaje[$key]){
                if(array_key_exists($mensaje[$key],$resultado[$key])){
                    $resultado[$key][$mensaje[$key]]+=1;
                }else{
                    $resultado[$key][$mensaje[$key]]=1;
                }
            }
            //pm_clasificacion
            $key='pm_clasificacion';
            if($mensaje[$key]){
                if(array_key_exists($mensaje[$key],$resultado[$key])){
                    $resultado[$key][$mensaje[$key]]+=1;
                }else{
                    $resultado[$key][$mensaje[$key]]=1;
                }
            }
            //pm_es_punto_inflexion
            $key='pm_es_punto_inflexion';
            if($mensaje[$key]){
                if(array_key_exists($mensaje[$key],$resultado[$key])){
                    $resultado[$key][$mensaje[$key]]+=1;
                }else{
                    $resultado[$key][$mensaje[$key]]=1;
                }
            }
        }
        $key='pm_fecha';
        if (($resultado[$key])&&(count($resultado[$key])>0)) arsort($resultado[$key]);
        /*$key='dia_semana';
        if (($resultado[$key])&&(count($resultado[$key])>0)) arsort($resultado[$key]);*/
        $key='nombre_dia';
        if (($resultado[$key])&&(count($resultado[$key])>0)) arsort($resultado[$key]);
        $key='horario';
        if (($resultado[$key])&&(count($resultado[$key])>0)) arsort($resultado[$key]);
        $key='r_persona';
        if (($resultado[$key])&&(count($resultado[$key])>0)) arsort($resultado[$key]);
        $key='r_edad';
        if (($resultado[$key])&&(count($resultado[$key])>0)) arsort($resultado[$key]);
        $key='r_genero';
        if (($resultado[$key])&&(count($resultado[$key])>0)) arsort($resultado[$key]);
        $key='pm_clasificacion';
        if (($resultado[$key])&&(count($resultado[$key])>0)) arsort($resultado[$key]);
        $key='pm_es_punto_inflexion';
        if (($resultado[$key])&&(count($resultado[$key])>0)) arsort($resultado[$key]);

        $tableArray=[];
        
        $keys=array_keys($resultado);
        foreach($keys as $key){
            foreach($resultado[$key] as $etiqueta => $cantidad){
                $tableArray[]=['clave'=>$key,'etiqueta'=>$etiqueta,'cantidad' => $cantidad];
            }
        }
        $provider = new ArrayDataProvider([
            'allModels' => $tableArray, 
            'pagination' => false,//[ 'pageSize' => 10, ], 
        ]);
        return $this->render("mensajes-totales", [
            "model" => $model,
            'provider'=>$provider, 
            "form" => $form, 
            'resultado'=>$tableArray,
            'ids_personas_data' => $ids_personas_data,
            
        ]);
    }
    
    public function actionMensajeriaLineaTiempo($p_id){
        $form=new InformesSearch();
        $form->load(Yii::$app->request->get());
        $modelProyecto = $this->findModel($p_id);
        $form->p_id=$modelProyecto->id;
        $ids_personas_data= ArrayHelper::map($modelProyecto->proyectosPersonas,'id','persona');
        
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;        
        $query= (new Query())->select([
                'p.id as p_id',
                'p.numero_proyecto as p_numero_proyecto',
                'p.fechahora_inicio as p_fechahora_inicio',
                'date_format(pm.fecha,"%Y-%m-%d") as pm_fecha',
                //'pm.fecha as pm_fecha',
                'ifnull(pm.lat,ifnull(r.lat,p.lat)) as latitud',
                'ifnull(pm.lng,ifnull(r.lng,p.lng)) as longitud',
                'dayofweek(pm.fecha) as dia_semana',
                'dayname(pm.fecha) as nombre_dia',
                'date_format(pm.fecha,"%H:%i") as pm_hora',
                'if(hour(pm.fecha)>=6 and hour(pm.fecha)<12, "mañana", if(hour(pm.fecha)>=12 and hour(pm.fecha)<22,"tarde", "noche")) as horario',
                'pm.persona as pm_id_persona',
                'r.persona as r_persona',
                'r.edad as r_edad',
                'r.genero as r_genero',
                'pm.texto_original as pm_texto_original',
                'pm.clasificacion as pm_clasificacion',
                'pm.es_punto_inflexion as pm_es_punto_inflexion',
                ])
                ->from('proyectos as p')
                ->join('JOIN','proyectos_mensajes as pm','pm.proyecto=p.id')
                ->join('JOIN','proyectos_personas as r','pm.persona=r.id')
                ->orderBy('pm.fecha, pm.persona')
        ;
        
        if($form->validate()){
            $query->andWhere(['p.id'=>$form->p_id])
            ->andFilterWhere(['>=','pm.fecha',$form->fechaDesde])
            ->andFilterWhere(['<=','pm.fecha',$form->fechaHasta])
            ->andFilterWhere(['like', 'r.persona', $form->r_persona])        
            ->andFilterWhere(['like', 'pm.texto_original', $form->pm_texto_original])        
            ->andFilterWhere(['in', 'pm.persona', $form->pm_id_persona])        
            ;
        }
        
        if(!$esAdmin){//refuerzo que solo pueda operar con proyectos creados por el usuario si no es admin
            $query->andWhere(['p.created_by'=>$userId]);
        }
        
        $model = $query->all();
        $provider = new ArrayDataProvider([
            'allModels' => $tableArray, 
            'sort' => [ 'attributes' => 
                 ['etiqueta', 'cantidad'], 
            ],
            'key' => 'etiqueta',
            'pagination' => false,//[ 'pageSize' => 10, ], 
        ]);
        
        $primerFecha = new \DateTime('2900-01-01');// 'now'
        $ultimaFecha = new \DateTime('1900-01-01'); // '2007-04-19 12:50'
        //$primerFecha->setTimestamp(strtotime('1901-02-02'));
        $remitentes=array();
        $contIndicesRemitentes=0;
        foreach($model as $m){
            if(strtotime($m['pm_fecha'].' '.$m['pm_hora']) < strtotime(date_format($primerFecha,'Y/m/d H:m:s'))){
                $primerFecha=new \DateTime($m['pm_fecha'].' '.$m['pm_hora']);
            }
            if(strtotime($m['pm_fecha'].' '.$m['pm_hora']) > strtotime(date_format($ultimaFecha,'Y/m/d H:m:s'))){
                $ultimaFecha=new \DateTime($m['pm_fecha'].' '.$m['pm_hora']);
            }
            
            if(!array_key_exists($m['r_persona'],$remitentes)){
                $remitentes[$m['r_persona']]=$contIndicesRemitentes++;
            }
        }
        
        if(isset($form->intervalo_minutos) && intval($form->intervalo_minutos)>=1 && intval($form->intervalo_minutos)<=10000){
            $intervalo_minutos=intval($form->intervalo_minutos);
        }else{
            $intervalo_minutos = 30;
        }
        $form->intervalo_minutos=$intervalo_minutos;
        
        $primerFecha->setTime(date_format($primerFecha,'H'),date_format($primerFecha,'i'),0);
        $primerNum=$primerFecha->getTimestamp();
        $siguienteNum=$primerNum+60*$intervalo_minutos; //30
        $ultimoNum=$ultimaFecha->getTimestamp();
        $cont=0;
        $lineaTiempo=array();
        //$lineaTiempo[$cont++]=$primerNum;
        while($siguienteNum<=$ultimoNum){
            $lineaTiempo[$cont]=array();
            $lineaTiempo[$cont][]=$siguienteNum;
            foreach($remitentes as $r=>$ind){
                $lineaTiempo[$cont][]="&nbsp;";
            }
            $cont++;
            $siguienteNum+=60*$intervalo_minutos;//30
        }
        $lineaTiempo[$cont]=array();
        $lineaTiempo[$cont][]=$siguienteNum;
        foreach($remitentes as $r=>$ind){
            $lineaTiempo[$cont][]="&nbsp;";
        }
        
        
        foreach($model as $m){
            $fechaMensaje = new \DateTime($m['pm_fecha'].' '.$m['pm_hora']);
            $fechaMensaje->setTime(date_format($fechaMensaje,'H'),date_format($fechaMensaje,'i'),0);
            $fechaMensajeTimestamp=$fechaMensaje->getTimestamp();
            $diferenciaTimestamps=$fechaMensajeTimestamp - $primerNum;
            $indiceTimestamp= intval($diferenciaTimestamps / (60*$intervalo_minutos));//30
            $indRemitente=$remitentes[$m['r_persona']];
            //echo "indice timestamp:".$indiceTimestamp;
            //$lineaTiempo[$indiceTimestamp][$indRemitente+1].="-".Html::encode($m['pm_texto_original'])."<br/>"; //$lineaTiempo[$indiceTimestamp][1]['cristina'].$m['m_mensaje']."<br/>";
            
            $alert = "";
            $nombreClasificacion= "neutro";
            if (isset($m['pm_clasificacion']) && $m['pm_clasificacion']==-1){
                $alert="lightcoral";
                $nombreClasificacion = "negativo";
            }elseif(isset($m['pm_clasificacion']) && $m['pm_clasificacion']==1){
                $alert="lightgreen";
                $nombreClasificacion = "positivo";
            }else{
                $alert="white";
            }
            $puntoInflexion = $m['pm_es_punto_inflexion']?"*":"";
            $pm_texto_original = Html::encode($m['pm_texto_original']);
            if($m['pm_es_punto_inflexion']){
                $pm_texto_original="<strong>".$pm_texto_original."</strong>";
            }
            //$titlespan = "clasificación:".m['pm_clasificacion'].$m['pm_es_punto_inflexion']?" Punto Inflexiòn":"";
            $titlespan = $nombreClasificacion . ( $m['pm_es_punto_inflexion']?" - Punto Inflexión":"");
            $lineaTiempo[$indiceTimestamp][$indRemitente+1].=
                        "<span style='background-color:$alert;' title='$titlespan'>". //class='alert alert-$alert'
                        //"(${m['pm_clasificacion']})-".
                        //"($puntoInflexion)-".
                        "-".$pm_texto_original.
                        "</span><br/>";

                        //$lineaTiempo[$indiceTimestamp][1]['cristina'].$m['m_mensaje']."<br/>";
        }
        
        $usuariosData=array();//$remitentes;
        foreach($remitentes as $rt=>$v){
            $usuariosData[$rt]=$rt;
        }
        
        return $this->render("mensajes-linea-tiempo", [
            "model" => $model,
            "form" => $form, 
            //"pages" => $pages,
            'cadena' => $cadena,
            'primerFecha'=>$primerFecha,
            'ultimaFecha'=>$ultimaFecha,
            'lineaTiempo'=>$lineaTiempo,
            'remitentes'=>$remitentes,
            'usuariosData'=>$usuariosData,
            'ids_personas_data' => $ids_personas_data,
        ]);
    }
    
    
    /**
     * Determinar puntos de inflexión
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionDeterminarPuntosInflexion($id)
    {
        $model = $this->findModel($id);
        $estadoIniciado = $model->estado_proyecto == EstadosProyectos::ESTADO_INICIADO;

        if(!$estadoIniciado){
            throw new ServerErrorHttpException('Solo se pueden valorar mensajes de proyectos en estado Iniciado');
        }
        
        $mensajes = [];

        $transaction = \Yii::$app->db->beginTransaction(); //El crear involucra varias tablas
        $error = null;
        $puntos_encontrados = 0;
        try {
            
           foreach($model->proyectosPersonas as $persona){
               $mensajeAnterior = $persona->proyectosMensajes[0];
               foreach($persona->proyectosMensajes as $mensaje){//los mensajes ya vienen ordenados por fecha
                    $mensaje->es_punto_inflexion=false;
                    if($mensaje->clasificacion!=$mensajeAnterior->clasificacion){
                        $mensaje->es_punto_inflexion=true;
                        $puntos_encontrados++;
                    }
                    $mensajeAnterior=$mensaje;
                    $mensaje->save();
               }
           }
           if (!$model->save()){
               throw new Exception(VarDumper::dumpAsString($model->errors));
           }
        } catch (Exception $e) {
            Yii::error(VarDumper::dumpAsString($e->getMessage()));
            $error = "Error durante el procesamiento emocional de los mensajes";
        }            

        if($error == null){
            $transaction->commit();
            //return $this->redirect(['view', 'id' => $model->id]);
        }else{
            $transaction->rollback();
           throw new ServerErrorHttpException('Error al guardar los datos');
        }            
        
        return $this->render('puntos-inflexion', [
            'model' => $model,
            'puntos_encontrados' => $puntos_encontrados,
        ]);
    }
    
    /**
     * Valorar los mensajes
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionValorar($id)
    {
        $model = $this->findModel($id);
        $estadoIniciado = $model->estado_proyecto == EstadosProyectos::ESTADO_INICIADO;

        if(!$estadoIniciado){
               throw new ServerErrorHttpException('Solo se pueden valorar mensajes de proyectos en estado Iniciado');
        }
        
        $mensajes = [];

        $transaction = \Yii::$app->db->beginTransaction(); //El crear involucra varias tablas
        $error = null;
        
        try {
           
           foreach($model->proyectosMensajes as $mensaje){
               $mensajes[] = $mensaje;
               $mensaje->clasificacion = $mensaje->valorEmocional();
               $mensaje->save();
           }

           if (!$model->save()){//guardo el autor del procesado
               throw new Exception(VarDumper::dumpAsString($model->errors));
           }
        } catch (Exception $e) {
            Yii::error(VarDumper::dumpAsString($e->getMessage()));
            $error = "Error durante el procesamiento emocional de los mensajes";
        }            

        if($error == null){
            $transaction->commit();
            //return $this->redirect(['view', 'id' => $model->id]);
        }else{
            $transaction->rollback();
           throw new ServerErrorHttpException('Error al guardar los datos');
        }            
        
        return $this->render('valorar', [
            'model' => $model,
            'mensajes' => $mensajes,
        ]);
    }
    
    
    /**
     * Actualiza los mensajes con los contenidos del los archivos
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionProcesar($id)
    {
        $model = $this->findModel($id);
        $estadoIniciado = $model->estado_proyecto == EstadosProyectos::ESTADO_INICIADO;

        if(!$estadoIniciado){
               throw new ServerErrorHttpException('Solo se pueden procesar mensajes de proyectos en estado Iniciado');
        }
        
        $texto = "";
        $mensajes = [];
        foreach($model->proyectosArchivos as $archivo){
            $texto.= $archivo->contenido."\n\r";
            //$m = mb_explode("\n",$archivo->contenido);
            if($archivo->contenido){
                $m = mb_split("\n",$archivo->contenido);
                
                $mensajes = array_merge($mensajes,$m);
            }
        }

        $transaction = \Yii::$app->db->beginTransaction(); //El crear involucra varias tablas
        $error = null;
        
        try {//comienzo vaciando la lista de personas y mensajes existente
           foreach ($model->proyectosMensajes as $pm){
               $pm->delete();
           }
           foreach ($model->proyectosPersonas as $pp){
               $pp->delete();
           }
            
//           $persona = new ProyectosPersonas();
//           $persona->proyecto = $model->id;
//           $persona->persona = "Leandro";
//           $persona->save(); //creo una sola persona por ahora
//           $idPersona = $persona->id;
//           $fechaHora = date("Y-m-d H:i:s");
           $listafrm = [];
           $postCabecera = false; //es para saber si estoy en las primeras líneas que solo contienen instrucciones sobre la inicialización del grupo
           foreach($mensajes as $m){
//               $mensaje = new ProyectosMensajes();
//               $mensaje->proyecto = $model->id;
//               $mensaje->fecha = $fechaHora;
//               $mensaje->persona = $idPersona;
//               $mensaje->texto = $m;
//               $mensaje->save();
           
               $frm = MensajesHelpers::fechaRemitenteMensaje($m);
               if(isset($frm['fecha'])){
                    $listafrm[]=$frm;
                    $postCabecera = true;
                    
               }else{ //si es una línea sin fecha la anexo al último mensaje
                    if(count($frm)>0){
                         if(count($listafrm)>0 && $postCabecera){
                             $listafrm[count($listafrm)-1]['mensaje'].="\n".$m;
                         }
                    }
               }
           }
           
           $listapersonas = ArrayHelper::index($listafrm, 'remitente'); //genero un array con las personas
           
           foreach($listapersonas as $nombrePersona => $datosPersona ){
                $persona = new ProyectosPersonas();
                $persona->proyecto = $model->id;
                $persona->persona = $nombrePersona;
                $persona->save(); //creo una sola persona por ahora
                $listapersonas[$nombrePersona]['id_persona']=$persona->id;
           }
           
           foreach($listafrm as $frm){
               $frm['id_persona']=$listapersonas[$frm['remitente']]['id_persona'];
               $mensaje = new ProyectosMensajes();
               $mensaje->proyecto = $model->id;
               $mensaje->fecha = $frm['fecha'];
               $mensaje->persona = $frm['id_persona'];
               
               $mensajestr = $frm['mensaje'];
               $mensaje->texto_original = $frm['mensaje'];
               $retornar = $this->emojis_adjuntos($mensajestr);
               if(isset($retornar['emojis']) || isset($retornar['adjuntos']) ){
                    $mensaje->texto = $retornar['texto'];
                    $mensaje->adjuntos = $retornar['adjuntos'];
                    $mensaje->emojis = $retornar['emojis'];
               }else{
                    $mensaje->texto = $frm['mensaje'];
               }
               
               $mensaje->save();               
           }
           
           if (!$model->save()){//guardo el autor del procesado
               throw new Exception(VarDumper::dumpAsString($model->errors));
           }
        } catch (Exception $e) {
            Yii::error(VarDumper::dumpAsString($e->getMessage()));
            $error = "Error durante el vaciado de los mensajes anteriores";
        }            

        if($error == null){
            $transaction->commit();
            //return $this->redirect(['view', 'id' => $model->id]);
        }else{
            $transaction->rollback();
           throw new ServerErrorHttpException('Error al guardar los datos');
        }            
        
        return $this->render('procesar', [
            'model' => $model,
            'texto' => $texto,
            'mensajes' => $mensajes,
            'listafrm' => $listafrm,
            'listapersonas' => $listapersonas,
        ]);
    }

    protected function emojis_adjuntos($string){
        $retornar = [
            'emojis' => null,
            'adjuntos' => null,
            'texto' => null,
        ];
        $emojis = [];
        $arrayAdjuntos = [];
        $adjuntos = [];
        $adjuntos1 = [];
        //$string = "😃 hello world 🙃";
        $replacement = "";
        $pattern = '/([0-9#][\x{20E3}])|[\x{00ae}\x{00a9}\x{203C}\x{2047}\x{2048}\x{2049}\x{3030}\x{303D}\x{2139}\x{2122}\x{3297}\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u';
        $patternAdjuntos = '/([a-zA-Z0-9-]+\.[a-zA-Z-0-9]{3,4} \(archivo adjunto\))/i';
        $patternAdjuntos1 = '/(<Multimedia omitido>)/i';
        $patternAdjuntos2 = '/(Este mensaje fue eliminado)/i';
        //$url_pattern = '/((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z0-9\&\.\/\?\:@\-_=#])*/'; //falla en algunos casos
        // https://stackoverflow.com/questions/6427530/regular-expression-pattern-to-match-url-with-or-without-http-www
        $url_pattern = '/(ftp|https?):\/\/(\w+:?\w*@)?(\S+)(:[0-9]+)?(\/([\w#!:.?+=&%@!\/-])?)?/';
        $email_pattern = '/([\w\.\-]+)@([\w\-]+)((\.(\w){2,63}){1,3})/i';
        preg_match_all($pattern, $string, $emojis);
        preg_match_all($patternAdjuntos, $string, $adjuntos);
        preg_match_all($patternAdjuntos1, $string, $adjuntos1);
        preg_match_all($patternAdjuntos2, $string, $adjuntos2);
        preg_match_all($url_pattern, $string, $adjuntos3);
        preg_match_all($email_pattern, $string, $adjuntos4);

        
        //echo "string: $string <br/>";
        
        $string = preg_replace($pattern, $replacement, $string);
        $string = preg_replace($patternAdjuntos, $replacement, $string);
        $string = preg_replace($patternAdjuntos1, $replacement, $string);
        $string = preg_replace($patternAdjuntos2, $replacement, $string);
        $string = preg_replace($url_pattern, $replacement, $string);
        $string = preg_replace($email_pattern, $replacement, $string);
        if(count($adjuntos)>0 || count($adjuntos1)>0){
            $arrayAdjuntos = array_merge($adjuntos[0],$adjuntos1[0],$adjuntos2[0],$adjuntos3[0],$adjuntos4[0]);
        }
        if(count($emojis)>0 || count($arrayAdjuntos)>0){
            $retornar['emojis'] = implode("\n",$emojis[0]);
            $retornar['adjuntos'] = implode("\n",$arrayAdjuntos);
            $retornar['texto'] = $string;
        }    
        //print_r($emojis[0]); // Array ( [0] => 😃 [1] => 🙃 ) 
        //echo "string: $string <br/>";        
        return $retornar;
    }
    

    
    /**
     * Updates an existing Proyectos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
               throw new ServerErrorHttpException('Error al guardar los datos');
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Proyectos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Proyectos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proyectos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
      $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
      $userId = \Yii::$app->user->identity->id;

      if (($model = Proyectos::findOne($id)) !== null) {
          if ($esAdmin){
              return $model;
          }else{
              if ($model->created_by==$userId){
                  return $model;
              }
              else {
                  throw new NotFoundHttpException('No se puede acceder al elemento.');
              }
          }
      } else {
          throw new NotFoundHttpException('No se puede acceder al elemento.');
      }
    }
}
