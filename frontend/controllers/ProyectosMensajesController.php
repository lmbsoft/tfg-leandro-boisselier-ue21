<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ProyectosMensajes;
use frontend\models\search\ProyectosMensajesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\PermisosHelpers;
use yii\helpers\VarDumper;
/**
 * ProyectosMensajesController implements the CRUD actions for ProyectosMensajes model.
 */
class ProyectosMensajesController extends Controller
{
  public function behaviors()
  {
     return [
      'access' => [
             'class' => \yii\filters\AccessControl::className(),
             'only' => ['index', 'view','create', 'update', 'delete'],
             'rules' => [
                 [
                     'actions' => ['index', 'view',],
                     'allow' => true,
                     'roles' => ['@'],
                     'matchCallback' => function ($rule, $action) {
                      return PermisosHelpers::requerirMinimoRol('Usuario')
                      && PermisosHelpers::requerirEstado('Activo');
                     }
                 ],
                  [
                     'actions' => [ 'create', 'update', 'delete'],
                     'allow' => true,
                     'roles' => ['@'],
                     'matchCallback' => function ($rule, $action) {
                      return PermisosHelpers::requerirMinimoRol('Usuario')
                      && PermisosHelpers::requerirEstado('Activo');
                     }
                 ],
             ],
         ],

        'verbs' => [
                  'class' => VerbFilter::className(),
                  'actions' => [
                      'delete' => ['post'],
                  ],
              ],
          ];
  }



    /**
     * Lists all ProyectosMensajes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProyectosMensajesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $idProyecto = null;
        if(isset($_GET['proyecto'])){
            $idProyecto=$_GET['proyecto'];
        }           

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'idProyecto' => $idProyecto,
        ]);
    }

    /**
     * Displays a single ProyectosMensajes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProyectosMensajes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProyectosMensajes();
        if(isset($_GET['proyecto'])){
            $model->proyecto=$_GET['proyecto'];
        }
        
        if ($model->load(Yii::$app->request->post())) {
            
            if($model->save()){
                return $this->redirect(['proyectos/view', 'id' => $model->proyecto]);
//                return $this->redirect(['view', 'id' => $model->id]);
            }else{
//               Yii::error(VarDumper::dumpAsString($model->getErrors()));
//               Yii::error(VarDumper::dumpAsString($model));
               throw new ServerErrorHttpException('Error al guardar los datos');
               
            }
        } else {
            $model->fecha = date('Y-m-d H:m:s');
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProyectosMensajes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                return $this->redirect(['proyectos/view', 'id' => $model->proyecto]);
            }else{
               throw new ServerErrorHttpException('Error al guardar los datos');
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProyectosMensajes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $proyecto = $this->findModel($id)->proyecto;

        $this->findModel($id)->delete();

        return $this->redirect(['proyectos/view', 'id' => $proyecto]);
    }

    /**
     * Finds the ProyectosMensajes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProyectosMensajes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
      $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
      $userId = \Yii::$app->user->identity->id;

      if (($model = ProyectosMensajes::findOne($id)) !== null) {
          if ($esAdmin){
              return $model;
          }else{
              if ($model->created_by==$userId){
                  return $model;
              }
              else {
                  throw new NotFoundHttpException('No se puede acceder al elemento.');
              }
          }
      } else {
          throw new NotFoundHttpException('No se puede acceder al elemento.');
      }
    }
}
