<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ProyectosArchivos;
use frontend\models\search\ProyectosArchivosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\PermisosHelpers;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

use frontend\models\AnonimizadorForm;
use frontend\models\Proyectos;
use common\models\MensajesHelpers;
use yii\helpers\ArrayHelper;

/**
 * ProyectosArchivosController implements the CRUD actions for ProyectosArchivos model.
 */
class ProyectosArchivosController extends Controller
{
  public function behaviors()
  {
     return [
      'access' => [
             'class' => \yii\filters\AccessControl::className(),
             'only' => ['index', 'view', 'create', 'update', 'delete', 'leer-archivo', 'anonimizar'],
             'rules' => [
                 [
                     'actions' => ['index', 'view',],
                     'allow' => true,
                     'roles' => ['@'],
                     'matchCallback' => function ($rule, $action) {
                      return PermisosHelpers::requerirMinimoRol('Usuario')
                      && PermisosHelpers::requerirEstado('Activo');
                     }
                 ],
                  [
                     'actions' => [ 'create', 'update', 'delete','leer-archivo', 'anonimizar'],
                     'allow' => true,
                     'roles' => ['@'],
                     'matchCallback' => function ($rule, $action) {
                      return PermisosHelpers::requerirMinimoRol('Usuario')
                      && PermisosHelpers::requerirEstado('Activo');
                     }
                 ],
             ],
         ],

        'verbs' => [
                  'class' => VerbFilter::className(),
                  'actions' => [
                      'delete' => ['post','leer-archivo'],
                  ],
              ],
          ];
  }

      /**
     * Herramientas para anonimizar el cuerpo de texto del contenido
     * @param integer $idArchivo
     * @return mixed
     */
    public function actionAnonimizar($idArchivo)
    {
        $anonimizar = new AnonimizadorForm();
        
        $model = $this->findModel($idArchivo);
        
        if ($anonimizar->load(Yii::$app->request->post())) {
            $patron=[];
            $reemplazo=[];
            $contenido = $model->contenido;
            
            if(isset($anonimizar->palabras)){
                $lineasPalabras = mb_split("\n",$anonimizar->palabras);
                foreach($lineasPalabras as $lp){//armar un arreglo de reemplazos con las palabras que tienen un reemplazo
                    $palabra_reemplazo = mb_split("<->",$lp,2); //(cantidad) palabra <-> reemplazo
                    if(count($palabra_reemplazo)==2){
                        $izquierda = trim($palabra_reemplazo[0]);
                        $izquierdaArray = mb_split("\s",$izquierda,2); //(cantidad) palabra
                        if(count($izquierdaArray)==2){
                            $izquierda = trim($izquierdaArray[1]);
                        }
                        $derecha = trim($palabra_reemplazo[1]);
                        if($izquierda!==$derecha){//solo agrego los que tienen cambio entre palabra y reemplazo
                            $patron[]="/\b".strtolower($izquierda)."\b/i"; //ver si es comilla simple o doble 
                            $reemplazo[]=$derecha; //reemplazo por vacio para eliminar los stopwords
                        }
                    }
                }
                
                $personas = [];
                if(isset($anonimizar->personas)){
                    $lineasPersonas = mb_split("\n",$anonimizar->personas);
                    foreach($lineasPersonas as $pr){//armar un arreglo de reemplazos con las palabras que tienen un reemplazo
                        $persona_reemplazo = mb_split("<->",$pr,2); //(cantidad) palabra <-> reemplazo
                        if(count($persona_reemplazo)==2){
                            $izquierda = trim($persona_reemplazo[0]);
                            $derecha = trim($persona_reemplazo[1]);
                            $personas[$izquierda]=$derecha; //es un array de $persona[busca]=>reemplazo
                        }
                    }
                }
                
                $contenido = "";
                $lineasPalabras = mb_split("\n",$model->contenido);
                foreach ($lineasPalabras as $lp){
                    $frm = MensajesHelpers::fechaRemitenteMensaje($lp);
                    if(isset($frm['fecha'])){
                         $fecha = \DateTime::createFromFormat("!Y-m-d H:i:s", $frm['fecha']); //tengo que volver a dar vuelta la fecha
                         $remitente = $frm['remitente']; //aquí podría reemplazar los remitentes
                         $remitente = ArrayHelper::getValue($personas, $remitente, $remitente);//busca si hay reemplazo de la persona, si no deja por defecto
                         
                         $mensaje = preg_replace($patron, $reemplazo, $frm['mensaje'])."\n";
                         
                         $contenido.= $fecha->format('j/n/y H:i') . " - " . $remitente . ": ".$mensaje;
                         
                    }else{ //si es una línea sin fecha utilizo solo la línea
                        $contenido .= preg_replace($patron, $reemplazo, $lp)."\n";
                    }                    
                }
                
            }
            $model->contenido = $contenido;
            if($model->save()){
                //return $this->redirect(['proyectos-archivos/view', 'id' => $idArchivo]);
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
               throw new ServerErrorHttpException('Error al guardar los datos');
            }
        } else { //preparar el formulario de reemplazos
            $anonimizar->idArchivo = $model->id;
            $anonimizar->personas = "Personas";
            $stopwords ="";
            if($model->proyecto0->stopwords0->stopwords){
                $stopwords = $model->proyecto0->stopwords0->stopwords;
            }
            if($model->proyecto0->stopwords_lista){
               $stopwords .= $model->proyecto0->stopwords_lista; 
            }

            $stopwordsArray = mb_split("\r\n",$stopwords);
            
            $texto = "";
            $mensajes = [];     
            $mensajes = mb_split("\n",$model->contenido);
            $personas = [];
            
            foreach($mensajes as $m){ //aquí podría luego armar el reemplazo de las personas
                 $frm = MensajesHelpers::fechaRemitenteMensaje($m);
                 if(isset($frm['fecha'])){
                    $personas[$frm['remitente']]= $frm['remitente'];
                 }
                 $texto .= $frm['mensaje']."\n";
            }
            
            $anonimizar->personas = "";
            $nroPersona=1;
            $nroProyecto=$model->id; //no conviene usar el proyecto porque se pueden generar repetidos
            foreach($personas as $persona=>$p){
                if($persona){
                    $anonimizar->personas.="${persona}<->Persona${nroProyecto}_${nroPersona}\n";
                    $nroPersona++;
                }
            }
            //$texto = strtolower($texto); //lo dejo comentado porque tengo poroblemas con algunas palabras luego: contraseÑa
            
            $ocurrencias = array_count_values(str_word_count($texto,2,'áéíóúäëïöüñÑàèìòùî'));

            arsort($ocurrencias);

            $palabras = "";
            foreach($ocurrencias as $palabra => $cant){
                if(!ArrayHelper::isIn($palabra, $stopwordsArray)){
                    $palabras .= "($cant) $palabra <-> $palabra\n";
                }
            }
            $anonimizar->palabras = $palabras;

            $tableArray = [];
            foreach($ocurrencias as $palabra => $cantidad){
                $tableArray[]=["palabra"=>$palabra, "cantidad"=>$cantidad];
            }
            
            return $this->render('anonimizar', [
                'anonimizar' => $anonimizar, 
                'ocurrencias' => $ocurrencias,
                'texto' => $texto,
            ]);
        }        

    }
    
    
    

    /**
     * Lee el contenido del archivo y lo incorpora en el texto
     * @param integer $id
     * @return mixed
     */
    public function actionLeerArchivo($id)
    {
        $model = $this->findModel($id);
        try {
            $model->contenido = $model->getContenidoAdjunto();
            if($model->save()){
                Yii::$app->session->setFlash('success', 'Contenido leído');
                //return $this->redirect(['proyectos/view', 'id' => $model->proyecto]);
                return $this->redirect(['view', 'id' => $model->id]);
            }
         } catch (Exception $e) {
             Yii::error(VarDumper::dumpAsString($e->getMessage()));
             Yii::$app->session->setFlash('error', 'Ocurrió un error leyendo el archivo');
         }            

        return $this->render('leer-archivo', [
            'model' => $model, 
        ]);
    }

    /**
     * Lists all ProyectosArchivos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProyectosArchivosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProyectosArchivos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProyectosArchivos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProyectosArchivos();
        if(isset($_GET['proyecto'])){
            $model->proyecto=$_GET['proyecto'];
        }               

        if ($model->load(Yii::$app->request->post())) {
            $model->archivoadjunto = UploadedFile::getInstance($model,'archivoadjunto');
            
            if($model->save()){
                //return $this->redirect(['proyectos/view', 'id' => $model->proyecto]);
                return $this->redirect(['view', 'id' => $model->id]);
//                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                
//               Yii::error(VarDumper::dumpAsString($model->getErrors()));
//               Yii::error(VarDumper::dumpAsString($model));
               throw new ServerErrorHttpException('Error al guardar los datos');
               
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProyectosArchivos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->archivoadjunto = UploadedFile::getInstance($model,'archivoadjunto');     
            if($model->save()){
                //return $this->redirect(['proyectos/view', 'id' => $model->proyecto]);
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
               throw new ServerErrorHttpException('Error al guardar los datos');
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProyectosArchivos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $proyecto = $this->findModel($id)->proyecto;

        $this->findModel($id)->delete();

        return $this->redirect(['proyectos/view', 'id' => $proyecto]);
    }

    /**
     * Finds the ProyectosArchivos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProyectosArchivos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
      $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
      $userId = \Yii::$app->user->identity->id;

      if (($model = ProyectosArchivos::findOne($id)) !== null) {
          if ($esAdmin){
              return $model;
          }else{
              if ($model->created_by==$userId){
                  return $model;
              }
              else {
                  throw new NotFoundHttpException('No se puede acceder al elemento.');
              }
          }
      } else {
          throw new NotFoundHttpException('No se puede acceder al elemento.');
      }
    }
}
