<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Textos;
use frontend\models\search\TextosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\data\ArrayDataProvider;

use common\models\PermisosHelpers;

use frontend\models\Diff;
use frontend\models\DiffForm;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * TextosController implements the CRUD actions for Textos model.
 */
class TextosController extends Controller
{
  public function behaviors()
  {
     return [
      'access' => [
             'class' => \yii\filters\AccessControl::className(),
             'only' => ['index', 'view','create', 'update', 'delete',
                 'contador','sentimientos','ubicaciones','diferencias','mapas','mapa-kml','stanza','textbasics','sumario'],
             'rules' => [
                 [
                     'actions' => ['index', 'view','contador',
                         'sentimientos','ubicaciones','diferencias','mapas','mapa-kml','stanza','textbasics','sumario'],
                     'allow' => true,
                     'roles' => ['@'],
                     'matchCallback' => function ($rule, $action) {
                      return PermisosHelpers::requerirMinimoRol('Usuario')
                      && PermisosHelpers::requerirEstado('Activo');
                     }
                 ],
                  [
                     'actions' => [ 'create', 'update', 'delete'],
                     'allow' => true,
                     'roles' => ['@'],
                     'matchCallback' => function ($rule, $action) {
                      return PermisosHelpers::requerirMinimoRol('Usuario')
                      && PermisosHelpers::requerirEstado('Activo');
                     }
                 ],
             ],
         ],

        'verbs' => [
                  'class' => VerbFilter::className(),
                  'actions' => [
                      'delete' => ['post'],
                  ],
              ],
          ];
  }

    
    public function actionDiferencias(){
        $model = new DiffForm();
        $texto1 = 'Texto 1';
        $texto2 = 'Texto 2';
        if($model->load(Yii::$app->request->post())){
            $texto1 = $model->texto1;
            $texto2 = $model->texto2;
        }
        $diferencias = Diff::compare($texto1, $texto2);
        $diferencias2 = Diff::compare($texto1, $texto2,true);
        $textoDiffTxt = Diff::toString($diferencias);
        $textoDiffHtml = Diff::toHtml($diferencias);
        $textoDiffTable = Diff::toTable($diferencias);
        $textoDiffTxt2 = Diff::toString($diferencias2);
        $textoDiffHtml2 = Diff::toHtml($diferencias2);
        $textoDiffTable2 = Diff::toTable($diferencias2);
        return $this->render('diferencias', [
                   'texto1' => $texto1,
                   'texto2' => $texto2,
                   'diferencias' => $diferencias,
                   'diferencias2' => $diferencias2,
                   'textoDiffTxt' => $textoDiffTxt,
                   'textoDiffHtml' => $textoDiffHtml,
                   'textoDiffTable' => $textoDiffTable,
                   'textoDiffTxt2' => $textoDiffTxt2,
                   'textoDiffHtml2' => $textoDiffHtml2,
                   'textoDiffTable2' => $textoDiffTable2,
                   'model' => $model,
               ]);        
    }

    public function actionContador($id){
        /*Contador sobre todo el texto, ignorando los comentarios entre <> pero si aplicando <ignorar></ignorar>
         * - elimina stopwords y reemplaza con stemmers
         * - genera ranking inverso
         */
        $texto = $this->findModel($id);
        
        //primero los stopwords
        $stopwords = "";
        if($texto->stopwords0){
            $stopwords = $texto->stopwords0->stopwords;
        }
        if($texto->stopwords_lista){
           $stopwords .= "\r\n".$texto->stopwords_lista; 
        }
        
        //$reemplazos = preg_split('/\n/',$stopwords);
        $reemplazos = preg_split('/\r\n/',$stopwords);
        
        
        
        //$reemplazos= Reemplazos::find()->all();
        $patron=[];
        $reemplazo=[];
        foreach ($reemplazos as $reemp){
            //$patron[]='/\b'.$reemp.'\b/i';
            //$patron[]='/\b'.trim($reemp).'\b/i';
            if(strlen(trim($reemp))>0){
                $patron[]="/\b".strtolower($reemp)."\b/i"; //ver si es comilla simple o doble
                //$reemplazo[]=$reemp->reemplazo;
                $reemplazo[]=" "; //reemplazo por vacio para eliminar los stopwords
            }
        }
        
        //luego los stemmers
        $stemmer = "";
        if($texto->stemmer0){
            $stemmer = $texto->stemmer0->palabras;
        }
        if($texto->stemmer_lista){
            $stemmer .= "\r\n".$texto->stemmer_lista;
        }
        
        $lineasStemmer = preg_split('/\r\n/',$stemmer);
        foreach ($lineasStemmer as $lineaStemmer){
            $arrayLineaStemmer = preg_split('/=/',$lineaStemmer);
            if(count($arrayLineaStemmer)==2){
                $patron[]="/\b".strtolower($arrayLineaStemmer[1])."\b/i";
                //$patron[]='/\b'.$arrayLineaStemmer[1].'\/b/i';
                $reemplazo[]=strtolower($arrayLineaStemmer[0]);
            }
        }
        
        //partir la lista de stemmers en reemplazo = valor
        //$cadena='';
        
        //$lineas = preg_split('/\n/',$texto->texto); //Mensajes::find()->all();
        
        //quitar todo lo que esté entre <ignorar></ignorar>

        /*
        $textoSin = preg_replace('/<*>/','-',$texto->texto);
        $lineas = preg_split('/\r\n/',$textoSin); //Mensajes::find()->all();
        foreach($lineas as $linea){
            $cadena.=" ".$linea;
        }
        */
        $cadena = $texto->texto;
        
        //quitar todo lo que esté dentro de <ignorar> </ignorar>
        
        //$cadena = preg_replace('/<ignorar>*<\/ignorar>/i',' ',$cadena);
        
        $lineas = preg_split('/\r\n/',$cadena);
        $ignorar = false;
        $cadenaIgnore = '';
        foreach ($lineas as $linea){
            $linea = trim($linea);
            if ($linea=='<ignorar>'){
                $ignorar = true;
            }
            else if($linea == '</ignorar>'){
                $ignorar = false;
            }else{
                if($ignorar == false){
                    //descartar todo lo que esté entre <>
                    $token=substr($linea,0,8); //con los 8 primeros caracteres podría determinar el token
                    if($token=='<seccion' || $token=='</seccio'){
                       //inicializar seccion    
                    }else if ($token=='<subsecc' || $token=='</subsec'){
                       //inicializar subseccion
                    }else if ($token=='<persona'){ //ignorar lo que sea de personajes
                       //inicializar subseccion
                    }else{
                       //en la seccion ó subsección correspondiente poner la línea
                       $cadenaIgnore .= "\r\n".trim($linea);
                    }
                }
            }
        }
        
        $cadenaIgnore = strtolower($cadenaIgnore);
        
        $cadenaReemplazada = preg_replace($patron, $reemplazo, $cadenaIgnore);
        
        

        //tengo que transformar los caracteres especiales para el array_count_values
        /*
        $patron=[];
        $reemplazo=[];
        $patron[]='/á/'; $reemplazo[]='a';
        $patron[]='/é/'; $reemplazo[]='e';
        $patron[]='/í/'; $reemplazo[]='i';
        $patron[]='/ó/'; $reemplazo[]='o';
        $patron[]='/ú/'; $reemplazo[]='u';
        $patron[]='/ä/'; $reemplazo[]='a';
        $patron[]='/ë/'; $reemplazo[]='e';
        $patron[]='/ï/'; $reemplazo[]='i';
        $patron[]='/ö/'; $reemplazo[]='o';
        $patron[]='/ü/'; $reemplazo[]='u';
        $patron[]='/ñ/'; $reemplazo[]='n';
        $patron[]='/Ñ/'; $reemplazo[]='Ñ';
        $patron[]='/\s\s+/'; $reemplazo[]=' ';
        */
        //$cadenaReemplazada = preg_replace($patron, $reemplazo, $cadenaReemplazada);        

        $cadenaReemplazada = preg_replace('/\s\s+/', ' ', $cadenaReemplazada); //elimino espacios
        
        //$ocurrencias= array_count_values(str_word_count($cadenaReemplazada,1));
        $ocurrencias = array_count_values(str_word_count($cadenaReemplazada,2,'áéíóúäëïöüñÑàèìòùî'));
        
        arsort($ocurrencias);

        
        $tableArray = [];
        foreach($ocurrencias as $palabra => $cantidad){
            $tableArray[]=["palabra"=>$palabra, "cantidad"=>$cantidad];
        }
        
        $provider = new ArrayDataProvider([
            'allModels'=> $tableArray,
            'sort'=>['attributes'=>['palabra','cantidad']],
            'key'=>'palabra',
            'pagination'=>false,
        ]);
        
        return $this->render("contador", 
            [
                "model"=>$texto,
                "cadena"=> $cadena,
                "cadenaReemplazada"=> $cadenaReemplazada,
                "ocurrencias" => $ocurrencias,
                "provider" => $provider,
            ]
        );

    }
    
    public function actionSumario($id){
        /*Contador sobre todo el texto, ignorando los comentarios entre <> pero si aplicando <ignorar></ignorar>
         * - elimina stopwords y reemplaza con stemmers
         * - genera ranking inverso
         */
        $texto = $this->findModel($id);
        $lang = "en";
        
        //primero los stopwords
        $stopwords = "";
        if($texto->stopwords0){
            $stopwords = $texto->stopwords0->stopwords;
            //\Yii::warning(VarDumper::dumpAsString("nombre stopword: ".$stopwords->stopwords0->nombre));
            if ($texto->stopwords0->nombre == "Español"){
                $lang = "es";
            }else if ($texto->stopwords0->nombre == "Italiano"){
                $lang = "it";
            }
        }
        
        if($texto->stopwords_lista){
           $stopwords .= "\r\n".$texto->stopwords_lista; 
        }
        
        //$reemplazos = preg_split('/\n/',$stopwords);
        $reemplazos = preg_split('/\r\n/',$stopwords);

        //$reemplazos= Reemplazos::find()->all();
        $patron=[];
        $reemplazo=[];
        foreach ($reemplazos as $reemp){
            //$patron[]='/\b'.$reemp.'\b/i';
            //$patron[]='/\b'.trim($reemp).'\b/i';
            if(strlen(trim($reemp))>0){
                $patron[]="/\b".strtolower($reemp)."\b/i"; //ver si es comilla simple o doble
                //$reemplazo[]=$reemp->reemplazo;
                $reemplazo[]=" "; //reemplazo por vacio para eliminar los stopwords
            }
        }
        
        //luego los stemmers
        $stemmer = "";
        if($texto->stemmer0){
            $stemmer = $texto->stemmer0->palabras;
        }
        if($texto->stemmer_lista){
            $stemmer .= "\r\n".$texto->stemmer_lista;
        }
        
        $lineasStemmer = preg_split('/\r\n/',$stemmer);
        foreach ($lineasStemmer as $lineaStemmer){
            $arrayLineaStemmer = preg_split('/=/',$lineaStemmer);
            if(count($arrayLineaStemmer)==2){
                $patron[]="/\b".strtolower($arrayLineaStemmer[1])."\b/i";
                //$patron[]='/\b'.$arrayLineaStemmer[1].'\/b/i';
                $reemplazo[]=strtolower($arrayLineaStemmer[0]);
            }
        }
        
        $cadena = $texto->texto;
        
        $lineas = preg_split('/\r\n/',$cadena);
        $ignorar = false;
        $cadenaIgnore = '';
        $arrayLineas = [];
        
        $documento = $texto->nombre;
        $parte = "PARTE 0";
        $parrafoNombre = "p";
        $contParrafo = 0;
        $parrafo = "$parrafoNombre $contParrafo";
        foreach ($lineas as $linea){
            $linea = trim($linea);
            if ($linea=='<ignorar>'){
                $ignorar = true;
            }
            else if($linea == '</ignorar>'){
                $ignorar = false;
            }else{
                if($ignorar == false){
                    //descartar todo lo que esté entre <>
                    $token=trim(substr($linea,0,6)); //con los 8 primeros caracteres podría determinar el token
                    if($token=='<PARTE' || $token=='</PART'){
                       if ($token == '<PARTE'){
                           $parte = preg_replace("/[^a-zA-Z0-9\s]/", "", $linea); //eliminar los < >
                       }
                    }else if ($token=='<par>' || $token=='</par>'){
                       if ($token == '<par>'){
                           $contParrafo ++;
                           $parrafo = "$parrafoNombre $contParrafo";
                       }
                    }else if ($token=='<persona'){ //ignorar lo que sea de personajes
                       //inicializar subseccion
                    }else{
                       //en la seccion ó subsección correspondiente poner la línea
                       $cadenaIgnore .= "\r\n".trim($linea);
                       //$arrayLineas[]= trim($linea);
                       //ArrayHelper::getValue($comment, 'user.username', 'Unknown');
                       $arrayLineas[$documento][$parte][$parrafo] = ArrayHelper::getValue($arrayLineas, "$documento.$parte.$parrafo", "") ." ". trim($linea);
                    }
                }
            }
        }
        
        $cadenaIgnore = strtolower($cadenaIgnore);
        
        $cadenaReemplazada = preg_replace($patron, $reemplazo, $cadenaIgnore);
        
        $cadenaReemplazada = preg_replace('/\s\s+/', ' ', $cadenaReemplazada); //elimino espacios
        
        $ocurrencias = array_count_values(str_word_count($cadenaReemplazada,2,'áéíóúäëïöüñÑàèìòùî'));
        
        arsort($ocurrencias);
        
        $cadena;
        $resultado = [];
        $tableArray = [];
        $error = null;
        
        $word_count = 100; //$texto->cantidad_topten?$texto->cantidad_topten:100;
        $word_count_parrafo = 30; // $texto->sp_parrafo?$texto->sp_parrafo:30;
        $word_count_parte = 100; // $texto->sp_parte?$texto->sp_parte:100;
        $word_count_documento = 300; //$texto->sp_documento?$texto->sp_documento:300;

        foreach($arrayLineas as $nombredoc => $doc){
            //$resultado = \Yii::$app->stanza->stanzatest($cadenaReemplazada,$lang);
            //$resultado = \Yii::$app->stanza->stanzatest($al,$lang);
            $docText = "";
            foreach ($doc as $nombreparte => $parte){
                $parteText = "";
                foreach ($parte as $parrafo => $p){
                        try {

                            $resultado = \Yii::$app->nlp->generarResumen($p,$word_count_parrafo); //  stanza->stanzatest($p,$lang);
                            
                            if($resultado){
                                $tableArray[]=[ "doc"=>$nombredoc,
                                                "parte"=>"$nombredoc $nombreparte",
                                                "parrafo"=>"$nombredoc $nombreparte $parrafo",
                                                "sumario"=>$resultado->summarize,
                                                "original"=>$p,
                                        ];
                            }

                        } catch (Exception $e) {
                            //Yii::error(VarDumper::dumpAsString($e->getMessage()));
                            $error = "Error al procesar datos Summarize";
                        }
                        $parteText .= $p."\n";
                }
                try { //summarizo la parte
                    $resultado = \Yii::$app->nlp->generarResumen($parteText,$word_count_parte); //  stanza->stanzatest($p,$lang);
                    if($resultado){
                        $tableArray[]=[ "doc"=>$nombredoc,
                                        "parte"=>"$nombredoc $nombreparte",
                                        "parrafo"=>"$nombredoc $nombreparte",
                                        "sumario"=>$resultado->summarize,
                                        "original"=>$parteText,
                                ];
                    }
                } catch (Exception $e) {
                    //Yii::error(VarDumper::dumpAsString($e->getMessage()));
                    $error = "Error al procesar datos Summarize";
                }
                
                $docText .=$parteText ."\n";
            }
            try { //summarizo el documento
                $resultado = \Yii::$app->nlp->generarResumen($docText,$word_count_documento); //  stanza->stanzatest($p,$lang);
                if($resultado){
                    $tableArray[]=[ "doc"=>$nombredoc,
                                    "parte"=>"$nombredoc",
                                    "parrafo"=>"$nombredoc",
                                    "sumario"=>$resultado->summarize,
                                    "original"=>$docText,
                            ];
                }
            } catch (Exception $e) {
                //Yii::error(VarDumper::dumpAsString($e->getMessage()));
                $error = "Error al procesar datos Summarize";
            }
            
        }
        
//        try {
//            $tableArrayJson = json_encode($tableArray);
//            $texto->stanzatext = $tableArrayJson;
//            $texto->save();
//        } catch (Exception $e) {
//            //Yii::error(VarDumper::dumpAsString($e->getMessage()));
//            $error = "Error al guardar el texto";
//        }            
//            
//        if($error != null){
//           //throw new ServerErrorHttpException($error);
//        }          
        
//        foreach($ocurrencias as $palabra => $cantidad){
//            $tableArray[]=["palabra"=>$palabra, "cantidad"=>$cantidad];
//        }
        

        $provider = new ArrayDataProvider([
            'allModels'=> $tableArray,
            'sort'=>['attributes'=>['doc','parte','parrafo']],
            'key'=>'lemma',
            'pagination'=>false,
        ]);
        
        return $this->render("resumen", 
            [
                "model"=>$texto,
                "cadena"=> $cadena,
                "cadenaReemplazada"=> $cadenaReemplazada,
                "ocurrencias" => $ocurrencias,
                "provider" => $provider,
                "resultado" => $resultado,
                "arrayLineas" => $arrayLineas,
                "error" => $error,
                "tableArray" => $tableArray,
                "lang" => $lang,
            ]
        );

    }      
    
    
    public function actionTextbasics($id){
        $texto = $this->findModel($id);
        $lang = "en";
        $documento = $texto->nombre;
        $parte = "";
        $parrafo = "";
        
        $parrafos_textos = [];
        //primero los stopwords
        $stopwords = "";
        if($texto->stopwords0){
            $stopwords = $texto->stopwords0->stopwords;
            //\Yii::warning(VarDumper::dumpAsString("nombre stopword: ".$stopwords->stopwords0->nombre));
            if ($texto->stopwords0->nombre == "Español"){
                $lang = "es";
            }else if ($texto->stopwords0->nombre == "Italiano"){
                $lang = "it";
            }
        }
        $stopwordsLista=[];
        if($texto->stopwords_lista){
           $stopwords .= "\r\n".$texto->stopwords_lista; 
           $stopwordsLista = preg_split('/\r\n/',$stopwords);
        }
        
        $reemplazos = preg_split('/\r\n/',$stopwords);

        $tableArray = [];
        
        $cadena = $texto->texto;
        $parrafos_textos = preg_split('/\r\n/',$cadena);
        $documento = $texto->nombre;
        
        $topten = 10;  //$texto->cantidad_topten?$texto->cantidad_topten:10;
        $topics = 4;
        
        $textbasics_textos = [];
        
        try {
            $resultado = \Yii::$app->flask->textbasics($parrafos_textos,$lang, $stopwordsLista, $topics); //  stanza->stanzatest($p,$lang);
            if($resultado){
                $textbasics_textos[0] = Json::decode($resultado);
            }
        } catch (Exception $e) {
            $error = "Error al procesar textbasics";
        }
        
        
        return $this->render("textbasics", 
            [
                "model"=>$texto,
                "provider" => $provider,
                "parrafos_textos" => $parrafos_textos,
                'textbasics_textos'=>$textbasics_textos,
                "error" => $error,
                "tableArray" => $tableArray,
                "lang" => $lang,
                "reemplazos" => $reemplazos,
                "topten" => $topten,
            ]
        );
    }      
  

    
    
    
  
    public function actionStanza($id){
        /*Contador sobre todo el texto, ignorando los comentarios entre <> pero si aplicando <ignorar></ignorar>
         * - elimina stopwords y reemplaza con stemmers
         * - genera ranking inverso
         */
        $texto = $this->findModel($id);
        $lang = "en";
        
        //primero los stopwords
        $stopwords = "";
        if($texto->stopwords0){
            $stopwords = $texto->stopwords0->stopwords;
            //\Yii::warning(VarDumper::dumpAsString("nombre stopword: ".$stopwords->stopwords0->nombre));
            if ($texto->stopwords0->nombre == "Español"){
                $lang = "es";
            }else if ($texto->stopwords0->nombre == "Italiano"){
                $lang = "it";
            }
        }
        
        if($texto->stopwords_lista){
           $stopwords .= "\r\n".$texto->stopwords_lista; 
        }
        
        //$reemplazos = preg_split('/\n/',$stopwords);
        $reemplazos = preg_split('/\r\n/',$stopwords);

        //$reemplazos= Reemplazos::find()->all();
        $patron=[];
        $reemplazo=[];
        foreach ($reemplazos as $reemp){
            //$patron[]='/\b'.$reemp.'\b/i';
            //$patron[]='/\b'.trim($reemp).'\b/i';
            if(strlen(trim($reemp))>0){
                $patron[]="/\b".strtolower($reemp)."\b/i"; //ver si es comilla simple o doble
                //$reemplazo[]=$reemp->reemplazo;
                $reemplazo[]=" "; //reemplazo por vacio para eliminar los stopwords
            }
        }
        
        //luego los stemmers
        $stemmer = "";
        if($texto->stemmer0){
            $stemmer = $texto->stemmer0->palabras;
        }
        if($texto->stemmer_lista){
            $stemmer .= "\r\n".$texto->stemmer_lista;
        }
        
        $lineasStemmer = preg_split('/\r\n/',$stemmer);
        foreach ($lineasStemmer as $lineaStemmer){
            $arrayLineaStemmer = preg_split('/=/',$lineaStemmer);
            if(count($arrayLineaStemmer)==2){
                $patron[]="/\b".strtolower($arrayLineaStemmer[1])."\b/i";
                //$patron[]='/\b'.$arrayLineaStemmer[1].'\/b/i';
                $reemplazo[]=strtolower($arrayLineaStemmer[0]);
            }
        }
        
        $cadena = $texto->texto;
        
        $lineas = preg_split('/\r\n/',$cadena);
        $ignorar = false;
        $cadenaIgnore = '';
        foreach ($lineas as $linea){
            $linea = trim($linea);
            if ($linea=='<ignorar>'){
                $ignorar = true;
            }
            else if($linea == '</ignorar>'){
                $ignorar = false;
            }else{
                if($ignorar == false){
                    //descartar todo lo que esté entre <>
                    $token=substr($linea,0,8); //con los 8 primeros caracteres podría determinar el token
                    if($token=='<seccion' || $token=='</seccio'){
                       //inicializar seccion    
                    }else if ($token=='<subsecc' || $token=='</subsec'){
                       //inicializar subseccion
                    }else if ($token=='<persona'){ //ignorar lo que sea de personajes
                       //inicializar subseccion
                    }else{
                       //en la seccion ó subsección correspondiente poner la línea
                       $cadenaIgnore .= "\r\n".trim($linea);
                    }
                }
            }
        }
        
        $cadenaIgnore = strtolower($cadenaIgnore);
        
        $cadenaReemplazada = preg_replace($patron, $reemplazo, $cadenaIgnore);
        
        $cadenaReemplazada = preg_replace('/\s\s+/', ' ', $cadenaReemplazada); //elimino espacios
        
        $ocurrencias = array_count_values(str_word_count($cadenaReemplazada,2,'áéíóúäëïöüñÑàèìòùî'));
        
        arsort($ocurrencias);
        
        $cadena;
        
        try {
            $resultado = \Yii::$app->stanza->stanzatest($cadena,$lang);
        } catch (Exception $e) {
            Yii::error(VarDumper::dumpAsString($e->getMessage()));
            $error = "Error al procesar datos STANZA";
        }            

        if($error != null){
           throw new ServerErrorHttpException($error);
        }          
        
        $tableArray = [];
//        foreach($ocurrencias as $palabra => $cantidad){
//            $tableArray[]=["palabra"=>$palabra, "cantidad"=>$cantidad];
//        }
        
        
        foreach($resultado as $x){
            foreach($x as $r){
                if($r->upos!=null){
                    $tableArray[]=["lemma"=>$r->lemma, "text"=>$r->text,"upos"=>$r->upos];
                }
            }
        }

        $provider = new ArrayDataProvider([
            'allModels'=> $tableArray,
            'sort'=>['attributes'=>['lemma','text','upos']],
            'key'=>'lemma',
            'pagination'=>false,
        ]);
        
        return $this->render("stanza", 
            [
                "model"=>$texto,
                "cadena"=> $cadena,
                "cadenaReemplazada"=> $cadenaReemplazada,
                "ocurrencias" => $ocurrencias,
                "provider" => $provider,
                "resultado" => $resultado,
                "lang" => $lang,
            ]
        );

    }      
  
    private $ultimoNombre ='indefinido';
    private $ultimaLat =0;
    private $ultimaLng =0;
    private $ultimaAlt =0;
    private $ultimoMomento =0;
    private $lugares_lista = [];
    
    private function extraerSentimientos($paramstr){
        $sentimiento = [
                        'nombre'=>$this->ultimoNombre,
                        'sentimientos'=>[],
                        'ubicacion'=>[ //si no se pueden extraer parámetros de posición utiliza los últimos parámetros conocidos
                            'lat'=>$this->ultimaLat,
                            'lng'=>$this->ultimaLng,
                            'alt'=>$this->ultimaAlt
                            ],
                        'momento'=>-1 //si al final sigue en -1 le asigno 1 más de ultimoMomento y lo incremento
                ];
        $parametros = preg_split('/,/',$paramstr);
        foreach($parametros as $parametro){
            $clavevalor = preg_split('/=/',$parametro);
            if(count($clavevalor)==2){
                $clave=trim($clavevalor[0]);
                $valor=trim($clavevalor[1]);
                
                if($clave=='nombre'){
                    $sentimiento['nombre']=$valor;
                    $this->ultimoNombre = $valor; //lo guardo por si el próximo no tiene nombre
                }elseif($clave=='sentimiento'){
                    $valor=preg_replace('/\)/','',$valor); //elimino el ) al final
                    $nombrevalor=preg_split('/\(/',$valor);
                    if(count($nombrevalor)==2){
                        $n=trim($nombrevalor[0]);
                        $n=$sentimiento['nombre']."_".$n; //concatenar el nombre al sentimiento por si se juntan personajes
                        $v=intval(trim($nombrevalor[1]));
                        $sentimiento['sentimientos'][$n]=$v;
                    } //no hago nada porque se supone que todos van con parámetro de intensidad
                }elseif($clave=='momento'){
                    $sentimiento['momento']=strval($valor);
                    $this->ultimoMomento = $sentimiento['momento'];
                }elseif($clave=='lugar'){ //supuestamente si usa lugar no debería poner latitud y longitud, peero
                    if(array_key_exists($valor,$this->lugares_lista)){
                        $sentimiento['ubicacion']['lat']=$this->lugares_lista[$valor]['lat'];
                        $this->ultimaLat = $sentimiento['ubicacion']['lat'];
                        $sentimiento['ubicacion']['lng']=$this->lugares_lista[$valor]['lng'];
                        $this->ultimaLng = $sentimiento['ubicacion']['lng'];
                        $sentimiento['ubicacion']['alt']=$this->lugares_lista[$valor]['alt'];
                        $this->ultimaAlt = $sentimiento['ubicacion']['alt'];
                    }//si no existe el nombre de lugar ya tenía la útltima ubicación por defecto
                    
                }elseif($clave=='lat'){
                        $sentimiento['ubicacion']['lat']=$valor;
                        $this->ultimaLat = $sentimiento['ubicacion']['lat'];
                }elseif($clave=='lng'){
                        $sentimiento['ubicacion']['lng']=$valor;
                        $this->ultimaLng = $sentimiento['ubicacion']['lng'];
                }elseif($clave=='alt'){
                        $sentimiento['ubicacion']['alt']=$valor;
                        $this->ultimaAlt = $sentimiento['ubicacion']['alt'];
                }else{ //no es un parámetro reconocido, lo cargo a la lista nomás
                    $sentimiento[$clave]=$valor;
                }
            }
        }
        if($sentimiento['momento']==-1){
            $this->ultimoMomento++;
            $sentimiento['momento']=$this->ultimoMomento;
            
        }
        return $sentimiento;
    }
    
    
    /**
     * Lists all Textos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TextosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Textos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Textos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Textos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Textos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Textos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Textos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Textos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {        
        $esAdmin = PermisosHelpers::requerirMinimoRol('Admin');
        $userId = \Yii::$app->user->identity->id;

        if (($model = Textos::findOne($id)) !== null) {
            if ($esAdmin){
                return $model;
            }else{
                if ($model->created_by==$userId){
                    return $model;
                }
                else {
                    throw new NotFoundHttpException('No se puede acceder al elemento.');
                }
            }
        } else {
            throw new NotFoundHttpException('No se puede acceder al elemento.');
        }
        
    }
}
