<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'modules' => [
        'social' => [
            // la clase del módulo
            'class' => 'kartik\social\Module',
 
            // la configuración global para el widget disqus
            'disqus' => [
                'settings' => ['shortname' => 'DISQUS_SHORTNAME'] // conf por defecto
            ],
 
            // la configuración global para el widget de los plugins de facebook
            'facebook' => [
                'appId' => '1613652862251614',
                'secret' => '10477999737238a8fcf1ad8add6fefd6',
        ],
 
        // la configuración social para el widget del plugin social de google
        'google' => [
    'clientId' => 'GOOGLE_API_CLIENT_ID',
    'pageId' => 'GOOGLE_PLUS_PAGE_ID',
    'profileId' => 'GOOGLE_PLUS_PROFILE_ID',
],
 
        // la configuración global para el widget del plugin de google analytic
        'googleAnalytics' => [
    'id' => 'TRACKING_ID',
    'domain' => 'TRACKING_DOMAIN',
],
        
        // la configuración global para el widget del plugin de twitter
        'twitter' => [
    'screenName' => 'TWITTER_SCREEN_NAME'
    ],
    ],
    // sus otros módulos
    'datecontrol' =>  [
        'class' => 'kartik\datecontrol\Module',
        // format settings for displaying each date attribute
        'displaySettings' => [
            'date' => 'd-m-Y',
            'time' => 'H:i:s A',
            'datetime' => 'd-m-Y H:i:s A',
        ],
        // format settings for saving each date attribute
        'saveSettings' => [
            'date' => 'Y-m-d', 
            'time' => 'H:i:s',
            'datetime' => 'Y-m-d H:i:s',
        ],
        // automatically use kartik\widgets for each of the above formats
        'autoWidget' => true,
    ]        
        
],

    'language' => 'es-ES',    
    'timeZone' => 'America/Buenos_Aires',
    //'timeZone'=>'America/Argentina/Buenos_Aires', 
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'fecha' => [
            'class' => 'common\components\FechaComponent',
        ],
        'nlp' => [
            'class' => 'common\components\WebnlpComponent',
        ],
        'flask' => [
            'class' => 'common\components\FlaskComponent',
        ],
        'stanza' => [
            'class' => 'common\components\StanzaComponent',
        ],
        'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],    
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'timeZone' => 'America/Argentina/Buenos_Aires'
        ],         
 
    ],
];
