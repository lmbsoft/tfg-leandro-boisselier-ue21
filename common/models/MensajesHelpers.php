<?php
namespace common\models;

use yii;
use yii\helpers\VarDumper;
class MensajesHelpers
{
    public static function fechaRemitenteMensaje($m){
        $frm = [];
        $remitente = null;
        $mensaje = null;
        $fecha_mensaje = mb_split("-",$m,2);
        $fechastr = trim($fecha_mensaje[0]);
        $fecha=\DateTime::createFromFormat("!j/n/y H:i", $fechastr);
        //yii::warning(VarDumper::dumpAsString($fecha));

        if($fecha==false){ //un intento más de convertir con otro formato - 6/12/18 12:37 p. m.

            $fecha=\DateTime::createFromFormat("!j/n/Y H:i", $fechastr); //pruebo con un año de 4 dígitos

            if($fecha==false){ //un intento más de convertir con otro formato - 6/12/18 12:37 p. m.

                $fs = mb_split(" ",$fechastr,3);
                if(count($fs)==3){
                    $fechastr2 = $fs[0]." ".$fs[1];
                    if($fs[2]=="a. m."){
                        $fechastr2 .= " am";
                    }else{
                        $fechastr2 .= " pm";
                    }
                    $fecha=\DateTime::createFromFormat("!j/n/y g:i a", $fechastr2);
                    //yii::warning(VarDumper::dumpAsString($fecha));
                    
                    if($fecha==false){ //un intento más de convertir con otro formato - 6/12/2018 12:37 p. m.
                        $fecha=\DateTime::createFromFormat("!j/n/Y g:i a", $fechastr2);
                    }
                }
            }
        }
        if($fecha){
            $remitente_mensaje = mb_split(":",$fecha_mensaje[1],2);

            if(count($remitente_mensaje)==2){
                $remitente = trim($remitente_mensaje[0]);
                $mensaje = trim($remitente_mensaje[1]);

                $frm=[
                    'fecha'=>$fecha->format('Y-m-d H:i:s'),
                    'remitente'=>$remitente,
                    'mensaje'=>$mensaje,
                    'id_proyecto' => 0,
                    'id_persona' => 0,
                ];
            }else{ //mensajes de control, como X añadió a 123
                $frm=[//al devolverlo vacío lo va a ignorar
                ];
            }
        }else{
            
            $frm=[
                'fecha'=>null,
                'remitente'=>null,
                'mensaje'=>$m,
                'id_proyecto' => 0,
                'id_persona' => 0,
            ];            
        }
        return($frm);
    }
    
    public static function removerStopwords($texto, $entrada_stopwords=null){
        //primero los stopwords
        $stopwords = "";
        if($entrada_stopwords){
            $stopwords = $entrada_stopwords;
        }
        //$reemplazos = preg_split('/\n/',$stopwords);
        $reemplazos = preg_split('/\r\n/',$stopwords);
        
        //$reemplazos= Reemplazos::find()->all();
        $patron=[];
        $reemplazo=[];
        foreach ($reemplazos as $reemp){
            //$patron[]='/\b'.$reemp.'\b/i';
            //$patron[]='/\b'.trim($reemp).'\b/i';
            if(strlen(trim($reemp))>0){
                $patron[]="/\b".strtolower($reemp)."\b/i"; //ver si es comilla simple o doble
                //$reemplazo[]=$reemp->reemplazo;
                $reemplazo[]=" "; //reemplazo por vacio para eliminar los stopwords
            }
        }
        
        $cadenaReemplazada = preg_replace($patron, $reemplazo, $texto);
        
        return $cadenaReemplazada;

    }
    
    public static function dateTominutes($seconds){
        $string = "";

        $days = intval(intval($seconds) / (3600*24));
        $hours = (intval($seconds) / 3600) % 24;
        $minutes = (intval($seconds) / 60) % 60;
        $seconds = (intval($seconds)) % 60;

        if($days> 0){
            $string .= $days==1?"$days día ":"$days días ";
        }
        if($hours > 0){
            $string .= $hours==1?"$hours hora ":"$hours horas ";
        }
        if($minutes > 0){
            $string .= $minutes==1?"$minutes minuto ":"$minutes minutos ";
        }
        if ($seconds > 0){
            $string .= $seconds==1?"$seconds segundo":"$seconds segundos";
        }

        return $string;
    }            
}
