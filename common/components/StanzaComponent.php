<?php

namespace common\components;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use yii\helpers\Json;
use yii\helpers\VarDumper;

//use yii\base\InvalidParamException;
//use yii\web\BadRequestHttpException;

class StanzaComponent extends \yii\base\Component {
    
    public function stanzatest($texto, $lang="en") { {
            $client = new \GuzzleHttp\Client();
            $pedido = [
                'text' => $texto,
                'personas' => $personas,
                'lang' => $lang,
            ];
            $response = null;
            $resultado = null;

            try {
                $response = $client->request('POST', 'http://flask:5000/stanzatest', [
                //$response = $client->request('POST', 'http://acodi.aplicacionesonline.com.ar:5000/stanzatest', [
                    'allow_redirects' => true,
                    'timeout' => 6000,
                    //'body' => json_encode($pedido),
                    'json' => $pedido,
                ]);

                $resultado = $response->getBody()->getContents();
                return json_decode($resultado);
            } catch (RequestException $e) {

                //echo var_dump(($e->getRequest()));
                \Yii::error(VarDumper::dumpAsString($e->getRequest()));

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
//                    var_dump($response->getStatusCode()); // HTTP status code;
//                    var_dump($response->getReasonPhrase()); // Response message;
//                    var_dump((string) $response->getBody()); // Body, normally it is JSON;
//                    var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//                    var_dump($response->getHeaders()); // Headers array;
//                    var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//                    var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
                }
            }
        }
    }
    
    
}
