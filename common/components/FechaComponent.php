<?php
namespace common\components;

class FechaComponent extends \yii\base\Component{
    public function getFecha($formato='Y-m-d H:i:s'){
        return date($formato);
    }
}