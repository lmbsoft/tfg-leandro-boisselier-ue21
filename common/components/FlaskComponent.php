<?php

namespace common\components;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use yii\helpers\Json;
use yii\helpers\VarDumper;

//use yii\base\InvalidParamException;
//use yii\web\BadRequestHttpException;

class FlaskComponent extends \yii\base\Component {

    public function imagen($texto) { {
            $client = new \GuzzleHttp\Client();
            //$response = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');
            $pedido = [
                'text' => $texto,
                'lang' => 'es',
            ];
            $response = null;
            $resultado = null;

            try {
                $response = $client->request('POST', 'http://flask:5000/imagen', [
                    'allow_redirects' => true,
                    'timeout' => 10,
                    'form_params' => $pedido,
                ]);

                $resultado = $response->getBody()->getContents();
                //\Yii::error(VarDumper::dumpAsString("Resultado!:".$resultado));
                //return json_decode($resultado);
                return ($resultado);
            } catch (RequestException $e) {
                /**
                 * Here we actually catch the instance of GuzzleHttp\Psr7\Response
                 * (find it in ./vendor/guzzlehttp/psr7/src/Response.php) with all
                 * its own and its 'Message' trait's methods. See more explanations below.
                 *
                 * So you can have: HTTP status code, message, headers and body.
                 * Just check the exception object has the response before.
                 */
                //echo var_dump(($e->getRequest()));
                \Yii::error(VarDumper::dumpAsString($e->getRequest()));

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
//                    var_dump($response->getStatusCode()); // HTTP status code;
//                    var_dump($response->getReasonPhrase()); // Response message;
//                    var_dump((string) $response->getBody()); // Body, normally it is JSON;
//                    var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//                    var_dump($response->getHeaders()); // Headers array;
//                    var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//                    var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
                }
            }
        }
    }

    public function data($texto) { {
            $client = new \GuzzleHttp\Client();
            $pedido = [
                'text' => $texto,
                'lang' => 'es',
            ];
            $response = null;
            $resultado = null;

            try {
                $response = $client->request('POST', 'http://flask:5000/data', [
                    'allow_redirects' => true,
                    'timeout' => 120,
                    //'body' => json_encode($pedido),
                    'json' => $pedido,
                ]);

                $resultado = $response->getBody()->getContents();
                return ($resultado);
            } catch (RequestException $e) {

                //echo var_dump(($e->getRequest()));
                \Yii::error(VarDumper::dumpAsString($e->getRequest()));

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
//                    var_dump($response->getStatusCode()); // HTTP status code;
//                    var_dump($response->getReasonPhrase()); // Response message;
//                    var_dump((string) $response->getBody()); // Body, normally it is JSON;
//                    var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//                    var_dump($response->getHeaders()); // Headers array;
//                    var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//                    var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
                }
            }
        }
    }
    
    public function heatmap($texto) { {
            $client = new \GuzzleHttp\Client();
            $pedido = [
                'text' => $texto,
                'lang' => 'es',
            ];
            $response = null;
            $resultado = null;

            try {
                $response = $client->request('POST', 'http://flask:5000/heatmap', [
                    'allow_redirects' => true,
                    'timeout' => 120,
                    //'body' => json_encode($pedido),
                    'json' => $pedido,
                ]);

                $resultado = $response->getBody()->getContents();
                return ($resultado);
            } catch (RequestException $e) {

                //echo var_dump(($e->getRequest()));
                \Yii::error(VarDumper::dumpAsString($e->getRequest()));

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
//                    var_dump($response->getStatusCode()); // HTTP status code;
//                    var_dump($response->getReasonPhrase()); // Response message;
//                    var_dump((string) $response->getBody()); // Body, normally it is JSON;
//                    var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//                    var_dump($response->getHeaders()); // Headers array;
//                    var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//                    var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
                }
            }
        }
    }
    
    public function mensajesxpersona($texto) { {
            $client = new \GuzzleHttp\Client();
            $pedido = [
                'text' => $texto,
                'lang' => 'es',
            ];
            $response = null;
            $resultado = null;

            try {
                $response = $client->request('POST', 'http://flask:5000/mensajesxpersona', [
                    'allow_redirects' => true,
                    'timeout' => 300,
                    //'body' => json_encode($pedido),
                    'json' => $pedido,
                ]);

                $resultado = $response->getBody()->getContents();
                return ($resultado);
            } catch (RequestException $e) {

                //echo var_dump(($e->getRequest()));
                \Yii::error(VarDumper::dumpAsString($e->getRequest()));

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
//                    var_dump($response->getStatusCode()); // HTTP status code;
//                    var_dump($response->getReasonPhrase()); // Response message;
//                    var_dump((string) $response->getBody()); // Body, normally it is JSON;
//                    var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//                    var_dump($response->getHeaders()); // Headers array;
//                    var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//                    var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
                }
            }
        }
    }
    
    public function estadisticas($texto, $personas) { {
            $client = new \GuzzleHttp\Client();
            $pedido = [
                'text' => $texto,
                'personas' => $personas,
                'lang' => 'es',
            ];
            $response = null;
            $resultado = null;

            try {
                $response = $client->request('POST', 'http://flask:5000/estadisticas', [
                    'allow_redirects' => true,
                    'timeout' => 300,
                    //'body' => json_encode($pedido),
                    'json' => $pedido,
                ]);

                $resultado = $response->getBody()->getContents();
                return ($resultado);
            } catch (RequestException $e) {

                //echo var_dump(($e->getRequest()));
                \Yii::error(VarDumper::dumpAsString($e->getRequest()));

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
//                    var_dump($response->getStatusCode()); // HTTP status code;
//                    var_dump($response->getReasonPhrase()); // Response message;
//                    var_dump((string) $response->getBody()); // Body, normally it is JSON;
//                    var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//                    var_dump($response->getHeaders()); // Headers array;
//                    var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//                    var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
                }
            }
        }
    }
    
    
    public function textbasics($texto, $lang="en", $stopwords =[], $topics=4) { {
            $client = new \GuzzleHttp\Client();
            $language = "english";
            switch ($lang){
                case "en": $language = "english";break;
                case "es": $language = "spanish";break;
                case "it": $language = "italian";break;
                default: $language = "english";
            }
            
            $pedido = [
                'text' => $texto,
                'lang' => $language,
                'stopwords' => $stopwords,
                'topics' => $topics,
            ];
            
            $response = null;
            $resultado = null;

            try {
                $response = $client->request('POST', 'http://flask:5000/textbasics', [
                    'allow_redirects' => true,
                    'timeout' => 300,
                    //'body' => json_encode($pedido),
                    'json' => $pedido,
                ]);

                $resultado = $response->getBody()->getContents();
                return ($resultado);
            } catch (RequestException $e) {

                //echo var_dump(($e->getRequest()));
                \Yii::error(VarDumper::dumpAsString($e->getRequest()));

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
//                    var_dump($response->getStatusCode()); // HTTP status code;
//                    var_dump($response->getReasonPhrase()); // Response message;
//                    var_dump((string) $response->getBody()); // Body, normally it is JSON;
//                    var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//                    var_dump($response->getHeaders()); // Headers array;
//                    var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//                    var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
                }
            }
        }
    }
    
}
