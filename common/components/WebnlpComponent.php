<?php

namespace common\components;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use Yii;

//use yii\base\InvalidParamException;
//use yii\web\BadRequestHttpException;

class WebnlpComponent extends \yii\base\Component {

    public function valorarSentimientos($texto) { {
            $client = new \GuzzleHttp\Client();
            //$response = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');
            $pedido = [
                'text' => $texto,
                'lang' => 'es',
            ];
            $response = null;
            $resultado = null;

            try {
                $response = $client->request('POST', 'http://web64nlpserver:6400/polyglot/sentiment', [
                    'allow_redirects' => true,
                    'timeout' => 120,
                    'form_params' => $pedido,
                ]);

                $resultado = $response->getBody()->getContents();
                return json_decode($resultado);
            } catch (RequestException $e) {
                /**
                 * Here we actually catch the instance of GuzzleHttp\Psr7\Response
                 * (find it in ./vendor/guzzlehttp/psr7/src/Response.php) with all
                 * its own and its 'Message' trait's methods. See more explanations below.
                 *
                 * So you can have: HTTP status code, message, headers and body.
                 * Just check the exception object has the response before.
                 */
                \Yii::error(VarDumper::dumpAsString($e->getRequest()));

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
//                    var_dump($response->getStatusCode()); // HTTP status code;
//                    var_dump($response->getReasonPhrase()); // Response message;
//                    var_dump((string) $response->getBody()); // Body, normally it is JSON;
//                    var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//                    var_dump($response->getHeaders()); // Headers array;
//                    var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//                    var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
                }
            }
        }
    }

    public function generarResumen($texto, $word_count=100) { {
            $client = new \GuzzleHttp\Client();
            //$response = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');
            $pedido = [
                'text' => $texto,
                'word_count' => $word_count,
            ];
            $response = null;
            $resultado = null;

            try {
                //$response = $client->request('POST', Yii::$app->params['urlWebnlp'].'/gensim/summarize', [
                //$response = $client->request('POST', 'http://acodi.aplicacionesonline.com.ar:6400/gensim/summarize', [
                $response = $client->request('POST', 'http://web64nlpserver:6400/gensim/summarize', [
                    'allow_redirects' => true,
                    'timeout' => 600,
                    'form_params' => $pedido,
                ]);

                $resultado = $response->getBody()->getContents();
                return json_decode($resultado);
            } catch (RequestException $e) {
                /**
                 * Here we actually catch the instance of GuzzleHttp\Psr7\Response
                 * (find it in ./vendor/guzzlehttp/psr7/src/Response.php) with all
                 * its own and its 'Message' trait's methods. See more explanations below.
                 *
                 * So you can have: HTTP status code, message, headers and body.
                 * Just check the exception object has the response before.
                 */
                \Yii::error(VarDumper::dumpAsString($e->getRequest()));

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
//                    var_dump($response->getStatusCode()); // HTTP status code;
//                    var_dump($response->getReasonPhrase()); // Response message;
//                    var_dump((string) $response->getBody()); // Body, normally it is JSON;
//                    var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//                    var_dump($response->getHeaders()); // Headers array;
//                    var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//                    var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
                }
            }
        }
    }

    
    public function valorarEntitiesPolyglot($texto, $lang="en") { {
            $client = new \GuzzleHttp\Client();
            //$response = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');
            $pedido = [
                'text' => $texto,
                'lang' => $lang,
            ];
            $response = null;
            $resultado = null;

            try {
                $response = $client->request('POST', 'http://web64nlpserver:6400/polyglot/entities', [
                    'allow_redirects' => true,
                    'timeout' => 600,
                    'form_params' => $pedido,
                ]);

                $resultado = $response->getBody()->getContents();
                //\Yii::warning(VarDumper::dumpAsString($resultado));
                return Json::decode($resultado, true); //json_decode($resultado); //
            } catch (RequestException $e) {
                /**
                 * Here we actually catch the instance of GuzzleHttp\Psr7\Response
                 * (find it in ./vendor/guzzlehttp/psr7/src/Response.php) with all
                 * its own and its 'Message' trait's methods. See more explanations below.
                 *
                 * So you can have: HTTP status code, message, headers and body.
                 * Just check the exception object has the response before.
                 */
                \Yii::error(VarDumper::dumpAsString($e->getRequest()));

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
//                    var_dump($response->getStatusCode()); // HTTP status code;
//                    var_dump($response->getReasonPhrase()); // Response message;
//                    var_dump((string) $response->getBody()); // Body, normally it is JSON;
//                    var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//                    var_dump($response->getHeaders()); // Headers array;
//                    var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//                    var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
                }
            }
        }
    }


    public function valorarEntitiesSpacy($texto, $lang="en") { {
            $client = new \GuzzleHttp\Client();
            //$response = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');
            $pedido = [
                'text' => $texto,
                'lang' => $lang,
            ];
            $response = null;
            $resultado = null;

            try {
                $response = $client->request('POST', 'http://web64nlpserver:6400/spacy/entities', [
                    'allow_redirects' => true,
                    'timeout' => 600,
                    'form_params' => $pedido,
                ]);

                $resultado = $response->getBody()->getContents();
                return Json::decode($resultado,true); //json_decode($resultado); //
            } catch (RequestException $e) {
                /**
                 * Here we actually catch the instance of GuzzleHttp\Psr7\Response
                 * (find it in ./vendor/guzzlehttp/psr7/src/Response.php) with all
                 * its own and its 'Message' trait's methods. See more explanations below.
                 *
                 * So you can have: HTTP status code, message, headers and body.
                 * Just check the exception object has the response before.
                 */
                \Yii::error(VarDumper::dumpAsString($e->getRequest()));

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
//                    var_dump($response->getStatusCode()); // HTTP status code;
//                    var_dump($response->getReasonPhrase()); // Response message;
//                    var_dump((string) $response->getBody()); // Body, normally it is JSON;
//                    var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//                    var_dump($response->getHeaders()); // Headers array;
//                    var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//                    var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;
                }
            }
        }
    }
    
}
