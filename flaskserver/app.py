from flask import Flask, jsonify, request, render_template_string
import pickle
from wordcloud import WordCloud, STOPWORDS
import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import emoji

import io
import base64
import sys

import stanza
#leandro
app = Flask(__name__)

@app.route('/api/echo-json',methods = ['GET','POST'])
def add():
    data = request.get_json()
    return jsonify(data)

@app.route('/postjson',methods = ['POST'])
def postJsonHandler():
    data = request.get_json()
    return jsonify(data)

@app.route('/imagen2',methods = ['GET','POST'])
def imagen2():
    data = request.get_json()
    return jsonify({"campo":"esto va nulo"})

#https://stackoverflow.com/questions/38061267/matplotlib-graphic-image-to-base64
@app.route('/imagen',methods = ['GET','POST'])
def imagen():
    s = io.BytesIO()
    plt.plot(list(range(100)))
    plt.savefig(s, format='png', bbox_inches="tight")
    plt.close()
    s = base64.b64encode(s.getvalue()).decode("utf-8").replace("\n", "")
    return '<img align="left" src=''data:image/png;base64,%s''>' % s

@app.route('/data', methods = ['POST'])
def data():
    dataJson = request.get_json()
    print('This is error output', file=sys.stderr)
    #print(dataJson, file=sys.stderr)
    ###print('This is standard output', file=sys.stdout)
    data_string = dataJson['text']

    #data_string = "9/6/20 22:21 - Héctor Jaskolowski: Grande Fabián 👍"
    data = io.StringIO(data_string)
    data = pd.read_csv(data, delimiter = "\t", header = None, names = ['text'])


    # Extract datetime
    data[['datetime_str','text_2']] = data["text"].str.split(" - ", 1, expand=True)
    data["datetime"] = pd.to_datetime(data["datetime_str"], format="%d/%m/%y %H:%M", errors='coerce')
    #print(data)
    data = data.dropna(subset=['datetime'])
    data = data.drop(columns = ['datetime_str'])

    # Extract sender and message
    data[['sender','text_message']] = data['text_2'].str.split(': ', 1, expand=True)
    data = data.dropna(subset=['text_message'])
    data = data.drop(columns = ['text','text_2'])

    # Anonomise Names
    #data['first_name'] = data['sender'].str.split(' ', expand = True)[0]
    #data['last_name'] = data['sender'].str.split(' ', expand = True)[1]
    #data['sender'] = data['first_name'].where(data['last_name'].isnull(), data['first_name'] + ' ' + data['last_name'].str[0])
    #data = data.drop(columns = ['first_name','last_name'])

    return data.to_json()


import base64
import traceback

# Añadir variable de debug al inicio
DEBUG = False

@app.route('/heatmap', methods=['POST'])
def heatmap():
    try:
        if DEBUG:
            print("Entering heatmap function", file=sys.stderr)
        
        dataJson = request.get_json()
        if not dataJson:
            if DEBUG:
                print("No JSON data received", file=sys.stderr)
            return jsonify({"error": "No JSON data received"}), 400
        
        if DEBUG:
            print('Received JSON data:', dataJson, file=sys.stderr)
        
        if 'text' not in dataJson:
            if DEBUG:
                print("'text' key not found in JSON data", file=sys.stderr)
            return jsonify({"error": "'text' key not found in JSON data"}), 400
        
        data_string = dataJson['text']
        if DEBUG:
            print('Data string length:', len(data_string), file=sys.stderr)

        if len(data_string) == 0:
            if DEBUG:
                print("Empty data string received", file=sys.stderr)
            return jsonify({"error": "Empty data string received"}), 400

        data = io.StringIO(data_string)
        data = pd.read_csv(data, delimiter="\t", header=None, names=['text'])
        if DEBUG:
            print('DataFrame shape:', data.shape, file=sys.stderr)

        if data.empty:
            if DEBUG:
                print("DataFrame is empty after parsing", file=sys.stderr)
            return jsonify({"error": "No valid data after parsing"}), 400

        # Extract datetime
        data[['datetime_str','text_2']] = data["text"].str.split(" - ", 1, expand=True)
        data["datetime"] = pd.to_datetime(data["datetime_str"], format="%d/%m/%y %H:%M", errors='coerce')
        data["datetime"] = data["datetime"].dt.strftime('%Y-%m-%d %H:%M')
        data = data.dropna(subset=['datetime'])
        data = data.drop(columns=['datetime_str'])

        # Extract sender and message
        data[['sender','text_message']] = data['text_2'].str.split(': ', 1, expand=True)
        data = data.dropna(subset=['text_message'])
        data = data.drop(columns=['text','text_2'])

        if DEBUG:
            print('Processed DataFrame shape:', data.shape, file=sys.stderr)

        if data.empty:
            if DEBUG:
                print("DataFrame is empty after processing", file=sys.stderr)
            return jsonify({"error": "No valid data after processing"}), 400

        # Create new fields to use in heatmap
        data['day_of_week'] = pd.to_datetime(data['datetime']).dt.dayofweek
        data['hour_of_day'] = pd.to_datetime(data['datetime']).dt.hour

        # Create new Dataframe containing data counts
        heatmap_data = data.groupby(['day_of_week', 'hour_of_day']).size().unstack(fill_value=0)
        if DEBUG:
            print('Heatmap data shape:', heatmap_data.shape, file=sys.stderr)
            print('Heatmap data:\n', heatmap_data, file=sys.stderr)

        if heatmap_data.empty:
            if DEBUG:
                print("Heatmap data is empty", file=sys.stderr)
            return jsonify({"error": "No data for heatmap"}), 400

        # Create heatmap
        fig, ax = plt.subplots(figsize=(15, 10))
        im = ax.imshow(heatmap_data, cmap='Reds')

        # Set ticks and labels
        ax.set_xticks(np.arange(len(heatmap_data.columns)))
        ax.set_yticks(np.arange(len(heatmap_data.index)))
        ax.set_xticklabels(heatmap_data.columns)
        
        # Use only the days present in the data
        days = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo']
        present_days = [days[i] for i in heatmap_data.index]
        ax.set_yticklabels(present_days)

        # Rotate the tick labels and set their alignment
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

        # Loop over data dimensions and create text annotations
        for i in range(len(heatmap_data.index)):
            for j in range(len(heatmap_data.columns)):
                text = ax.text(j, i, heatmap_data.iloc[i, j],
                               ha="center", va="center", color="black")

        ax.set_title("Cantidad de mensajes por día de la semana y hora del día")
        ax.set_xlabel("Hora del día")
        ax.set_ylabel("Día de la semana")
        fig.tight_layout()

        # Save the plot to a BytesIO object
        img_buffer = io.BytesIO()
        plt.savefig(img_buffer, format='png', bbox_inches="tight", dpi=300)
        img_buffer.seek(0)
        plt.close()

        # Encode the image to base64
        img_str = base64.b64encode(img_buffer.getvalue()).decode('utf-8')

        if DEBUG:
            print("Successfully generated image", file=sys.stderr)
        
        # Create HTML string with the image
        html_string = f'<img src="data:image/png;base64,{img_str}" alt="Heatmap" style="width: 100%; max-width: 1200px; height: auto;">'
        
        return html_string
    except Exception as e:
        if DEBUG:
            print('Error occurred:', str(e), file=sys.stderr)
            print('Traceback:', traceback.format_exc(), file=sys.stderr)
        return f"Error: {str(e)}"


@app.route('/wordcloud', methods=['POST'])
def wordcloud():
    try:
        dataJson = request.get_json()
        if not dataJson or 'text' not in dataJson:
            return jsonify({"error": "No text data provided"}), 400

        text = dataJson['text']

        stopwords_list = dataJson.get('stopwords', [])  # Obtener stopwords, vacío por defecto
        if stopwords_list is None: # Manejar el caso donde stopwords es null
            stopwords_list = []

        stemmers_dict = dataJson.get('stemmers', {}) # Obtener stemmers, vacío por defecto
        if stemmers_dict is None: # Manejar el caso donde stemmers es null
            stemmers_dict = {}

        # Aplicar stemming (reemplazo con stemmers)
        for valor, reemplazo in stemmers_dict.items():
            text = text.replace(valor, reemplazo)

        # Generar la nube de palabras
        stopwords = set(STOPWORDS).union(stopwords_list)  # Combinar stopwords por defecto con la lista recibida
        wc = WordCloud(stopwords=stopwords, background_color="white", max_words=200, width=800, height=400).generate(text)

        # Convertir la imagen a base64
        plt.figure(figsize=(10,5))
        plt.imshow(wc, interpolation='bilinear')
        plt.axis("off")

        img_buffer = io.BytesIO()
        plt.savefig(img_buffer, format='png', bbox_inches="tight")
        img_buffer.seek(0)
        plt.close()

        img_str = base64.b64encode(img_buffer.getvalue()).decode('utf-8')
        html_string = f'<img src="data:image/png;base64,{img_str}" alt="Wordcloud">'

        return html_string

    except Exception as e:
        return f"Error: {str(e)}"


@app.route('/emoji_count', methods=['POST'])
def emoji_count():
    try:
        dataJson = request.get_json()
        if not dataJson or 'text' not in dataJson:
            return jsonify({"error": "No text data provided"}), 400

        text = dataJson['text']

        # Contar emojis (similar a la lógica en estadisticas)
        emoji_counts = {}
        for char in text:
            if char in emoji.UNICODE_EMOJI_SPANISH:
                emoji_name = emoji.demojize(char, language='es')
                emoji_counts[char] = emoji_counts.get(char, {'count': 0, 'name': emoji_name})
                emoji_counts[char]['count'] += 1

        # Ordenar por frecuencia (opcional)
        sorted_emojis = sorted(emoji_counts.items(), key=lambda item: item[1]['count'], reverse=True)

        return jsonify(sorted_emojis)

    except Exception as e:
        return f"Error: {str(e)}"
    

@app.route('/mensajesxpersona', methods = ['POST'])
def mensajesxpersona():
    dataJson = request.get_json()
    print('This is error output', file=sys.stderr)
    #print(dataJson, file=sys.stderr)
    ###print('This is standard output', file=sys.stdout)
    data_string = dataJson['text']

    data = io.StringIO(data_string)
    data = pd.read_csv(data, delimiter = "\t", header = None, names = ['text'])

    # Extract datetime
    data[['datetime_str','text_2']] = data["text"].str.split(" - ", 1, expand=True)
    data["datetime"] = pd.to_datetime(data["datetime_str"], format="%d/%m/%y %H:%M", errors='coerce')
    #print(data)
    data = data.dropna(subset=['datetime'])
    data = data.drop(columns = ['datetime_str'])

    # Extract sender and message
    data[['sender','text_message']] = data['text_2'].str.split(': ', 1, expand=True)
    data = data.dropna(subset=['text_message'])
    data = data.drop(columns = ['text','text_2'])

    # Create sender counts as a series
    sender_count_series = data.groupby(['sender']).size().sort_values(ascending=False)

    # Create sender counts series as a DataFrame
    sender_count_df = pd.DataFrame(sender_count_series)

    # Reset index in order to name columns correctly
    sender_count_df = sender_count_df.reset_index()
    sender_count_df.columns = ['sender', 'count']

    # Plot bar chart with sender message counts
    s = io.BytesIO()
    plt.figure(figsize=(15, 5))
    plt.bar(sender_count_df['sender'], sender_count_df['count'], color='green')
    plt.xlabel("Persona")
    plt.ylabel("Cantidad de Mensajes")
    plt.title("Cantidad de mensajes por persona")
    plt.xticks(rotation=45, ha="right")
    #plt.show()    


    plt.savefig(s, format='png', bbox_inches="tight")
    plt.close()
    s = base64.b64encode(s.getvalue()).decode("utf-8").replace("\n", "")
    return '<img align="left" src=''data:image/png;base64,%s''>' % s


@app.route('/estadisticas', methods = ['POST'])
def estadisticas():
    dataJson = request.get_json()
    print('This is error output', file=sys.stderr)
    #print(dataJson, file=sys.stderr)
    ###print('This is standard output', file=sys.stdout)
    data_string = dataJson['text']
    personas = dataJson['personas']
    #print(personas, file=sys.stderr)

    data = io.StringIO(data_string)
    data = pd.read_csv(data, delimiter = "\t", header = None, names = ['text'])

    # Extract datetime
    data[['datetime_str','text_2']] = data["text"].str.split(" - ", 1, expand=True)
    data["datetime"] = pd.to_datetime(data["datetime_str"], format="%d/%m/%y %H:%M", errors='coerce')
    #print(data)
    data = data.dropna(subset=['datetime'])
    data = data.drop(columns = ['datetime_str'])

    # Extract sender and message
    data[['sender','text_message']] = data['text_2'].str.split(': ', 1, expand=True)
    data = data.dropna(subset=['text_message'])
    data = data.drop(columns = ['text','text_2'])

    retornar={}

    #return '<img align="left" src=''data:image/png;base64,%s''>' % s

    #Most Popular Words
    ## Create DataFrame with all words used split by sender
    # Filter out 'Media Omitted' messages
    data_texts = data[~data['text_message'].str.contains('omitted')]

    # Create empty DataFrame to use for the word count
    words = pd.DataFrame(columns=['sender','words'])

    # Loop through all messages in the DataFrame
    for sender, message in zip(data_texts.sender, data_texts.text_message):
 
        # Split out each word in each message
        message_split = message.split()
        for word in message_split:
 
            # Add each word to the DataFrame
            words = words.append({'sender' : sender , 'words' : word.lower()}, ignore_index=True)

    # Remove punctuation from the start and end of words
    words['words'] = words['words'].str.replace('(^\W)|(\W$)','')

    # Create list of 'non-words' that we don't care about
    non_words = ['the','to','a','i','you','it','and','for','be','that','in','of','have','on','at','is','as','or','we','if','so','']

    # Filter out this list of 'non-words' from each set of words
    words_filtered = words[~words['words'].isin(non_words)]

    # Create word counts as a series
    word_count_total_series = words_filtered.groupby(['words']).size().sort_values(ascending=False)

    # Create word counts series as a DataFrame
    word_count_total = pd.DataFrame(word_count_total_series)

    # Reset index in order to name columns correctly
    word_count_total = word_count_total.reset_index()
    word_count_total.columns = ['words', 'count']

    # Display top 10 words used
    retornar['palabras_mas_populares'] = word_count_total.head(20)
    #print(retornar, file=sys.stderr)


    
    #Ranking
    retornar['ranking'] = word_count_total[word_count_total['words'].isin(['felicitaciones','bien','no','en','la','se','que','y','se'])]
    #print(retornar, file=sys.stderr)


    #Words by Sender
    # Create word counts by sender as a series
    word_count_by_sender_series = words_filtered.groupby(['sender', 'words']).size().sort_values(ascending=False)

    # Create word counts by sender series as a DataFrame
    word_count_by_sender = pd.DataFrame(word_count_by_sender_series)

    # Reset index in order to name columns correctly
    word_count_by_sender = word_count_by_sender.reset_index()
    word_count_by_sender.columns = ['sender', 'words', 'count']

    s = io.BytesIO()

    retornar['palabras_usadas_por']={}
    #print(personas, file=sys.stderr)

    for persona in personas:
        #print(persona, file=sys.stderr)
        s = io.BytesIO()

        # Plot bar chart with top n all words used by me
        word_count_by_sender_top_n = word_count_by_sender[word_count_by_sender['sender']==persona].head(20).sort_values(by='count',ascending=True)
        plt.barh(word_count_by_sender_top_n['words'], word_count_by_sender_top_n['count'], color='green')
        plt.xlabel("Cantidad de Palabras Unicas")
        plt.ylabel("Palabra")
        plt.title("Palabras usadas por " + persona)
        #plt.show()
        plt.savefig(s, format='png', bbox_inches="tight")
        plt.close()
        s = base64.b64encode(s.getvalue()).decode("utf-8").replace("\n", "")
        retornar['palabras_usadas_por'][persona] = '<img align="left" src=''data:image/png;base64,%s''>' % s    
        #print(retornar, file=sys.stderr)


    #Most Popular Emojis
    # Clear DataFrame to create words used
    emojis = pd.DataFrame(columns=['sender','emoji','emojiname','datetime'])
    # Loop through all messages in the DataFrame
    for sender, message, datetime in zip(data.sender, data.text_message, data.datetime):
    # Split out each word in each message
        message_split = list(message)
 
        # Loop through each word in split message
        for character in message_split:
            # If the word is an emoji
            #if character in emoji.UNICODE_EMOJI and character != "\U0001f3fc":
            if character in emoji.UNICODE_EMOJI_SPANISH and character != "\U0001f3fc":
                # Add each emoji to the DataFrame
                #print(character, file=sys.stderr)
                emojiname = emoji.demojize(character, language = "es")
                emojis = emojis.append({'sender' : sender, 'emoji' : character, 'emojiname' : emojiname, 'datetime' : datetime}, ignore_index=True)
            #print(character, file=sys.stderr)

    # Display top n most popular emojis
    #retornar['emojis'] = emojis.groupby(['emoji']).size().sort_values(ascending=False).head(30)
    retornar['emojis'] = emojis.groupby(['emoji','emojiname']).size().sort_values(ascending=False).head(30)
    #print(emoji.UNICODE_EMOJI_SPANISH, file=sys.stderr)

   
    return json.dumps([
        retornar['palabras_mas_populares'].values.tolist(),
        retornar['palabras_usadas_por'],
        #retornar['emojis'].values.tolist(),
        retornar['emojis'].to_json(),
        ])

    #retornar['emojis'].to_json()
    #retornar['palabras_mas_populares'].to_json() 
    #json.dumps(retornar['palabras_usadas_por'])



@app.route('/stanzatest',methods = ['POST'])
def stanzatest():
    dataJson = request.get_json()
    data_string = dataJson['text']
    lang = dataJson['lang']
    nlp = stanza.Pipeline(lang, pos_batch_size=6000) #'en'
    doc = nlp(data_string) # "Barack Obama was born in Hawaii."
    print('This is error output', file=sys.stderr)
    #print(doc, file=sys.stderr)
    #print(doc.to_dict(), file=sys.stderr)

    return jsonify(doc.to_dict()) #jsonify(doc.tolist())


@app.route('/textbasics', methods = ['POST'])
#Text analysis basics in Python
#https://towardsdatascience.com/text-analysis-basics-in-python-443282942ec5
def textbasics():
    dataJson = request.get_json()
    print('This is error output', file=sys.stderr)
    #print(dataJson, file=sys.stderr)
    ###print('This is standard output', file=sys.stdout)

    retornar={}

    corpus = dataJson['text']
    lang = dataJson['lang']
    topics = dataJson['topics']
    stopwords2 = dataJson['stopwords']

    import pandas as pd
    df = pd.DataFrame(corpus)
    df.columns = ['textos']

    from textblob import TextBlob

    df['polarity'] = df['textos'].apply(lambda x: TextBlob(x).polarity)
    df['subjective'] = df['textos'].apply(lambda x: TextBlob(x).subjectivity)

    retornar['df'] = df.to_dict()


    #Sentiment analysis of Bigram/Trigram
    from nltk.corpus import stopwords
    #stoplist = stopwords.words('english') + ['though']
    stoplist = stopwords.words(lang) + stopwords2

    from sklearn.feature_extraction.text import CountVectorizer
    c_vec = CountVectorizer(stop_words=stoplist, ngram_range=(2,3))
    # matrix of ngrams
    ngrams = c_vec.fit_transform(df['textos'])

    # count frequency of ngrams
    count_values = ngrams.toarray().sum(axis=0)
    # list of ngrams
    vocab = c_vec.vocabulary_
    df_ngram = pd.DataFrame(sorted([(count_values[i],k) for k,i in vocab.items()], reverse=True)).rename(columns={0: 'frequency', 1:'bigram/trigram'})
    df_ngram['polarity'] = df_ngram['bigram/trigram'].apply(lambda x: TextBlob(x).polarity)
    df_ngram['subjective'] = df_ngram['bigram/trigram'].apply(lambda x: TextBlob(x).subjectivity)

    retornar['df_ngram'] = df_ngram.to_dict()

    #Topic modeling
    from sklearn.feature_extraction.text import TfidfVectorizer
    from sklearn.decomposition import NMF
    from sklearn.pipeline import make_pipeline

    tfidf_vectorizer = TfidfVectorizer(stop_words=stoplist, ngram_range=(2,3))
    #nmf = NMF(n_components=3)
    nmf = NMF(n_components=topics)
    pipe = make_pipeline(tfidf_vectorizer, nmf)
    pipe.fit(df['textos'])

    def print_top_words(model, feature_names, n_top_words):
        retornarp = []
        for topic_idx, topic in enumerate(model.components_):
            message = "Topic #%d: " % topic_idx
            message += ", ".join([feature_names[i] for i in topic.argsort()[:-n_top_words - 1:-1]])
            #print(message, file=sys.stderr)
            retornarp.append(message)
        #print("", file=sys.stderr)
        return retornarp
    
    #print_top_words(nmf, tfidf_vectorizer.get_feature_names(), n_top_words=3)
    retornado = print_top_words(nmf, tfidf_vectorizer.get_feature_names(), n_top_words=3)
    retornar['nfm'] = retornado
    #print(retornado, file=sys.stderr)

    #LDA models
    from sklearn.decomposition import LatentDirichletAllocation
    tfidf_vectorizer = TfidfVectorizer(stop_words=stoplist, ngram_range=(2,3))
    #lda = LatentDirichletAllocation(n_components=3)
    lda = LatentDirichletAllocation(n_components=topics)
    pipe = make_pipeline(tfidf_vectorizer, lda)
    pipe.fit(df['textos'])    

    #print_top_words(lda, tfidf_vectorizer.get_feature_names(), n_top_words=3)    
    retornado = print_top_words(lda, tfidf_vectorizer.get_feature_names(), n_top_words=3)
    retornar['lda'] = retornado
    #print(retornado, file=sys.stderr)

#    return jsonify(df.to_dict())
#    return jsonify(df_ngram.to_dict())
   
    return json.dumps([
        retornar['df'],
        retornar['df_ngram'],
        retornar['nfm'],
        retornar['lda'],
        ])



##############################################################################################
def otracosa():
    dataJson = request.get_json()
    print('This is error output', file=sys.stderr)
    print(dataJson, file=sys.stderr)


    ###print('This is standard output', file=sys.stdout)
    data_string = dataJson['text']
    personas = dataJson['personas']
    #print(personas, file=sys.stderr)

    data = io.StringIO(data_string)
    data = pd.read_csv(data, delimiter = "\t", header = None, names = ['text'])

    # Extract datetime
    data[['datetime_str','text_2']] = data["text"].str.split(" - ", 1, expand=True)
    data["datetime"] = pd.to_datetime(data["datetime_str"], format="%d/%m/%y %H:%M", errors='coerce')
    #print(data)
    data = data.dropna(subset=['datetime'])
    data = data.drop(columns = ['datetime_str'])

    # Extract sender and message
    data[['sender','text_message']] = data['text_2'].str.split(': ', 1, expand=True)
    data = data.dropna(subset=['text_message'])
    data = data.drop(columns = ['text','text_2'])

    retornar={}

    #return '<img align="left" src=''data:image/png;base64,%s''>' % s

    #Most Popular Words
    ## Create DataFrame with all words used split by sender
    # Filter out 'Media Omitted' messages
    data_texts = data[~data['text_message'].str.contains('omitted')]

    # Create empty DataFrame to use for the word count
    words = pd.DataFrame(columns=['sender','words'])

    # Loop through all messages in the DataFrame
    for sender, message in zip(data_texts.sender, data_texts.text_message):
 
        # Split out each word in each message
        message_split = message.split()
        for word in message_split:
 
            # Add each word to the DataFrame
            words = words.append({'sender' : sender , 'words' : word.lower()}, ignore_index=True)

    # Remove punctuation from the start and end of words
    words['words'] = words['words'].str.replace('(^\W)|(\W$)','')

    # Create list of 'non-words' that we don't care about
    non_words = ['the','to','a','i','you','it','and','for','be','that','in','of','have','on','at','is','as','or','we','if','so','']

    # Filter out this list of 'non-words' from each set of words
    words_filtered = words[~words['words'].isin(non_words)]

    # Create word counts as a series
    word_count_total_series = words_filtered.groupby(['words']).size().sort_values(ascending=False)

    # Create word counts series as a DataFrame
    word_count_total = pd.DataFrame(word_count_total_series)

    # Reset index in order to name columns correctly
    word_count_total = word_count_total.reset_index()
    word_count_total.columns = ['words', 'count']

    # Display top 10 words used
    retornar['palabras_mas_populares'] = word_count_total.head(20)
    #print(retornar, file=sys.stderr)


    
    #Ranking
    retornar['ranking'] = word_count_total[word_count_total['words'].isin(['felicitaciones','bien','no','en','la','se','que','y','se'])]
    #print(retornar, file=sys.stderr)


    #Words by Sender
    # Create word counts by sender as a series
    word_count_by_sender_series = words_filtered.groupby(['sender', 'words']).size().sort_values(ascending=False)

    # Create word counts by sender series as a DataFrame
    word_count_by_sender = pd.DataFrame(word_count_by_sender_series)

    # Reset index in order to name columns correctly
    word_count_by_sender = word_count_by_sender.reset_index()
    word_count_by_sender.columns = ['sender', 'words', 'count']

    s = io.BytesIO()

    retornar['palabras_usadas_por']={}
    #print(personas, file=sys.stderr)

    for persona in personas:
        #print(persona, file=sys.stderr)
        s = io.BytesIO()

        # Plot bar chart with top n all words used by me
        word_count_by_sender_top_n = word_count_by_sender[word_count_by_sender['sender']==persona].head(20).sort_values(by='count',ascending=True)
        plt.barh(word_count_by_sender_top_n['words'], word_count_by_sender_top_n['count'], color='green')
        plt.xlabel("Cantidad de Palabras Unicas")
        plt.ylabel("Palabra")
        plt.title("Palabras usadas por " + persona)
        #plt.show()
        plt.savefig(s, format='png', bbox_inches="tight")
        plt.close()
        s = base64.b64encode(s.getvalue()).decode("utf-8").replace("\n", "")
        retornar['palabras_usadas_por'][persona] = '<img align="left" src=''data:image/png;base64,%s''>' % s    
        #print(retornar, file=sys.stderr)


    #Most Popular Emojis
    # Clear DataFrame to create words used
    emojis = pd.DataFrame(columns=['sender','emoji','emojiname','datetime'])
    # Loop through all messages in the DataFrame
    for sender, message, datetime in zip(data.sender, data.text_message, data.datetime):
    # Split out each word in each message
        message_split = list(message)
 
        # Loop through each word in split message
        for character in message_split:
            # If the word is an emoji
            #if character in emoji.UNICODE_EMOJI and character != "\U0001f3fc":
            if character in emoji.UNICODE_EMOJI_SPANISH and character != "\U0001f3fc":
                # Add each emoji to the DataFrame
                #print(character, file=sys.stderr)
                emojiname = emoji.demojize(character, language = "es")
                emojis = emojis.append({'sender' : sender, 'emoji' : character, 'emojiname' : emojiname, 'datetime' : datetime}, ignore_index=True)
            #print(character, file=sys.stderr)

    # Display top n most popular emojis
    #retornar['emojis'] = emojis.groupby(['emoji']).size().sort_values(ascending=False).head(30)
    retornar['emojis'] = emojis.groupby(['emoji','emojiname']).size().sort_values(ascending=False).head(30)
    #print(emoji.UNICODE_EMOJI_SPANISH, file=sys.stderr)

   
    return json.dumps([
        retornar['palabras_mas_populares'].values.tolist(),
        retornar['palabras_usadas_por'],
        #retornar['emojis'].values.tolist(),
        retornar['emojis'].to_json(),
        ])


