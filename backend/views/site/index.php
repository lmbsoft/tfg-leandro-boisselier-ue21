<?php
 
use yii\helpers\Html;
use common\models\PermisosHelpers;
 
/**
 * @var yii\web\View $this
 */
 
$this->title = 'Analizador de Comunicaciones Digitales';
 
$es_admin = PermisosHelpers::requerirMinimoRol('SuperUsuario');
 
?>
 
 
<div class="site-index">
 
    <div class="jumbotron">
 
        <h1>Bienvenido al panel de Backend</h1>
 
        <p class="lead">
            administrar usuarios, roles, y más
        </p>
 
        <p>
 
            <?php
 
            if (!Yii::$app->user->isGuest && $es_admin) {
 
                echo Html::a('Administrar Usuarios', ['user/index'], ['class' => 'btn btn-lg btn-success']);
 
            }
 
            ?>
 
        </p>
 
    </div>
 
    <div class="body-content">
 
        <div class="row">
            <div class="col-lg-4">
                <h2>Usuarios</h2>
                <p>
                    Editar estados y roles.
                </p>
                <p>
                    <?php
                    if (!Yii::$app->user->isGuest && $es_admin) {
                        echo Html::a('Administrar Usuarios', ['user/index'], ['class' => 'btn btn-default']);
                    }
                    ?>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Roles</h2>
                <p>
                    Administrar Roles.
                </p>
                <p>
                    <?php
                    if (!Yii::$app->user->isGuest && $es_admin) {
                        echo Html::a('Administrar Roles', ['rol/index'], ['class' => 'btn btn-default']);
                    }
                    ?>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Perfiles</h2>
                <p>
                    Administrar perfiles.
                </p>
                <p>
                    <?php
                    if (!Yii::$app->user->isGuest && $es_admin) {
                        echo Html::a('Administrar Perfiles', ['perfil/index'], ['class' => 'btn btn-default']);
                    }
                    ?>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Tipos de Usuario</h2>
                <p>
                    Administrar tipos de usuario. 
                </p>
                <p>
                    <?php
                    if (!Yii::$app->user->isGuest && $es_admin) {
                        echo Html::a('Administrar Tipos de Usuario', ['tipo-usuario/index'], ['class' => 'btn btn-default']);
                    }
                    ?>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Estados</h2>
                <p>
                    Administra Estados.
                </p>
                <p>
                    <?php
                    if (!Yii::$app->user->isGuest && $es_admin) {
                        echo Html::a('Administrar Estados', ['estado/index'], ['class' => 'btn btn-default']);
                    }
                    ?>
                </p>
            </div>
            <div class="col-lg-4">
                &nbsp;
            </div>
        </div>
    </div>
</div>
