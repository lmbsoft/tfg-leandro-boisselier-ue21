<?php

use yii\db\Schema;
use yii\db\Migration;

class m201216_101040_acodi extends Migration
{

    private $tableOptions;

    public function up()
    {
        $this->tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $this->tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ENGINE=InnoDB';
        }

        $this->createTable('stopwords',[
            'id'=>$this->primaryKey(),
            'nombre'=>$this->string()->notNull(),
            //'stopwords'=>'LONGTEXT NULL',
            'stopwords' => $this->text(),//$this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
        ]);

        $this->createTable('stemmers',[
            'id'=>$this->primaryKey(),
            'nombre'=>$this->string()->notNull(),
            //'stopwords'=>'LONGTEXT NULL',
            'palabras' => $this->text(),//$this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
        ]);

        $this->createTable('textos', [
            'id' => $this->primaryKey(),
            'nombre'=>$this->string()->notNull(),
            
            'categoria' => $this->string(),
            'clasificacion' => $this->string(),
            'descripcion'=>$this->text(),
            'autor'=>$this->string(),
            'texto'=>$this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
            'stopwords'=>$this->integer(),
            'stopwords_lista'=>$this->text(),
            'stemmer'=>$this->integer(),
            'stemmer_lista'=>$this->text(),
            'observaciones'=>$this->text(),

            'lugares' => $this->text(),

            'publico'=>$this->boolean(),
            'cantidad_topten'=>$this->integer()->defaultValue(10),
            
            'created_at' => $this->datetime(),
            'updated_at' => $this->datetime(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        $this->addForeignKey('fk_textos_stopwords','textos','stopwords','stopwords','id','RESTRICT');
        $this->addForeignKey('fk_textos_stemmers','textos','stemmer','stemmers','id','RESTRICT');


        // estados proyectos
        $this->createTable('estados_proyectos', [
            'id' => Schema::TYPE_PK,
            'estado_proyecto' => Schema::TYPE_STRING . "(45) NOT NULL",
            'estado_valor' => Schema::TYPE_INTEGER . "(11) NOT NULL",
        ], $this->tableOptions);
        
                $this->insert('estados_proyectos', [
//                        'id' => 1,
                        'estado_proyecto' => 'Anulado',
                        'estado_valor' => 0,
                ]);        
                $this->insert('estados_proyectos', [
//                        'id' => 2,
                        'estado_proyecto' => 'Iniciado',
                        'estado_valor' => 10,
                ]);        
                $this->insert('estados_proyectos', [
//                        'id' => 3,
                        'estado_proyecto' => 'Finalizado',
                        'estado_valor' => 20,
                ]); 
                        
        //proyectos
        $this->createTable('proyectos', [
            'id' => Schema::TYPE_PK,
            'numero_proyecto' => Schema::TYPE_STRING . "(50) UNIQUE NOT NULL",
            'nombre' => Schema::TYPE_STRING . "(255) NOT NULL",
            'fechahora_inicio' => Schema::TYPE_DATETIME . " NULL",
            'estado_proyecto' => Schema::TYPE_INTEGER . "(11) NOT NULL",
            'fechahora' => Schema::TYPE_DATETIME . " NULL",
            'observaciones' => Schema::TYPE_TEXT . " NULL",
            'stopwords'=> Schema::TYPE_INTEGER . "(11) NULL",
            'stopwords_lista'=>Schema::TYPE_TEXT . " NULL",
            'stemmer'=> Schema::TYPE_INTEGER . "(11) NULL",
            'stemmer_lista'=>Schema::TYPE_TEXT . " NULL",
            'lat' => Schema::TYPE_DOUBLE . " NULL DEFAULT 0.0",
            'lng' => Schema::TYPE_DOUBLE . " NULL DEFAULT 0.0",
            'created_at' => Schema::TYPE_DATETIME . " NULL",
            'updated_at' => Schema::TYPE_DATETIME . " NULL",
            'created_by' => Schema::TYPE_INTEGER . "(11) NULL",
            'updated_by' => Schema::TYPE_INTEGER . "(11) NULL",
        ], $this->tableOptions);
        
        $this->addForeignKey('fk_proyectos_estados', 'proyectos', 'estado_proyecto', 'estados_proyectos', 'id','RESTRICT', 'CASCADE'); // $delete= 'CASCADE' $update='CASCADE'
        $this->addForeignKey('fk_proyectos_stopwords','proyectos','stopwords','stopwords','id','RESTRICT');
        $this->addForeignKey('fk_proyectos_stemmers','proyectos','stemmer','stemmers','id','RESTRICT');
        
        //estados de archivos
        $this->createTable('estados_archivos', [
            'id' => Schema::TYPE_PK,
            'estado_archivo' => Schema::TYPE_STRING . "(45) NOT NULL",
            'estado_valor' => Schema::TYPE_INTEGER . "(11) NOT NULL",
        ], $this->tableOptions);
        
                $this->insert('estados_archivos', [
//                        'id' => 1,
                        'estado_archivo' => 'Activo',
                        'estado_valor' => 0,
                ]);        
                $this->insert('estados_archivos', [
//                        'id' => 2,
                        'estado_archivo' => 'Inactivo',
                        'estado_valor' => 10,
                ]);

        // archivos del proyecto
        $this->createTable('proyectos_archivos', [
            'id' => Schema::TYPE_PK,
            'proyecto' => Schema::TYPE_INTEGER . "(11) NOT NULL",
            'nombre' => Schema::TYPE_STRING . "(255) NOT NULL",
            //'contenido' => Schema::TYPE_TEXT . " NULL",
            'contenido'=>$this->getDb()->getSchema()->createColumnSchemaBuilder('longtext'),
            'estado_archivo' => Schema::TYPE_INTEGER . "(11) NOT NULL",
            'adjunto' => Schema::TYPE_STRING . "(255) NULL",
            'tipo_mime' => Schema::TYPE_STRING . "(255) NULL",
            'bytes' => Schema::TYPE_INTEGER . "(11) NULL",
            'created_at' => Schema::TYPE_DATETIME . " NULL",
            'updated_at' => Schema::TYPE_DATETIME . " NULL",
            'created_by' => Schema::TYPE_INTEGER . "(11) NULL",
            'updated_by' => Schema::TYPE_INTEGER . "(11) NULL",
        ], $this->tableOptions);             

        $this->addForeignKey('fk_archivos_estados', 'proyectos_archivos', 'estado_archivo', 'estados_archivos', 'id','RESTRICT', 'CASCADE'); // $delete= 'CASCADE' $update='CASCADE'
        $this->addForeignKey('fk_archivos_proyectos', 'proyectos_archivos', 'proyecto', 'proyectos', 'id','CASCADE','CASCADE'); // $delete= 'CASCADE' $update='CASCADE'

        //remitentes
        $this->createTable('proyectos_personas', [
            'id' => Schema::TYPE_PK,
            'proyecto' => Schema::TYPE_INTEGER . "(11) NOT NULL",
            'persona' => Schema::TYPE_STRING . "(255) NOT NULL",
            'edad' => Schema::TYPE_INTEGER . "(11) NULL",
            'genero' => Schema::TYPE_STRING . "(255) NULL",
            'variedad_espanol' => Schema::TYPE_STRING . "(255) NULL",
            'nivel_educativo' => Schema::TYPE_STRING . "(255) NULL",
            'observaciones' => Schema::TYPE_TEXT . " NULL",
            'lat' => Schema::TYPE_DOUBLE . " NULL DEFAULT 0.0",
            'lng' => Schema::TYPE_DOUBLE . " NULL DEFAULT 0.0",
            'created_at' => Schema::TYPE_DATETIME . " NULL",
            'updated_at' => Schema::TYPE_DATETIME . " NULL", //redundante pero es de sistema
            'created_by' => Schema::TYPE_INTEGER . "(11) NULL",
            'updated_by' => Schema::TYPE_INTEGER . "(11) NULL",
        ], $this->tableOptions);
        
        $this->addForeignKey('fk_personas_proyectos', 'proyectos_personas', 'proyecto', 'proyectos', 'id','CASCADE','CASCADE'); // $delete= 'CASCADE' $update='CASCADE'
        
        //mensajes
        $this->createTable('proyectos_mensajes', [
            'id' => Schema::TYPE_PK,
            'proyecto' => Schema::TYPE_INTEGER . "(11) NOT NULL",
            'fecha' => Schema::TYPE_DATETIME . " NULL",
            'persona' => Schema::TYPE_INTEGER . "(11) NOT NULL",
            'texto_original' => Schema::TYPE_TEXT . " NULL",
            'texto' => Schema::TYPE_TEXT . " NULL",
            'emojis' => Schema::TYPE_TEXT . " NULL",
            'adjuntos' => Schema::TYPE_TEXT . " NULL",
            'clasificacion' => Schema::TYPE_INTEGER . "(11) NULL",
            'es_punto_inflexion' => Schema::TYPE_BOOLEAN . " NULL",
            'lat' => Schema::TYPE_DOUBLE . " NULL DEFAULT 0.0",
            'lng' => Schema::TYPE_DOUBLE . " NULL DEFAULT 0.0",
            'created_at' => Schema::TYPE_DATETIME . " NULL",
            'updated_at' => Schema::TYPE_DATETIME . " NULL", //redundante pero es de sistema
            'created_by' => Schema::TYPE_INTEGER . "(11) NULL",
            'updated_by' => Schema::TYPE_INTEGER . "(11) NULL",
        ], $this->tableOptions);
        
        $this->addForeignKey('fk_mensajes_proyectos', 'proyectos_mensajes', 'proyecto', 'proyectos', 'id','CASCADE','CASCADE'); // $delete= 'CASCADE' $update='CASCADE'
        $this->addForeignKey('fk_mensajes_personas', 'proyectos_mensajes', 'persona', 'proyectos_personas', 'id','CASCADE','CASCADE'); // $delete= 'CASCADE' $update='CASCADE'
    }

    public function down()
    {
        $this->dropForeignKey('fk_mensajes_personas','proyectos_mensajes');
        $this->dropForeignKey('fk_mensajes_proyectos','proyectos_mensajes');
        $this->dropTable('proyectos_mensajes');
        $this->dropForeignKey('fk_personas_proyectos', 'proyectos_personas');
        
        $this->dropTable('proyectos_personas');
        
        $this->dropForeignKey('fk_archivos_proyectos','proyectos_archivos');
        $this->dropForeignKey('fk_archivos_estados','proyectos_archivos');
        $this->dropTable('proyectos_archivos');
        $this->dropTable('estados_archivos');
        $this->dropForeignKey('fk_proyectos_estados','proyectos');
        $this->dropForeignKey('fk_proyectos_stopwords','proyectos');
        $this->dropForeignKey('fk_proyectos_stemmers','proyectos');
        $this->dropTable('proyectos');
        $this->dropTable('estados_proyectos');

        $this->dropForeignKey('fk_textos_stemmers','textos');
        $this->dropForeignKey('fk_textos_stopwords','textos');
        $this->dropTable('textos');
        $this->dropTable('stemmers');
        $this->dropTable('stopwords');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
