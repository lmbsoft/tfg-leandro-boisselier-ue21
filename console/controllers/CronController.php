<?php

namespace console\controllers;

use yii\console\Controller;
use yii\helpers\Console;
use Yii;
use frontend\models\Avisos;
use frontend\models\Tareas;
/**
 * Console crontab actions
 */
class CronController extends Controller
{
    /**
     * Genera un timestamp
     */
    public function actionTimestamp()
    {
        file_put_contents(Yii::getAlias('@app/timestamp.txt'), time());
        $this->stdout('Timestamp generado en //console', Console::FG_GREEN, Console::BOLD);
        $this->stdout(PHP_EOL);
    }


}
