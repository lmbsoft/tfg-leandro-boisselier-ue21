# Universidad Siglo 21

Proyecto de Prototipado

Trabajo Final de Graduación

Leandro Boisselier


# Título

## Sistema clasificador de emociones para las comunicaciones digitales privadas por medio de técnicas de inteligencia artificial


# Configuración

Requerimientos:

- Docker
- docker-compose

Instrucciones de instalación:

1. clonar el proyecto
2. iniciar la composición con: docker-compose up -d
3. ingresar en el servicio web con: docker-compose exec web bash
4. instalar las dependencias con: composer.phar install
5. inicializar el proyecto de yii con: ./init
6. ajustar la configuracion de la base de datos en: common/config/main-local.php
```php
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=db;dbname=tfg',
            'username' => 'tfg',
            'password' => 'tfg',
            'charset' => 'utf8mb4',
        ],
```
7. inicializar la base de datos mediante las migraciones con: ./yii migrate/up

Direcciones de acceso:

- Frontend: http://nombreequipo.local:8080
- Backend: http://nombreequipo.local:8081

Usuarios predeterminados:

- usuario: usuario/usuario
- administrador: admin/admin
- superusuario: superusuario/superusuario

# Demo
En la siguiente direccion puede probar el sistema en funcionamiento

https://acodi.aplicacionesonline.com.ar

usuario: demo21

password: demo21


# Autor

Leandro Boisselier
